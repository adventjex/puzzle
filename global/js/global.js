function showCurrentLevel(){
    var levelWrapper = $('.level');
    levelWrapper.html('<p>' + window.globals.currentLevel +'</p>')
    levelWrapper.animate({
        opacity: "1",
        bottom: "20px"
    },2000);
    levelWrapper.show();
};

function showLevelContent(){
    var levelContent = $('.level-content');
    levelContent.animate({
        opacity: "1"
    },2000);
    levelContent.show();
};

service = {
    verticalCenter: function(element){
        function center(){
            var height = -1*element.height()/2;
            element.css('margin-top', height );
            element.css('top', '50%');
        }
        center();

        $(window).resize(function(){
            center();
        });
    },
};


$(document).ready(function(){

    var initiateLevelElements = {
        headerCode: function headerCode(){
            return '<div class="header">\
                        <div class="row">\
                            <div class="menu">\
                                <p class="open_modal" data-modal="#login_open">Login</p>\
                                <p class="open_modal" data-modal="#register_open">Register</p>\
                                <p class="logout">Logout</p>\
                                <span class="levels_icon"></span>\
                            </div>\
                        </div>\
                    </div>'
        },

        loginCode: function loginCode(){
            return '<div class="modal" id="login_open">\
                        <div class="modal_body">\
                            <form>\
                                <input placeholder="Email" spellcheck="false" type="email" class="email" />\
                                <input placeholder="Password" type="password" class="password" />\
                                <a class="button" id="login_button">LOGIN</a>\
                                <p class="error" style="display: none; color: red; text-align: center; margin-top: 15px;">Email and password does not match or account does not exist.</p>\
                            </form>\
                        </div>\
                        <span class="close_modal"></span>\
                    </div>'
        },

        registerCode: function registerCode(){
            return '<div class="modal" id="register_open">\
                        <div class="modal_body">\
                            <form>\
                               <input placeholder="Email" spellcheck="false" type="email" class="email" />\
                                <input placeholder="Password" type="password" class="password1" />\
                                <input placeholder="Re-enter Password" type="password" class="password2" />\
                                <a class="button" id="register_button">REGISTER</a>\
                                <p class="error" style="display: none; color: red; text-align: center; margin-top: 15px;">Email and password does not match or account already exists.</p>\
                            </form>\
                        </div>\
                        <span class="close_modal"></span>\
                    </div>'
        },

        addToBody: function addToBody(){
            var body = $('body');
            body.prepend(this.loginCode());
            body.prepend(this.registerCode());
            body.prepend(this.headerCode());

        },

        init: function init(){
            this.addToBody();
        }
    }

    var modals = {

        detectShow: function detectShow(){
            var self = this;

            $(document).on('click', '.open_modal',  function(e){
                var element = e.target;
                var thisModal = $(element).attr('data-modal');
                
                self.showModal(thisModal);

            });
        },

        detectClose: function detectClose(){
            var self = this;
            $(document).on('click', '.close_modal',  function(e){
                self.closeModal();

            });
        },

        showModal: function showModal(modal){
            var modal = $(modal);
            var self = this;
            modal.show();
        },

        closeModal: function closeModal(){
            $(document).find('.modal').hide();
        },

        init: function init(){
            this.detectShow();
            this.detectClose();
        }
    }

    var accountFunctions = {

        register: function register(){
            self = this;
            $(document).on('click touchstart', '#register_button', function(){
                var element = $(event.target);
                var email = element.siblings('.email').val();
                var password1 = element.siblings('.password1').val();
                var password2 = element.siblings('.password2').val();

                window.backend.register(email,password1,password2,function(response){
                    if(response.status >= 400){
                        $('#register_open').find('.error').hide();
                        window.backend.login(email,password1,function(response){

                            modals.closeModal();
                            self.loggedInFix();
                        });
                    }

                    else{
                        $('#register_open').find('.error').show();
                    }

                });
            }); 
        },

        login: function login(){
            self = this;
            $(document).on('click touchstart', '#login_button', function(){
                var element = $(event.target);
                var email = element.siblings('.email').val();
                var password = element.siblings('.password').val();
                window.backend.login(email,password,function(response){ 

                    if(response.status >= 400){
                        $('#login_open').find('.error').hide();
                        modals.closeModal();
                        self.loggedInFix();
                    }

                    else{
                        $('#login_open').find('.error').show();
                    }
                    
                });
            });
        },

        logout: function logout(){
            var url = "http://107.170.224.220/authentication/logout/";
            var self = this;

            $(document).on('click touchstart', '.logout', function(){
                $.post( url, function( response ) {
                     console.log("successfully logged out");
                     self.notLoggedInFix();
                });
            });
        },

        loggedInFix: function loggedInFix(){
            $('.open_modal').hide();
            $('.logout').css('display', 'inline-block');
        },

        notLoggedInFix: function notLoggedInFix(){
            $('.open_modal').show();
            $('.logout').hide();
        },

        checkIfLoggedIn: function checkIfLoggedIn(){
            var self = this;
            window.backend.levels_completed(function(){
                self.loggedInFix();
            });
        },

        init: function init(){
            this.checkIfLoggedIn();
            this.register();
            this.login();
            this.logout();
            
        }
    }


    var allLevelsArray = [
                            [
                                {
                                  "puzzle": "hidden_text",
                                  "answer": "monsters head"
                                },
                                {
                                  "puzzle": "backwards_puzzle",
                                  "answer": "42"
                                },
                                {
                                  "puzzle": "deer_puzzle",
                                  "answer": "deer"
                                },
                                {
                                  "puzzle": "shape_binary",
                                  "answer": "eden"
                                },
                                {
                                  "puzzle": "pie_puzzle",
                                  "answer": "147"
                                }
                            ],
                            [
                                {
                                  "puzzle": "hide_letters",
                                  "answer": "inferno"
                                },
                                {
                                  "puzzle": "a_riddle_puzzle",
                                  "answer": "mirror"
                                },
                                {
                                  "puzzle": "anagram_basic",
                                  "answer": "sins"
                                },
                                {
                                  "puzzle": "solar_system_puzzle",
                                  "answer": "jupiter"
                                },
                                {
                                  "puzzle": "spin_em_up",
                                  "answer": "spinemup"
                                },
                                {
                                  "puzzle": "dual_view_puzzle",
                                  "answer": "takesides"
                                },
                                {
                                  "puzzle": "mapgoogle_ford_puzzle",
                                  "answer": "ford"
                                },
                                {
                                  "puzzle": "painting",
                                  "answer": "power"
                                },
                                {
                                  "puzzle": "anagram_advance",
                                  "answer": "grudge"
                                },
                                {
                                  "puzzle": "hexagon_pyramid",
                                  "answer": "7"
                                },
                                {
                                  "puzzle": "md5encryption_puzzle",
                                  "answer": "38b212fffa9e467b6b267f171cffbdf3"
                                },
                                {
                                  "puzzle": "einstein_puzzle",
                                  "answer": "einstein"
                                },
                                {
                                  "puzzle": "md5encryption_stopgo_puzzle",
                                  "answer": "798f6af69fdd00319a34c9cb61b5d1bc"
                                },
                                {
                                  "puzzle": "graphic_decipher",
                                  "answer": "everything is shadow"
                                },
                                {
                                  "puzzle": "turn_em_off",
                                  "answer": "turnemoff"
                                },
                                {
                                  "puzzle": "italian_numbers",
                                  "answer": "68"
                                },
                                {
                                  "puzzle": "turn_em_on",
                                  "answer": "turnemon"
                                },
                                {
                                  "puzzle": "caesar_cipher_puzzle",
                                  "answer": "enigma"
                                },
                                {
                                  "puzzle": "light_em_up",
                                  "answer": "lightemup"
                                },
                                {
                                  "puzzle": "noble_gas",
                                  "answer": "radon"
                                },
                                {
                                  "puzzle": "towers_of_hanoi_puzzle",
                                  "answer": "towersofhanoi"
                                },
                                {
                                  "puzzle": "decagon_symbols",
                                  "answer": "decagonsymbols"
                                },
                                {
                                  "puzzle": "four_way_graphic_encoding",
                                  "answer": "candle"
                                },
                                {
                                  "puzzle": "substitute_cipher_puzzle",
                                  "answer": "proxy"
                                },
                                {
                                  "puzzle": "phone_number",
                                  "answer": "endofact1"
                                }
                            ],
                            [
                                {
                                  "puzzle": "morse_code",
                                  "answer": "sounds"
                                },
                                {
                                  "puzzle": "everest",
                                  "answer": "kola superdeep borehole"
                                },
                                {
                                  "puzzle": "rotation_puzzle",
                                  "answer": "matching"
                                },
                                {
                                  "puzzle": "teapot_puzzle",
                                  "answer": "189"
                                },
                                {
                                  "puzzle": "spinning_decagon",
                                  "answer": "593"
                                },
                                {
                                  "puzzle": "ascii-abe-puzzle",
                                  "answer": "lincoln"
                                },
                                {
                                  "puzzle": "elevator_puzzle",
                                  "answer": "theanimals"
                                },
                                {
                                  "puzzle": "painting_theme",
                                  "answer": "lucifer"
                                },
                                {
                                  "puzzle": "demons_humans",
                                  "answer": "allcrossed"
                                },
                                {
                                  "puzzle": "ball_puzzle",
                                  "answer": "blindman"
                                },
                                {
                                  "puzzle": "fast_moving_puzzle",
                                  "answer": "james fallon"
                                },
                                {
                                  "puzzle": "jug_puzzle",
                                  "answer": "twojugs"
                                },
                                {
                                  "puzzle": "elevator_video",
                                  "answer": "elisa lam"
                                },
                                {
                                  "puzzle": "adolf_hitler",
                                  "answer": "adolf hitler"
                                },
                                {
                                  "puzzle": "last_supper",
                                  "answer": "judas"
                                },
                                {
                                  "puzzle": "light_bulb",
                                  "answer": "1993"
                                },
                                {
                                  "puzzle": "jonestown",
                                  "answer": "jim jones"
                                },
                                {
                                  "puzzle": "first_to_hundred",
                                  "answer": "hundred"
                                },
                                {
                                  "puzzle": "hare_and_tortoise",
                                  "answer": "tortoise"
                                },
                                {
                                  "puzzle": "letter_symmetry",
                                  "answer": "ox"
                                },
                                {
                                  "puzzle": "gravity_puzzle",
                                  "answer": "carmen colon"
                                },
                                {
                                  "puzzle": "telephone_puzzle",
                                  "answer": "1876"
                                },
                                {
                                  "puzzle": "recursive_puzzle",
                                  "answer": "next-puzzle"
                                },
                                {
                                  "puzzle": "color_blind",
                                  "answer": "life"
                                },
                                {
                                  "puzzle": "around_the_earth",
                                  "answer": "act3"
                                }
                            ],
                            [
                                {
                                  "puzzle": "heart_puzzle",
                                  "answer": "heartache"
                                },
                                {
                                  "puzzle": "alphabet_puzzle",
                                  "answer": "initiative"
                                },
                                {
                                  "puzzle": "card_puzzle",
                                  "answer": "blackjack"
                                },
                                {
                                  "puzzle": "complete_puzzle",
                                  "answer": "perfectscore"
                                },
                                {
                                  "puzzle": "drop_puzzle",
                                  "answer": "inorder"
                                },
                                {
                                  "puzzle": "reflected_number",
                                  "answer": "66"
                                },
                                {
                                  "puzzle": "earth_radius",
                                  "answer": "china"
                                },
                                {
                                  "puzzle": "duplicating_puzzle",
                                  "answer": "99"
                                },
                                {
                                  "puzzle": "font_puzzle",
                                  "answer": "illusion"
                                },
                                {
                                  "puzzle": "chinese_zodiac",
                                  "answer": "zodiac"
                                },
                                {
                                  "puzzle": "symbol_puzzle",
                                  "answer": "gluttony"
                                },
                                {
                                  "puzzle": "element_quantity_puzzle",
                                  "answer": "flame"
                                },
                                {
                                  "puzzle": "map_puzzle",
                                  "answer": "lion"
                                },
                                {
                                  "puzzle": "cities_puzzle",
                                  "answer": "usborne"
                                },
                                {
                                  "puzzle": "sixqueens",
                                  "answer": "chess"
                                },
                                {
                                  "puzzle": "president_puzzle",
                                  "answer": "george"
                                },
                                {
                                  "puzzle": "four_knights_puzzle",
                                  "answer": "reflection"
                                },
                                {
                                  "puzzle": "3_jug_puzzle",
                                  "answer": "threeejugs"
                                },
                                {
                                  "puzzle": "seconds_puzzle",
                                  "answer": "timeout"
                                },
                                {
                                  "puzzle": "calculator_numbers",
                                  "answer": "5555"
                                },
                                {
                                  "puzzle": "wonders_puzzle",
                                  "answer": "giza"
                                },
                                {
                                  "puzzle": "sun_reflection",
                                  "answer": "deepoceansun"
                                },
                                {
                                  "puzzle": "pattern_puzzle",
                                  "answer": "symmetry"
                                },
                                {
                                  "puzzle": "micro_puzzle",
                                  "answer": "13425"
                                },
                                {
                                  "puzzle": "united_states_puzzle",
                                  "answer": "wvwiwy"
                                }
                            ],
                            [
                                {
                                  "puzzle": "qwerty_puzzle",
                                  "answer": "adapt"
                                },
                                {
                                  "puzzle": "shape_number",
                                  "answer": "8346"
                                },
                                {
                                  "puzzle": "qrcode_puzzle",
                                  "answer": "denso wave"
                                },
                                {
                                  "puzzle": "empty_puzzle",
                                  "answer": "3kings"
                                },
                                {
                                  "puzzle": "letter_stroke",
                                  "answer": "124"
                                },
                                {
                                  "puzzle": "3x3_sum_puzzle",
                                  "answer": "sum_puzzle_qaz"
                                },
                                {
                                  "puzzle": "phone_pad_puzzle",
                                  "answer": "tninecipher"
                                },
                                {
                                  "puzzle": "spin_coding",
                                  "answer": "jack froese"
                                },
                                {
                                  "puzzle": "combination_lock",
                                  "answer": "combination"
                                },
                                {
                                  "puzzle": "bell_puzzle",
                                  "answer": "oil"
                                },
                                {
                                  "puzzle": "word_in_words_puzzle",
                                  "answer": "tyrion"
                                },
                                {
                                  "puzzle": "curves_puzzle",
                                  "answer": "pqrsu"
                                },
                                {
                                  "puzzle": "babushka_lady",
                                  "answer": "babushka lady"
                                },
                                {
                                  "puzzle": "not_you_puzzle",
                                  "answer": "1200"
                                },
                                {
                                  "puzzle": "count_number_spelling_puzzle",
                                  "answer": "35444"
                                },
                                {
                                  "puzzle": "jumbled_code_puzzle",
                                  "answer": "fu-go"
                                },
                                {
                                  "puzzle": "product_puzzle",
                                  "answer": "2250"
                                },
                                {
                                  "puzzle": "number_spelling_puzzle",
                                  "answer": "stfn"
                                },
                                {
                                  "puzzle": "man_and_balls",
                                  "answer": "threeballs"
                                },
                                {
                                  "puzzle": "shape_number_pyramid",
                                  "answer": "191878"
                                },
                                {
                                  "puzzle": "code_shapes_puzzle",
                                  "answer": "chessboard"
                                },
                                {
                                  "puzzle": "four_numbers",
                                  "answer": "186"
                                },
                                {
                                  "puzzle": "ring_puzzle",
                                  "answer": "centurysum"
                                },
                                {
                                  "puzzle": "nand_puzzle",
                                  "answer": "binarypower"
                                },
                                {
                                  "puzzle": "base_puzzle",
                                  "answer": "thereisnorightanswer"
                                }
                            ]
                            ];


    var generateAllLevels = {

        codeToAppend: "",

        generateLevelsCode: function generateLevelsCode(array){
            var self = this;
            for (var i = 0; i < array.length; i++){
                var act = array[i]
                for (var j = 0; j < act.length; j++){
                    var number = j+1;
                    var name = act[j]["puzzle"];
                    self.codeToAppend += "<a href='#'><span class='num'>" + number + ".</span> " + name + "<span class='box'></span></a>"
                }
            }

            self.addCode(self.codeToAppend);
        },

        addCode: function addCode(code){
            var levelsContainer = $('.all_levels');
            levelsContainer.html(code);
        },

        init: function init(){
            this.generateLevelsCode(allLevelsArray);
        }


    }

    var functionsInit = function(){
        // initiateLevelElements.init();
        // modals.init();
        // accountFunctions.init();
        generateAllLevels.init();
    }();
});

