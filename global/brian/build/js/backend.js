(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Created by brian on 7/23/15.
 */
window.backend = {};
window.backend.hostname = "107.170.224.220";
window.backend.protocol = "http://";
window.backend.login_path = "authentication/login/";
window.backend.registration_path = "authentication/registration";
window.backend.levels_completed_path = "user-profile/levels-completed/";


window.backend.build_url = function(path){
	return window.backend.protocol+window.backend.hostname + path;
};

window.backend.get_cookie = function(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

window.backend.get_csrf = window.backend.get_cookie('csrftoken');


window.backend.setup_ajax = function(){
	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			var csrftoken = window.backend.get_csrf;
			if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		},
		xhrFields: {withCredentials: true}
	});
};

window.backend.login = function(email, password, callback){
	var login_url = window.backend.build_url(window.backend.login_path);
	var data = {
		"email":email,
		"password":password
	};
	$.post(login_url, data, callback).fail(callback);
};

window.backend.register = function(email, password1, password2, callback){
	var register_url = window.backend.build_url(window.backend.registration_path);
	var data = {
    	"email":email,
    	"password1":password1,
    	"password2":password2
	};
	$.post(register_url, data, callback).fail(callback);

};

window.backend.levels_completed = function(callback){
	var levels_completed_url = window.backend.build_url(window.backend.levels_completed_path);
	$.get(levels_completed_url, callback);
};

window.backend.setup_ajax();

},{}]},{},[1]);
