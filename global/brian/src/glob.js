/**
 * Created by brian on 3/7/15.
 */

var _ = require('underscore');
var md5 = require("blueimp-md5").md5;


window.utils = {
	turnArgumentsIntoArray: function (inputArguments) {
		return Array.prototype.slice.call(inputArguments, 0);
	},

	buildPathAbsolutePath: function () {
		var argumentsArray = utils.turnArgumentsIntoArray(arguments);
		return "/" + argumentsArray.join("/");
	},

	dynamicVerticalAndHorizontalCentering: function (element) {
		var $enclosingElement = $(element).parent();

		var centerContainer = document.createElement("div");
		var windowHeight = $(window).height();
		var $centerContainer = $(centerContainer).css({
			position: "relative",
			height: windowHeight

		});

		var centerElement = document.createElement("div");
		var $centerElement = $(centerElement).css({
			position: "absolute",
			top: "50%",
			left: "50%",
			transform: "translate(-50%, -50%)",
			width: "100%",
			"max-width": "750px",
			padding: "0 20px"
		});

		$(element).after($centerContainer);
		$centerElement.append(element);
		$centerContainer.append($centerElement);
		$enclosingElement.append($centerContainer);

		$(window).resize(function () {
			var currentHeight = $(window).height();
			$centerContainer.height(currentHeight);
		});

		return $centerContainer

	},
	fadeInElement: function (element) {
		var $element = $(element);
		$element.animate({
			opacity: "1"
		}, 2000);
		$element.show();
	},
	cloneObject: function (objectToClone) {
		return _.map(objectToClone, _.clone);
	}
};


window.malum = {
	getActName: function(){
		return window.location.pathname.match(/act\d+/)[0];
	},
	getActNumber: function(){
		return parseInt(window.location.pathname.match(/act(\d+)/)[1]);
	},
	createUrlForLevel: function (answer) {
		var encryptedAnswer = md5(answer);
		var htmlFile = "index.html";
		var actName = this.getActName();
		return utils.buildPathAbsolutePath(actName, encryptedAnswer, htmlFile);
	},
	checkAnswer: function (levelNumber, answer, callback) {
		//var encryptedAnswer = md5(answer);
		//if (encryptedAnswer !== window.globals.answerHash){
		//	return false;
		//}

		var act = window.malum.getActNumber();
		window.backend.complete_level(answer, function(){
			if (textStatus === "success") {
				console.log("level completion saved!");
			}
		});
		window.backend.call_check_answer(act, levelNumber, answer, callback)
	},
	createButton: function (buttonText, callback) {
		var $div = $(document.createElement("div")).addClass("continue_wrapper");
		var $buttonElement = $(".next_level");
		if ($buttonElement.length <= 0) {
			$buttonElement = $(document.createElement("button"));
		}
		$buttonElement.css({
			position: "fixed",
			right: 20
			//bottom: -20,
			//opacity: 0
		}).html(buttonText).click(callback).addClass("next_level");
		$div.append($buttonElement);
		$("body").append($div);
		if ($div.transit) {
			$div.transit({
				bottom: 0,
				opacity: 1
			});
		} else {
			$div.animate({
				bottom: 0,
				opacity: 1
			});
		}

	},
	checkAnswerAndCreateButton: function (answer, callbackIfFail, callbackIfSuccess) {
		var hostName = window.location.hostname;
		var port = window.location.port;
		window.malum.checkAnswer(window.globals.currentLevel, answer.toLowerCase().trim(), function (data, textStatus) {
			if (textStatus === "success") {
				//var redirectUrl = "http://" + hostName + ":" + port + data;
				var redirectUrl = window.malum.createUrlForLevel(answer.toLowerCase().trim());
				window.malum.createButton("Continue", function (event) {
					event.preventDefault();
					window.location = redirectUrl;
				});
				if(callbackIfSuccess){
					callbackIfSuccess();
				}
			} else if (callbackIfFail) {
				callbackIfFail();
			}
		});
	}
};


module.exports = {
	malum: window.malum,
	utils: window.utils
};

/**
 * Created by brian on 7/23/15.
 */
window.backend = {};
window.backend.hostname = "107.170.224.220/";
window.backend.protocol = "http://";
window.backend.login_path = "authentication/login/";
window.backend.registration_path = "authentication/registration/";
window.backend.levels_completed_path = "user-profile/levels-completed/";
window.backend.check_answer_path = "check-answer/";


window.backend.verify_answer = function(act, level_number, answer, callback){
	var verify_url = window.backend.build_url(window.backend.check_answer_path);
	//console.log(url);
	var data = {
		act: act,
		level_number: level_number,
		answer: answer,

	};
	$.post(verify_url, data, callback);
};

window.backend.build_url = function(path){
	return window.backend.protocol+window.backend.hostname + path;
};

window.backend.get_cookie = function(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

window.backend.get_csrf = window.backend.get_cookie('csrftoken');

window.backend.setup_ajax = function(){
	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			//var csrftoken = window.backend.get_cookie('csrftoken');
			//if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
			//	xhr.setRequestHeader("X-CSRFToken", csrftoken);
			//}
		},
		xhrFields: {withCredentials: true},
		data: {
			"_content_type":"application/json"
		},
		crossDomain: true,
		//contentType: "application/json",
		//dataType: 'json',
	});
};

window.backend.login = function(email, password, callback){
	//window.backend.setup_ajax();
	var login_url = window.backend.build_url(window.backend.login_path);
	var data = {
		"email":email,
		"password":password
	};
	$.post(login_url, data, callback).fail(callback);
};

window.backend.register = function(email, password1, password2, callback){
	//window.backend.setup_ajax();
	var register_url = window.backend.build_url(window.backend.registration_path);
	var data = {
    	"email":email,
    	"password1":password1,
    	"password2":password2
	};
	$.post(register_url, data, callback).fail(callback);

};

window.backend.levels_completed = function(callback){
	//window.backend.setup_ajax();
	var levels_completed_url = window.backend.build_url(window.backend.levels_completed_path);
	$.get(levels_completed_url, callback);
};

window.backend.complete_level = function(answer, callback){
	//window.backend.setup_ajax();
 	var complete_level_url = window.backend.build_url(window.backend.levels_completed_path);
	console.log(complete_level_url);
	var data = {
		"level":window.globals.levelName,
		"answer":answer
	};
	data = {"_content":JSON.stringify(data)};
	$.post(complete_level_url, data, callback).fail(function(e){
		console.log(e.status);
		if(e.status==409){
			callback("","success");
		} else {
			console.log("Failed to save level.");
		}
	});
};

window.backend.call_check_answer = function(act, level, answer, callback){
	var check_answer_url = window.backend.build_url(window.backend.check_answer_path);
	console.log(check_answer_url);
	var data = {
		act:act,
		level_number:level,
		answer:answer
	};
	data = {"_content":JSON.stringify(data)};
	$.post(check_answer_url, data, callback)

};

window.backend.setup_ajax();







//DEBUG CODE BELOW
function fileExists(pathString){
	//var http = new XMLHttpRequest();
    //http.open('HEAD', pathString, false);
    //http.send();
    //return http.status!=404;
	return true;
}

(function addSkipFeature(){

	$(".level").click(function(){
		if(fileExists("../DEBUG_FLAG")) {
			var hostName = window.location.hostname;
			var port = window.location.port;
			var redirectUrl = "http://" + hostName + ":" + port + "/" + window.globals.answerHash;
			window.malum.createButton("DEBUG: CONTINUE", function (event) {
				event.preventDefault();
				window.location = redirectUrl;
			});
		}
	});
})();

(function () {
	$(".submit").click(function (event) {
		event.preventDefault();
		var answer = $(".answer-input").val();
		window.malum.checkAnswerAndCreateButton(answer);
	});
})();
