#!/usr/bin/env python2.7

__author__ = 'brian'
import shutil
import os
import json
from jinja2 import Template
import hashlib
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import re
import secrets
import hashlib
import pickle





def file_get_contents(filename):
    with open(filename) as f:
        return f.read()


def generate_javascript_local_globals_file_string(currentLevel, levelName, encoded_answer, templateFile):
    file_contents = file_get_contents(get_absolute_path(templateFile))
    template = Template(file_contents)
    return template.render(levelNumber=currentLevel, answerHash=encoded_answer, levelName=levelName)


def create_file(relative_path, file_content):
    opened_file = open(get_absolute_path(relative_path), 'w')
    opened_file.write(file_content)


def create_glue_file(level, levelName, encoded_answer, relative_path):
    glue_file_name = "glue.js"
    file_contents = generate_javascript_local_globals_file_string(level, levelName, encoded_answer, glue_file_name)
    create_file(os.path.join(relative_path, "js", glue_file_name), file_contents)


def deserialize_json_string(string):
    return json.loads(string)


def get_list_of_ordered_puzzle_answer_dicts():
    return deserialize_json_string(file_get_contents(get_absolute_path("levels.json")))

def get_list_of_ordered_puzzle_answer_dicts_from_global(act_number, global_dict):
    return global_dict[act_number]


def get_puzzle_names(list_of_puzzle_answer_dicts):
    return [puzzleAnswerDict['puzzle'] for puzzleAnswerDict in list_of_puzzle_answer_dicts]


def get_puzzle_answers(list_of_puzzle_answer_dicts):
    return [puzzleAnswerDict['answer'] for puzzleAnswerDict in list_of_puzzle_answer_dicts]


def get_encoded_answers(list_of_puzzle_answer_dicts):
    for answer in get_puzzle_answers(list_of_puzzle_answer_dicts):
        md5object = hashlib.md5()
        md5object.update(answer)
        yield md5object.hexdigest()


def get_puzzle_folders(encoded_answers):
    try:
        len(encoded_answers)
    except:
        encoded_answers = [i for i in encoded_answers]

    return ["start"]+encoded_answers


def get_absolute_path(relative_path):
    return os.path.join(os.getcwd(), relative_path)


def delete_folder(relative_path_of_folder):
    absolute_path = get_absolute_path(relative_path_of_folder)
    if os.path.exists(absolute_path):
        shutil.rmtree(absolute_path)


def copy_folder(relative_source, relative_destination):
    absolute_source = get_absolute_path(relative_source)
    absolute_destination = get_absolute_path(relative_destination)
    shutil.copytree(src=absolute_source, dst=absolute_destination, ignore=shutil.ignore_patterns("node_modules"))

def clear_folder(relative_path):
    absolute_destination = get_absolute_path(relative_path)
    shutil.rmtree(absolute_destination)

def get_global_json_object():
    contents = file_get_contents("global_levels.json")
    return json.loads(contents)


def create_act_directories(global_json_object):
    return ["act{0}".format(act_number) for act_number in xrange(0,len(global_json_object))]

def compile_act(act_number, global_dict):
    build_path = "staticbuild/act{0}/".format(act_number)
    global_build_files_directory = "global"
    global_build_files_directory_css = "act{0}/act_global_css".format(act_number)

    list_of_puzzle_answer_dicts = get_list_of_ordered_puzzle_answer_dicts_from_global(act_number, global_dict)
    # print list_of_puzzle_answer_dicts

    delete_folder(build_path)
    copy_folder(global_build_files_directory_css, os.path.join(build_path, "act_global_css"))
    puzzle_names = get_puzzle_names(list_of_puzzle_answer_dicts)
    puzzle_answers = get_encoded_answers(list_of_puzzle_answer_dicts)
    puzzle_folder_build = get_puzzle_folders(puzzle_answers)
    totalPuzzles = None
    for folder, encodedAnswer, index in zip(puzzle_names, puzzle_folder_build, xrange(0,len(puzzle_names))):
        print "level {1}:\t\t\t{0}".format(folder, index+1)
        print "folder name: {0}\n".format(encodedAnswer)
        relative_build_new_directory = os.path.join(build_path, encodedAnswer)
        absolute_build_new_directory = get_absolute_path(relative_build_new_directory)
        copy_folder("act{0}/{1}".format(act_number, folder), absolute_build_new_directory)
        next_encoded = puzzle_folder_build[index+1]
        create_glue_file(index+1, folder, next_encoded, relative_build_new_directory)
        totalPuzzles = index + 1

    print "puzzles compiled."
    print "There are {0} puzzles".format(totalPuzzles)



def main():
    build_path = "staticbuild/"
    global_build_files_directory = "global"
    clear_folder(build_path)
    copy_folder(global_build_files_directory, os.path.join(build_path, global_build_files_directory))
    global_json_object = get_global_json_object()
    actnames = create_act_directories(global_json_object)
    for act_level, act_name in zip(range(0,len(actnames)), actnames):
        compile_act(act_level, global_json_object)

    #print "UPLOADING TO S3"

    #upload_tree_to_s3("staticbuild")


def level_dir_to_s3_key(dirname):
    return re.sub('<*staticbuild/>*','',dirname)

def generate_file_state_dict(files):
    result = {}
    for file in files:
        with open(file) as f:
            contents = f.read()
            m = hashlib.md5(contents)
            key = m.digest()
            result[key] = file
    return result

def get_old_build_state():
    try:
        return pickle.load(open("build_state.p","rb"))
    except IOError:
        print "ERROR LOADING PICKLE STATE"
        return {}


def has_changed(file, build_state):
    with open(file) as f:
        contents = f.read()
        m = hashlib.md5(contents)
        key = m.digest()
        return not build_state.get(key) == file









def upload_tree_to_s3(folder):
    connection = S3Connection(secrets.AWS_ACCESS, secrets.AWS_SECRET)
    bucket = connection.get_bucket("puzzlemalum")

    file_infos = [file_info for file_info in os.walk(folder)]
    def memo(acc, file_info):
        return acc + [file_info[0] + "/" + a_file for a_file in file_info[2]]
    file_paths = reduce(memo, file_infos, [])
    old_build_state = get_old_build_state()

    for file_info in file_infos:
        directory = file_info[0]
        prefix_key = level_dir_to_s3_key(directory)
        files = file_info[2]

        for file in files:
            file_path = directory +"/" + file
            if has_changed(file_path, old_build_state):
                keyname = prefix_key + "/" + file
                print "keyname: " + keyname
                key = bucket.new_key(keyname)
                key.set_contents_from_filename(file_path)
                key.set_acl('public-read')
                print "uploaded %s ..." % file_path
            else:
                print "skipping %s ..." % file_path

    new_build_state = generate_file_state_dict(file_paths)
    pickle.dump(new_build_state, open("build_state.p", "wb"))






if __name__ == "__main__":
    main()










