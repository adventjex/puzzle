/**
 * Created by brian on 3/7/15.
 */
window.globals = {
	levelName: "{{ levelName }}",
	currentLevel: {{ levelNumber }},
	answerHash: "{{ answerHash}}"
};