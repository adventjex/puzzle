var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function(){
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);

		var answerIndex = [2,4,5];
		var wheel1 = [];
		var wheel2 = [];

	}


	function display(index, wheel1, wheel2) {
		return [wheel1[index], wheel2[index]];
	}

	Array.prototype.rotate = (function() {
		var unshift = Array.prototype.unshift,
			splice = Array.prototype.splice;

		return function(count) {
			var len = this.length >>> 0,
				count = count >> 0;

			unshift.apply(this, splice.call(this, count % len, len));
			return this;
		};
	})();


	function createControlRotator(initialElement) {
		var $pointUp = $(document.createElement("div"));
		var $upIcon = $(document.createElement("i")).addClass("fa fa-chevron-up");
		$pointUp.append($upIcon);
		var $pointDown = $(document.createElement("div"));
		var $downIcon = $(document.createElement("i")).addClass("fa fa-chevron-down");
		$pointDown.append($downIcon);
		var $display = $(document.createElement("div"));
		$display.append(initialElement);
		var $wrapper = $(document.createElement("span")).addClass("rotator");
		$wrapper.append($pointUp).append($display).append($pointDown);
		return $wrapper;
	}

})();



