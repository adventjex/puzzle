var _ = require('underscore');
var md5 = require("blueimp-md5").md5;
var SEGMENTS = 20;


(function () {
	init();

	//function isEven(number){
	//	return number%2===0;
	//}

	(function ground() {
		var $ground = $(document.createElement("div"));
		$ground.css({
			position: "absolute",
			width: "100%",
			height: 20,
			bottom: 200 - 20,
			"background-color": "#4C5053"
		});
		$(".content").append($ground);
	})();

	function generateSphere(radius, color) {
		var dimensionCSSValue = (radius * 2).toString() + "px";
		var $ball = $(document.createElement("div"));
		$ball.css({
			display: "block",
			width: dimensionCSSValue,
			height: dimensionCSSValue,
			margin: "20% auto 0",
			"background-color": color,
			"border-radius": "50%",
			"box-shadow": "inset -25px -25px 40px rgba(0,0,0,.5)",
			"background-image": "linear-gradient(-45deg, " + color + " 0%, transparent 100%)"
		});


		return $ball;
	};


	function wall(position, segments) {
		var width = Math.floor(($(window).width() - 160) / segments);
		var positionFromLeft = position * width + 80 + 20 + 20;
		var $wall = $(document.createElement("div"));
		$wall.css({
			position: "absolute",
			width: 20,
			height: 150,
			left: positionFromLeft,
			"background-color": "#39407C"
		}).addClass("wall");
		return $wall;
	}

	function Bump(name, position) {
		var that = this;
		that.isUp = false;
		that.isOn = $.cookie("wallOn") === "true" || $("endReached") === "true" || getCurrentTime() >= SEGMENTS ? true : false;
		if (that.isOn) {
			$.cookie("wallOn", "true");
		}


		this.render = function () {
			var $bump = wall(position, SEGMENTS);
			if (that.isUp) {
				$bump.css({
					bottom: 200 - 20
				})
			} else {
				$bump.css({
					bottom: 200 - 20 - 150
				});
			}
			$(".content").append($bump);
			that.el = $bump;

		};

		that.forceDown = function (speed) {
			that.el.transit({
				y: 0
			}, speed);
			that.isUp = false;
			that.setCookie("false");
		};

		that.forceUp = function (speed) {
			that.el.transit({
				y: -150
			}, speed);
			that.isUp = true;
			that.setCookie("true");
		};

		that.switch = function () {
			if (that.isUp) {
				that.el.transit({
					y: 0
				});
				that.isUp = false;
				that.setCookie("false");
			} else {
				var allWalls = walls;
				allWalls.forEach(function (wall) {
					wall.forceDown()
				});
				that.el.transit({
					y: -150
				});
				that.isUp = true;
				that.setCookie("true");
			}
		};

		this.setCookie = function (isUp) {
			$.cookie("wall_" + name, isUp);
		};

		this.readCookie = function () {
			var isUp = $.cookie("wall_" + name);
			return isUp === "true";
		};

		this.initPosition = function () {
			if (that.isOn) {
				that.render();
				that.el.click(that.switch);
				if (that.readCookie()) {
					that.forceUp(1);
				} else {
					that.forceDown(1);
				}
			}

		};

		this.initPosition();
	}

	function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) {
				return sParameterName[1];
			}
		}
	}

	function triggerIncrementTime(delay) {
		if (getCurrentTime() >= 100) {
			//DO NOTHING.
		} else if (getCurrentTime() === 0) {
			$.cookie("endReached", "false");
			setTimeout(function () {
				incrementTime();
			}, delay);
		} else {
			setTimeout(function () {
				incrementTime();
			}, delay);
		}
	}

	function getCurrentTime() {
		var currTime = parseInt(getUrlParameter("time"));
		if (currTime) {
			if (currTime > SEGMENTS) {
				currTime = SEGMENTS;
			} else if (currTime < 0) {
				currTime = 0;
			}
		} else {
			currTime = 0;
		}
		return currTime;
	}

	function getPreviousTime() {
		var prevTime = parseInt($.cookie("currentTime"));
		if (prevTime) {
			if (prevTime > SEGMENTS) {
				prevTime = SEGMENTS;
			} else if (prevTime < 0) {
				prevTime = 0;
			}
		} else {
			prevTime = 0;
		}
		return prevTime;
	}

	function incrementTime() {
		var currTime = getCurrentTime();
		if (!currTime) {
			currTime = 0;
		} else if (currTime >= SEGMENTS) {
			return currTime;
		}
		var newTimeValue = $.param({
			time: currTime + 1
		});
		var newurl = window.location.pathname + "?" + newTimeValue;
		window.location.href = newurl;
	}

	//1 is forward, 0 is same, -1 is backwards;
	function whichWayInTime() {
		var oldTime = getPreviousTime();
		var currentTime = getCurrentTime();
		return currentTime - oldTime;
	}


	function Ball(name, startPosition, endPosition, color, segments) {
		var that = this;
		this.position = startPosition;
		this.color = color;
		this.name = name;
		this.endPosition = endPosition;

		this.shouldFreeze = function () {
			var behind = walls[that.position - 1] ? walls[that.position - 1].isUp : false;
			var same = walls[that.position] ? walls[that.position].isUp : false;
			var front = walls[that.position - 1] ? walls[that.position + 1].isUp : false;
			return behind || same || front;
		};

		function cookieName() {
			return "Ball" + that.name;
		};

		this.render = function () {
			var width = Math.floor(($(window).width() - 160) / segments);
			var positionFromLeft = that.position * width + 80;
			var sphere = generateSphere(20, that.color).css({
				left: positionFromLeft,
				position: "absolute",
				bottom: 200
			});
			$(".lane").append(sphere);
		};

		this.updatePosition = function () {
			if (that.shouldFreeze()) {
				that.position += getPreviousTime();
			} else {
				if (getCurrentTime() === 0) {
					that.position = startPosition;
				} else {
					var timeDirection = whichWayInTime();
					that.position += timeDirection;
					if (that.position > that.endPosition) {
						that.position = endPosition;
					} else if (that.position < 0) {
						that.position = 0;
					}
				}
			}
			that.render();
			$.cookie(cookieName(), that.position);
		};

		this.updatePosition();

	}

	function Button(position, color) {
		this.position = position;
		this.pressed = false;
		this.color = color;
	}


	function init() {
		var currentLevel = showCurrentLevel();
		showLevelContent();
		var bumpArray = [];
		for (var i = 1; i < SEGMENTS; i++) {
			bumpArray.push(new Bump("ball_" + i.toString(), i));
		}
		window.walls = bumpArray;
		var ballOne = new Ball("ballOne", 0, SEGMENTS, "blue", SEGMENTS);
		var ballTwo = new Ball("ballTwo", 3, SEGMENTS, "red", SEGMENTS);
		triggerIncrementTime(1000);


	}

	//window.utils.dynamicVerticalAndHorizontalCentering($(".content"));

})
();



