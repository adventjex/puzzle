var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();
	function init() {
		showCurrentLevel();
		showLevelContent();
		var $content = $(".content");
		var $left = $(".left");
		var $right = $(".right");
		window.utils.dynamicVerticalAndHorizontalCentering($content);

		var selected = {};
		var left = {};
		var right = {};

		var gameState = {
			"lanternState": true,
			"totalTime": 30,
			"selectedTime": 0
		};
		displayInfo(gameState);

		var test = new Traveler(5, 20, true, "red", $left, $right, selected, gameState);
		var test1 = new Traveler(4, 15, true, "blue", $left, $right, selected, gameState);
	}

	function checkSelectedCanMove(selected, remainingTime) {
		if (selected.length > 2) {
			return false;
		}
		var totalTime = 0;
		for (var key in selected) {
			if (selected[key].state != lanternState) {
				return false;
			}
		}

		if (totalTime >= remainingTime) {
			return false;
		}

		return true;
	}

	function getGreatestNumberFromSelected(selected) {
		var selectedSpeeds = Array();
		for (var key in selected) {
			selectedSpeeds.push(selected[key].speed);
		}

		return _.reduce(selectedSpeeds, function (memo, number) {
			return number > memo ? number : memo
		}, 0);
	}

	function countProperties(obj) {
		var count = 0;

		for (var prop in obj) {
			if (obj.hasOwnProperty(prop))
				++count;
		}

		return count;
	}

	function renderSelected(selected) {
		var $mover = $(".mover");
		$mover.html("");
		for (var key in selected) {
			$mover.append(selected[key].renderObject);
		}
	}

	function renderLeftAndRight(left, right){
		var $left = $(".left");
		var $right = $(".right");
		$left.html("");
		$right.html("");

		for(var key in left){
			$left.append(left[key].renderObject);
		}

		for(var key in right){
			$right.append(right[key].renderObject);
		}
	}

	function displayInfo(gameState) {
		var $timeToCross = $(".time_to_cross").html("Time to cross: " + gameState.selectedTime.toString());
		var $timeLeft = $(".time_left").html("Time left: " + gameState.totalTime.toString());
	}

//states are left or right;
//left is true, right is false.
	function Traveler(speed, radius, initialState, color, $enclosingElementLeft, $enclosingElementRight, selectedObjects, gameState) {
		var that = this;
		that.selected = false;
		that.radius = radius;
		that.speed = speed;
		that.state = initialState;
		that.color = color;
		that.renderObject = generateRenderObject();
		that.move = function () {
			updateState();
			updateRenderObject();
		};

		that.toggleSelected = function () {
			if (!that.selected && canBeSelected()) {
				that.renderObject.children().eq(1).css("background-color", "white");
				selectedObjects[that.speed] = that;
				gameState.selectedTime = getGreatestNumberFromSelected(selectedObjects);

			} else {
				that.renderObject.children().eq(1).css("background-color", that.color);
				getEnclosingElement().append(that.renderObject);
				selectedObjects[that.speed] = undefined;
				delete selectedObjects[that.speed];
				gameState.selectedTime = getGreatestNumberFromSelected(selectedObjects);
			}
			that.selected = !that.selected;
			renderSelected(selectedObjects);
			renderLeftAndRight(); //TODO pass left and right globals to the object and put them as parameters to this function....
			displayInfo(gameState);

		};

		function canBeSelected() {
			return gameState.lanternState === that.state && countProperties(selectedObjects) < 2
		}

		function getEnclosingElement() {
			if (that.state) {
				return $enclosingElementLeft;
			} else {
				return $enclosingElementRight;
			}
		}

		function generateRenderObject() {
			var $sphere = generateSphere(that.radius, that.color);
			getEnclosingElement().prepend($sphere);
			return $sphere;
		}

		function updateRenderObject() {
			getEnclosingElement().append(that.renderObject);
		}

		function updateState() {
			that.state = !that.state;
		}

		function generateSphere(radius, color) {
			var dimensionCSSValue = (radius * 2).toString() + "px";
			var $ball = $(document.createElement("div"));
			$ball.css({
				display: "block",
				width: dimensionCSSValue,
				height: dimensionCSSValue,
				margin: "20% auto 0",
				"background-color": color,
				"border-radius": "50%",
				"box-shadow": "inset -25px -25px 40px rgba(0,0,0,.5)",
				"background-image": "linear-gradient(-45deg, " + color + " 0%, transparent 100%)"
			});


			return $ball;
		};
	};


})
();



