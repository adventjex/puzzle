var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();


	function init() {
		showCurrentLevel();
		showLevelContent();

		window.utils.dynamicVerticalAndHorizontalCentering($(".content"));

		var $submit = $(".submit");
		var $answerInput = $(".answer-input");
		$submit.off();
		$submit.click(function (e) {
			e.preventDefault();
			var answer = $answerInput.val();
			window.malum.checkAnswerAndCreateButton(answer, function(){
				window.open("answer", '_self');
			});
		});

	}

})();



