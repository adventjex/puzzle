var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function(){
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();

//The puzzle is based off of a 8,3 regular star. TO make it dynamic would mean
//creating an algorithm that generates these stars dynamically.
//Thus there will be no dynamic generation. All numbers are hardcoded.

//nodes are doubly linkedLists basically. Objects stored in an array that you can access by index.
//Objects can move through the linklist as long as no other object is in the way.
//False means the node is empty.
function Arena(){
	this.amountOfNodes = 8;
	this.nodes = [];
	for(var i = 0; i<this.amountOfNodes; i++){
		this.nodes.push(false);
	}
	this.edgeOrder = [0,3,6,1,4,7,2,5];

}

Arena.prototype.addObjToNode = function(obj, index){
	if(!this.nodes[index]){
		this.nodes[index] = obj;
	}
};

Arena.prototype.renderArena = function(){

};

Arena.prototype.drawPoints = function(svg){

};

function Pawn(color, index, arenaInstance){
	this.color = "color";
	this.arenaInstance = arenaInstance;
	this.index = index;
	this.arenaInstance.addObjToNode(this, this.index);
}

Pawn.prototype.moveForward = function(){
	var futureIndex;
	if(this.index === this.arenaInstance.nodes.length-1){
		futureIndex = 0;
	}
	else {
		futureIndex = this.index+1;
	}

	if(!this.areaInstance.nodes[futureIndex]){
		this.arenaInstance.nodes[futureIndex] = this;
		this.arenaInstance.nodes[this.index] = false;
		this.index = futureIndex;
	}
};

Pawn.prototype.moveBackward = function(){
	var futureIndex;
	if(this.index === 0){
		futureIndex = this.arenaInstance.nodes.length-1;
	}
	else {
		futureIndex = this.index-1;
	}

	if(!this.areaInstance.nodes[futureIndex]){
		this.arenaInstance.nodes[futureIndex] = this;
		this.arenaInstance.nodes[this.index] = false;
		this.index = futureIndex;
	}
};





