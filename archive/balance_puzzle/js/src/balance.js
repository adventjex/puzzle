var matter = require('matter-js');
var init = function () {

	var $bodyDiv = $('body');
	var bodyWidth = $bodyDiv.width();
	var windowHeight = $(window).height();

	var matterHeight = Math.floor(windowHeight);

	var bodyDivElement = $bodyDiv[0];
	var backgroundColor = "#171b1d";
	var lightBlue = "#fafafa";
	// Matter.js module aliases
	var Engine = Matter.Engine,
		World = Matter.World,
		Bodies = Matter.Bodies;

	// create a Matter.js engine

	var engine = Engine.create({
		render: {
			element: bodyDivElement,
			options: {
				background: backgroundColor,
				//wireframes: false,
				width: bodyWidth,
				height: matterHeight,
				hasBounds: false
			}
		}
	});

	// create two boxes and a ground

	var cubeLength = 40;
	var cubeOptions = {
		render: {
			fillStyle: lightBlue,
			strokeStyle: lightBlue,
			lineWidth: 1
		}
	}
	var boxA = Bodies.rectangle(400, 200, cubeLength, cubeLength, cubeOptions);
	var boxB = Bodies.rectangle(410, 150, cubeLength, cubeLength, cubeOptions);
	//var boxC = Bodies.rectangle(350, 200, cubeLength, cubeLength, cubeOptions);
	var ground = Bodies.rectangle(bodyWidth/2, matterHeight-60, bodyWidth, 60, {
		isStatic: true,
		render: {
			fillStyle: backgroundColor,
			strokeStyle: backgroundColor,
			lineWidth: 1
		}
	});

	// add all of the bodies to the world
	World.add(engine.world, [boxA, boxB, ground]);

	//center the canvas
	$bodyDiv.css({
		"text-align": "center"
	});

	//$("canvas").css({
	//	display: "inline-block",
	//	"max-width": "100%",
	//	"max-height": "100%"
	//});


	// run the engine
	Engine.run(engine);
}


$(document).ready(init);


