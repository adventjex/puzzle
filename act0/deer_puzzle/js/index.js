var boxes = {
				"s1": {position: 1, boxid: "#s1"},
				"s2": {position: 1, boxid: "#s2"},
				"s3": {position: 1, boxid: "#s3"},
				"s4": {position: 1, boxid: "#s4"},
				"s5": {position: 1, boxid: "#s5"},
				"s6": {position: 1, boxid: "#s6"},
				"s7": {position: 1, boxid: "#s7"},
				"s8": {position: 1, boxid: "#s8"},
				"s9": {position: 1, boxid: "#s9"},
			}

$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);

	var windowHeight = $(window).height();
	$('body').css('height', windowHeight);

});

$(document).on('click', 'img', function(){
	var id = $(this).attr('id');
	var newPosition = boxes[id].position;

	if(boxes[id].position == 4){
		newPosition = 1;
		rotateBox(newPosition,id);
	}
	else if(boxes[id].position <= 3){
		newPosition++;
		rotateBox(newPosition,id);
	}
});

var rotateBox = function(newPosition,id){
    if(newPosition == 1){
   		$(boxes[id].boxid).removeClass();
	   	$(boxes[id].boxid).addClass('r0');
   	}
    else if(newPosition == 2){
   		$(boxes[id].boxid).removeClass();
	   	$(boxes[id].boxid).addClass('r90');
   	}

    else if(newPosition == 3){
   		$(boxes[id].boxid).removeClass();
	   	$(boxes[id].boxid).addClass('r180');
   	}
    
    else if(newPosition == 4){
   		$(boxes[id].boxid).removeClass();
	   	$(boxes[id].boxid).addClass('r270');
   	}	

   	boxes[id].position = newPosition;
   	checkAnswer();
}

var checkAnswer = function(){
	if(boxes["s1"].position == 2 && boxes["s2"].position == 3 && boxes["s3"].position == 3 && boxes["s4"].position == 4  && boxes["s5"].position == 4  && boxes["s6"].position == 3 && boxes["s7"].position == 4 && boxes["s8"].position == 2 && boxes["s9"].position == 2 ){
		$('.imagerow').each(function(){
			$(this).animate(
				{
					opacity: 0.25
				},500
			)
		});
		window.malum.checkAnswerAndCreateButton("deer");
	}
}
