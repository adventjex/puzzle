/**
 * Created by brian on 3/7/15.
 */

var _ = require('underscore');
var md5 = require("blueimp-md5").md5;


window.utils = {
	turnArgumentsIntoArray: function (inputArguments) {
		return Array.prototype.slice.call(inputArguments, 0);
	},

	buildPathAbsolutePath: function () {
		var argumentsArray = utils.turnArgumentsIntoArray(arguments);
		return "/" + argumentsArray.join("/");
	},

	dynamicVerticalAndHorizontalCentering: function (element) {
		var $enclosingElement = $(element).parent();

		var centerContainer = document.createElement("div");
		var windowHeight = $(window).height();
		var $centerContainer = $(centerContainer).css({
			position: "relative",
			height: windowHeight

		});

		var centerElement = document.createElement("div");
		var $centerElement = $(centerElement).css({
			position: "absolute",
			top: "50%",
			left: "50%",
			transform: "translate(-50%, -50%)",
			width: "100%",
			"max-width": "750px",
			padding: "0 20px"
		});

		$(element).after($centerContainer);
		$centerElement.append(element);
		$centerContainer.append($centerElement);
		$enclosingElement.append($centerContainer);

		$(window).resize(function () {
			var currentHeight = $(window).height();
			$centerContainer.height(currentHeight);
		});

		return $centerContainer

	},
	fadeInElement: function (element) {
		var $element = $(element);
		$element.animate({
			opacity: "1"
		}, 2000);
		$element.show();
	},
	cloneObject: function (objectToClone) {
		return _.map(objectToClone, _.clone);
	}
};


window.malum = {
	createUrlForLevel: function (answer) {
		var encryptedAnswer = md5(answer);
		var htmlFile = "index.html";
		return utils.buildPathAbsolutePath(encryptedAnswer, htmlFile);
	},
	checkAnswer: function (levelNumber, answer, callback) {
		$.ajax({
			url: malum.createUrlForLevel(answer)
		}).always(function () {
			var textStatus = arguments[1];
			var data;
			if (textStatus === "success") {
				data = malum.createUrlForLevel(answer)
			} else if (textStatus === "error") {
				data = "404 error, or wrong answer";
			} else {
				data = "Error";
			}
			callback(data, textStatus);
		});
	},
	createButton: function (buttonText, callback) {
		var $div = $(document.createElement("div")).addClass("continue_wrapper");
		var $buttonElement = $(".next_level");
		if ($buttonElement.length <= 0) {
			$buttonElement = $(document.createElement("button"));
		}
		$buttonElement.css({
			position: "fixed",
			right: 20
			//bottom: -20,
			//opacity: 0
		}).html(buttonText).click(callback).addClass("next_level");
		$div.append($buttonElement);
		$("body").append($div);
		if ($div.transit) {
			$div.transit({
				bottom: 0,
				opacity: 1
			});
		} else {
			$div.animate({
				bottom: 0,
				opacity: 1
			});
		}

	},
	checkAnswerAndCreateButton: function (answer, callbackIfFail) {
		var hostName = window.location.hostname;
		var port = window.location.port;
		window.malum.checkAnswer(window.globals.currentLevel, answer.toLowerCase().trim(), function (data, textStatus) {
			if (textStatus === "success") {
				var redirectUrl = "http://" + hostName + ":" + port + data;
				window.malum.createButton("Continue", function (event) {
					event.preventDefault();
					window.location = redirectUrl;
				});
			} else if (callbackIfFail) {
				callbackIfFail();
			}
		});
	}
};


module.exports = {
	malum: window.malum,
	utils: window.utils
};

(function () {
	$(".submit").click(function (event) {
		event.preventDefault();
		var answer = $(".answer-input").val();
		window.malum.checkAnswerAndCreateButton(answer);
	});
})();