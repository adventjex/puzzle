$(document).ready(function(){

	showCurrentLevel();
	showLevelContent();
	
	var fireState = false;
	var lightningState = false;
	var waterState = false;
	var earthState = false;

	var fire = $('#fire');
	var lightning = $('#lightning');
	var water = $('#water');
	var earth = $('#earth');

	var fireBtn = $('#fire-btn');
	var lightningBtn = $('#lightning-btn');
	var waterBtn = $('#water-btn');
	var earthBtn = $('#earth-btn');

	fireBtn.click(function(){
		waterState = false;
		lightningState = true;
		lightning.addClass('fa-spin');
		water.removeClass('fa-spin');
		allMatching();
	});

	lightningBtn.click(function(){
		fireState = true;
		waterState = true;
		fire.addClass('fa-spin');
		water.addClass('fa-spin');
		allMatching();
	});

	waterBtn.click(function(){
		lightningState = false;
		earthState = true;
		earth.addClass('fa-spin');
		lightning.removeClass('fa-spin');
		allMatching();
	});

	earthBtn.click(function(){
		fireState = true;
		earthState = false;
		fire.addClass('fa-spin');
		earth.removeClass('fa-spin');
		allMatching();
	})

	function allMatching(){
		
		if (fireState && lightningState && waterState && earthState){
			window.malum.checkAnswerAndCreateButton("spinemup");

		}

		else{
			return;
		}
	}

	// var content = $('.content');
	// window.service.verticalCenter(content);





});

