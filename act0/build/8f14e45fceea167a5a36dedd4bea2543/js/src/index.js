var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function(){
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);

		var $input = $(".answer-input");
		var $button = $(".submit").click(function(event){
			event.preventDefault();
			var inputContent = $input.val();
			updateHashLevel(inputContent);
		})
	}

	function getHashLevelFromHash(hash){
		return {
			"DEAD END":8,
			"72cc1092aec2f4a0c1f233979c30dbe0":7,
			"57c16216a7920b72ce2410d1cb4af9e1":6,
			"f2d6aa45250213e3e61d63dcc4068372":5,
			"0947407aa756c6c3b0d317177899367c":4,
			"a332acfa2301a209928d9ff435a331ca":3,
			"7cf0b82afdfce9da7fa0ea9483474171":2,
			"d8aa4f8050dd83e5902c5e01be01c91b":1,
			"38b212fffa9e467b6b267f171cffbdf3":0
		}[hash]
	}

	function updateHashLevel(hash){
		var hashLevel = getHashLevelFromHash(hash);
		console.log(hashLevel);
		if(hashLevel || hashLevel === 0){
			$(".current_en").html(hash);
			$(".crypt_level").html(hashLevel);
			console.log(utils);
			window.malum.checkAnswerAndCreateButton(hash);
		} else {

		}
	}

})();




