var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function placePuzzle(element, sequences) {
		for (var i = sequences.length - 1; i > -1; i--) {
			var spans = generateSpansForStrings(sequences[i]);
			var $div = $(document.createElement("div")).addClass("numbers");
			for (var j = 0; j < spans.length; j++) {
				$div.append(spans[j]);
			}
			$(element).prepend($div);
		}
	}

	function generateSpansForStrings(arrayOfStrings, classString) {
		return _.reduce(arrayOfStrings, function (collector, string) {
			var $span = $(document.createElement("span")).append(string)
			if (classString) {
				$span.addClass(classString);
			}
			collector.push($span);
			return collector;
		}, [])
	}

	function rotateArrayRight(inputArray) {
		var lastElement = inputArray.pop();
		inputArray.unshift(lastElement);
		return inputArray;
	}

	function rotateArrayLeft(inputArray) {
		var firstElement = inputArray[0];
		var rotatedArray = inputArray.slice(1);
		rotatedArray.push(firstElement);
		return rotatedArray;
	}

	function appendArrayOfElements(element, inputArray) {
		for (var i = 0; i < inputArray.length; i++) {
			$(element).append(inputArray[i]);
		}
	}

	function stringToArrayOfChar(string) {
		return _.reduce(string, function (collector, letter) {
			collector.push(letter);
			return collector;
		}, []);
	}

	function createRotatingElements(inputArray, leftButton, rightButton) {
		inputArray.push(rightButton);
		console.log(inputArray.length);
		inputArray.unshift(leftButton);
		return inputArray;
	}

	function transitionDownFade(element) {
		$(element).transition({
			y:50,
			opacity:0
		});
	}

	function transitionBackFadeIn(element) {
		$(element).transition({
			y:0,
			opacity:1
		});
	}

	function sequentialTimeMap(arrayOfThings, interval, functionToApply, callback) {
		var stepInterval = interval / arrayOfThings.length;
		_.map(arrayOfThings, function (element, index) {
			setTimeout(function () {
				functionToApply(element);
			}, stepInterval * index);
		});

		setTimeout(callback, interval+stepInterval);
	}

	function createControlElement (css_class, callback) {
		var $span = $(document.createElement("span"));
		var $icon = $(document.createElement("i"));
		$icon.addClass(css_class);
		$icon.click(callback);
		$span.append($icon);
		return $span;
	}

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		var $alphabetDiv = $('.alphabet');
		var $cipherDiv = $('.cipher');
		var $innerCipherDiv = $('.inner_cipher');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		var message = "";
		var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		var alphabetArray = stringToArrayOfChar(alphabet);

		var cipherSpans = generateSpansForStrings(alphabetArray, "alpha");
		cipherSpans.forEach(function(element){
			$innerCipherDiv.append(element);
		});

		var $alpha = $(".alpha");

		var leftElement = createControlElement("fa fa-chevron-left", function(){
			console.log("left");
			console.log(alphabetArray);
			alphabetArray = rotateArrayLeft(alphabetArray);
			console.log(alphabetArray);

			sequentialTimeMap($alpha, 500, transitionDownFade, function() {
				$alpha.each(function (index) {
					$(this).html(alphabetArray[index]);
				});
				sequentialTimeMap($alpha, 500, transitionBackFadeIn, function(){});
			});

		});

		var rightElement = createControlElement("fa fa-chevron-right", function(){
			console.log("right");
			console.log(alphabetArray);
			alphabetArray = rotateArrayRight(alphabetArray);
			console.log(alphabetArray);
			sequentialTimeMap($alpha, 500, transitionDownFade, function() {
				$alpha.each(function (index) {
					$(this).html(alphabetArray[index]);
				});
				sequentialTimeMap($alpha, 500, transitionBackFadeIn, function(){});
			});
		});
		console.log(leftElement);
		$contentDiv.prepend(leftElement);
		$cipherDiv.after(rightElement);




		var sequences = [
			alphabet
		];
		placePuzzle($alphabetDiv, sequences);
		//$(".message").html(message);

	}


})();



