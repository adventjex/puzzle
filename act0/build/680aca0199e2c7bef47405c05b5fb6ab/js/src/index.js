var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $(".content");
		var $planets = $(".spheres")
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		var planetArray = ["S", "M", "V", "E", "M", "?", "S", "U", "N", "P"];
		var spans = generateSpansForStrings(planetArray, "sphere");
		var sunIcon = $(document.createElement("i")).addClass("fa fa-sun-o glowingSphere").transition({
			scale:[3,3]
		});

		$(spans[0]).prepend(sunIcon);

		spans.forEach(function(element, index){
			$planets.append(element);
		})



	}

	function reset (startIndex, currentIndex, arrayOfElements){
		moveFromElement(arrayOfElements[currentIndex], arrayOfElements[startIndex]);
		return startIndex;
	}

	function moveFromElement(element, newElement) {
		$(element).transition({
			scale: [1, 1]
		});
		$(newElement).transition({
			scale: [2, 2]
		})
	}

	function createControlElement(css_class, callback) {
		var $span = $(document.createElement("span"));
		var $icon = $(document.createElement("i"));
		$icon.addClass(css_class);
		$icon.click(callback);
		$span.append($icon);
		return $span;
	}

	function moveLeft(currentIndex, puzzleArray) {
		var currentMoves = puzzleArray[currentIndex];
		var newIndex = currentIndex - currentMoves;
		if (newIndex < 0) {
			return false;
		} else {
			return newIndex;
		}
	}

	function moveRight(currentIndex, puzzleArray) {
		var currentMoves = puzzleArray[currentIndex];
		var newIndex = currentIndex + currentMoves;
		if (newIndex >= puzzleArray.length) {
			return false;
		} else {
			return newIndex;
		}
	}

	function isGameWon(currentIndex, puzzleArray) {
		return currentIndex === (puzzleArray.length - 1);
	}

	function generateSpansForStrings(arrayOfStrings, classString) {
		return _.reduce(arrayOfStrings, function (collector, string) {
			var $span = $(document.createElement("span")).append(string)
			if (classString) {
				$span.addClass(classString);
			}
			collector.push($span);
			return collector;
		}, [])
	}
})();



