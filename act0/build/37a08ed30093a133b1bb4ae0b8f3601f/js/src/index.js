var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);

		var $input = $(".answer-input");
		var $button = $(".submit").click(function (event) {
			event.preventDefault();
			var inputContent = $input.val();
			updateHashLevel(inputContent);
		})
	}

	function getHashLevelFromHash(hash) {
		return {
			"EVERY END HAS A BEGINNING": 5,
			"42c779f111c140519703a8ee8a2a8f98": 4,
			"NDJjNzc5ZjExMWMxNDA1MTk3MDNhOGVlOGEyYThmOTg=": 3,
			"e7ca02d84b50aee0196a499ba589ee8a": 2,
			"ZTdjYTAyZDg0YjUwYWVlMDE5NmE0OTliYTU4OWVlOGE=": 1,
			"798f6af69fdd00319a34c9cb61b5d1bc": 0
		}[hash]
	}

	function isEven(number) {
		return number % 2 === 0;
	}

	function updateHashLevel(hash) {
		var hashLevel = getHashLevelFromHash(hash);
		console.log(hashLevel);
		if (hashLevel || hashLevel === 0) {
			$(".current_en").html(hash);
			var $cryptlevel = $(".crypt_level");
			$cryptlevel.html(hashLevel);
			if (isEven(hashLevel)) {
				$cryptlevel.addClass("right");
			} else {
				$cryptlevel.addClass("left");
			}
			console.log(utils);
			//window.malum.checkAnswer(window.globals.currentLevel, hashLevel, function (data, textStatus) {
			//	if (data === "success") {
			//		window.location = data;
			//	}
			//});
			window.malum.checkAnswerAndCreateButton(hashLevel);
		} else {

		}
	}

})();




