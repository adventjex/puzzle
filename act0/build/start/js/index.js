var current_frame, total_frames, path, length, handle, myobj;

myobj = document.getElementById('myobj').cloneNode(true);

var init = function() {
  current_frame = 0;
  total_frames = 80;
  path = new Array();
  length = new Array();
  for(var i=0; i<3;i++){
    path[i] = document.getElementById('i'+i);
    l = path[i].getTotalLength();
    console.log(l);
    length[i] = l;
    path[i].style.strokeDasharray = l + ' ' + l;  
    path[i].style.strokeDashoffset = l;
  }
  handle = 0;
}
 
 
var draw = function() {
   var progress = current_frame/total_frames;
   if (progress > 1) {
     window.cancelAnimationFrame(handle);
   } else {
     current_frame++;
     for(var j=0; j<path.length;j++){
       path[j].style.strokeDashoffset = Math.floor(length[j] * (1 - progress));
     }
     handle = window.requestAnimationFrame(draw);
   }
};


$(document).ready(function(){

	var pulse = $('.pulse');
	var logo = $('#logo');
	var logo2 = $('#logo2');
	logo2.hide();
	var opening = $('.opening');
	var keyhole = $('.keyhole');
	keyhole.hide();
	var keyholeImg = $('.keyhole-svg');
	
	var levelContent = $('.level-content');

	pulse.on('click', function(){

		logo.animate({
			opacity: '0',
			top: '20px'
		},1000);/* this makes the original logo disappear */

		logo.hide(function(){
			logo2.animate({
				opacity: '1',
				top: '0',
			},1000); 
			logo2.show(); /* show the second logo */

			setTimeout(function(){
				opening.animate({
					opacity: '0',
					top: '20px'
				},1000);

				setTimeout(function(){
					opening.hide();
					keyhole.show();
					init();
					draw();
				},1000);


			},1500); /* after 1.5 seconds hide everything in class opening */
		
		}); /* hide the first logo, show second, then fade everything out. then show keyhole. */

	});/* when you click the pulsing circle */


	keyhole.click(function(){
		keyhole.animate({
			opacity: '0',
		},800);

		
		setTimeout(function(){
			keyhole.hide();
			showCurrentLevel(1,1);
			levelContent.animate({
				opacity: "1",
				top: "0",
			},2000);
			levelContent.show();
			window.service.verticalCenter(content);
			
		},1100);


	});

	var content = $('.content');
	window.service.verticalCenter(content);


});

