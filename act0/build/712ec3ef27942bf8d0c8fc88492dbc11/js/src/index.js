var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $content = $(".content");
		window.utils.dynamicVerticalAndHorizontalCentering($content)
		game();



	}

	//smallest plates at the end of the plates array
	function Peg(plates) {
		var that = this;
		if (plates) {
			that.plates = plates;
		} else {
			that.plates = [];
		}

		this.lastPlate = function(){
			return that.plates[that.plates.length-1];
		};

		this._removePlate = function () {
			return that.plates.pop();
		};

		this._addPlate = function (plate) {
			if (that.plates.length > 0 && plate.size > that.plates[that.plates.length - 1].size) {
				return false;
			} else {
				that.plates.push(plate);
				return true;
			}
		};

		this.movePlate = function(pegToMoveToo){
			if (that.plates.length <= 0){
				return false;
			}
			console.log(that.lastPlate.size );
			console.log(pegToMoveToo.lastPlate());
			if (pegToMoveToo.plates.length > 0 && that.lastPlate().size > pegToMoveToo.lastPlate().size){
				return false;
			} else {
				var plateToMove = that._removePlate();
				pegToMoveToo._addPlate(plateToMove);
				return true;
			}
		};

		this.renderPeg = function (onClickCallback) {
			var $wrapperDiv = $(document.createElement("div"));
			$wrapperDiv.addClass("Peg").css({
				"min-height":150
			});
			$($wrapperDiv).click(function(){
				//$wrapperDiv.remove();
				onClickCallback();
			});

			that.plates.forEach(function (plate) {
				var additionalPlateSize = plate.size + 2;
				var $plateDiv = $(document.createElement("div")).addClass("plate").css({
					width: additionalPlateSize*additionalPlateSize*2
				});
				$wrapperDiv.append($plateDiv);
			});

			return $wrapperDiv;
		};
	}


	function Plate(size) {
		this.size = size;
	}

	function generatePlateArray(numberOfPlates) {
		var resultArray = [];
		for (var i = numberOfPlates; i > 0; i--) {
			var newPlate = new Plate(i);
			resultArray.push(newPlate);
		}
		return resultArray;
	}

	function game() {
		var winCondition = 5;
		var myPlates = generatePlateArray(winCondition);
		var Peg1 = new Peg(myPlates);
		var Peg2 = new Peg();
		var Peg3 = new Peg();

		var $peg1Span = $(".peg1").data("Peg", Peg1);
		var $peg2Span = $(".peg2").data("Peg", Peg2);
		var $peg3Span = $(".peg3").data("Peg", Peg3);

		var currentlySelectedPegElement = undefined;
		var $pegs = $(".pegs");
		//this is a clojure! be careful!
		function handleSelection(inputElement){
			var $plate = $(".plate");
			if (currentlySelectedPegElement === undefined){
				currentlySelectedPegElement = inputElement;
				$plate.removeClass("highlightPeg");
				$(currentlySelectedPegElement).children().last().children().last().addClass("highlightPeg");
			} else {
				currentlySelectedPegElement.data('Peg').movePlate(inputElement.data('Peg'));
				$plate.removeClass("highlightPeg");
				currentlySelectedPegElement = undefined;
				rerenderAll();
			}
			if (checkWin()){
				window.malum.checkAnswerAndCreateButton("towersofhanoi");
			}
		}

		function checkWin(){
			return Peg3.plates.length === winCondition;
		}

		function rerenderAll(){
			$pegs.html("");
			$pegs.each(function(index){
				var that = this;
				$(this).append($(this).data("Peg").renderPeg(function(){
					handleSelection($(that));
				}));
			})
		}

		$peg1Span.append(Peg1.renderPeg(function(){
			handleSelection($peg1Span);
		}));
		$peg2Span.append(Peg2.renderPeg(function(){
			handleSelection($peg2Span);
		}));
		$peg3Span.append(Peg3.renderPeg(function(){
			handleSelection($peg3Span);
		}));

		myPlates = [];
	}

})();



