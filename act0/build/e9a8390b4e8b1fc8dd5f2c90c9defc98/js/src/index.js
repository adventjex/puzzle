var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function placePuzzle(element, sequences) {
		for (var i = sequences.length-1; i > -1; i--) {
			var spans = generateSpansForStrings(sequences[i]);
			var $div = $(document.createElement("div")).addClass("numbers");
			for (var j = 0; j < spans.length; j++) {
				$div.append(spans[j]);
			}
			$(element).prepend($div);
		}
	}

	function modifySequence(twodeearray){
		twodeearray[0][0] = "e";
		twodeearray[0][4] = "v";
		twodeearray[0][9] = "e";
		twodeearray[1][6] = "r";
		twodeearray[3][2] = "y";
		twodeearray[3][3] = "t";
		twodeearray[4][6] = "h";
		twodeearray[4][8] = "i";
		twodeearray[5][1] = "n";
		twodeearray[5][9] = "g";
		twodeearray[6][5] = " ";
		twodeearray[6][9] = "i";
		twodeearray[7][1] = "s";
		twodeearray[8][3] = " ";
		twodeearray[8][7] = "s";
		twodeearray[9][0] = "h";
		twodeearray[9][1] = "a";
		twodeearray[9][2] = "d";
		twodeearray[9][4] = "o";
		twodeearray[9][10] = "w";
		return twodeearray;
	}

	function generateSpansForStrings(arrayOfStrings) {
		return _.reduce(arrayOfStrings, function (collector, string) {
			var $span = $(document.createElement("span")).append(string);
			collector.push($span);
			return collector;
		}, [])
	}

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		//window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		var sequences = generateMatrixOfRandomLetters(12,10);
		sequences = modifySequence(sequences);
		placePuzzle($contentDiv, sequences);
		var $mover = $(".mover");
		$mover.pep();

	}
	function generateRandomLetter(){
		return Math.random().toString(36).replace(/[^a-z]+/g, '')[0];
	}
	function generateRandomLetterArray(amount){
		return _.reduce(_.range(amount), function(collector, index){
			collector.push(generateRandomLetter());
			return collector;
		}, [])
	}

	function generateMatrixOfRandomLetters(width, height){
		return _.reduce(_.range(height), function(collector, index){
			collector.push(generateRandomLetterArray(width));
			return collector;
		}, []);
	}


})();



