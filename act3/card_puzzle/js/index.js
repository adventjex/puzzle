var cardArray = [2,3,4,5,6,7,8,9,10,10,10,10,10,10,10,10]
var total = 0;
var count = 124;
var moves = 6;

function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

var createCards = function(){

	var level = $('.levelarea');

	for (var i = 0; i < cardArray.length; i++){

		var num = Math.round(Math.random()*3);
		
		if (num == 0){
			var suit = 'spade';
		}

		else if (num == 1){
			var suit = 'heart';
		}

		else if (num == 2){
			var suit = "diamond"
		}

		else if (num == 3){
			var suit = "club";
		}

		if (cardArray[i]==10) {
			var face = Math.round(Math.random()*3);

			if(face == 0){
				cardNum = "10";
			}

			else if(face == 1){
				cardNum = "J";
			}

			else if (face == 2){
				cardNum = "Q";
			}

			else if (face == 3){
				cardNum = "K";
			}
		}

		else{
			cardNum = cardArray[i];
		}

		level.append('<div class="card-base card" data-number="'+ cardArray[i]+'" style="background-image: url(\'static/'+suit+'.svg\')">'+cardNum+'</div>');

	}

}

var checkTotal = function(number){
	if(number < 22){
		count = count - total;

		if(count < 1){
			count = 0;
			window.malum.checkAnswerAndCreateButton("blackjack");
			return;
		}

		total = 0;
		$('.count').html(count);
	}

	else{
		total = 0;
	}

}

var getTotal = function(){

	if (moves > 0 ){
		moves = moves -1;
		$('.moves').html(moves);
		$('.selected').each(function(){
			var string = $(this).attr('data-number');
			var number = parseInt(string);
			total = total+number;
		});
		$('.selected').each(function(){
			$(this).animate({
				opacity: 0
			},500);
			$(this).css('cursor', 'default');
			$(this).removeClass('card');
			$(this).removeClass('selected');
		});
		checkTotal(total);
	}

	else{
		location.reload();
	}
}

$(document).on('click', '.reset', function(){
	 location.reload();
});

$(document).on('click', '.card', function(){

	$(this).toggleClass('selected');

});

$(document).on('click', '.answer', function(){

	getTotal();

});




$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	shuffle(cardArray);
	createCards(16);


});

