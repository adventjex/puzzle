var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

window.GRIDSIZE = 6;

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function checkAll(){
	window.grid = window.grid ? window.grid : {};
	if(Object.size(window.grid) === GRIDSIZE){
		for(var myKey in window.grid){
			if(!window.grid[myKey].checkAll()){
				return false;
			}
		}
		return true;
	} else {
		return false;
	}
}

function Square(x, y) {
	this.x = x;
	this.y = y;
}

Square.prototype.createElement = function () {
	var that = this;
	var squareElement = document.createElement("span");
	$(squareElement)
		.addClass("row" + this.y.toString())
		.addClass("col" + this.x.toString())
		.addClass("square")
		.click(function () {
			var key = "row" + that.y.toString() + "col" + that.x.toString();
			window.grid = window.grid ? window.grid : {};
			if (!window.grid[key]) {
				window.grid[key] = new Queen(that.x, that.y);
				if(checkAll()){
					window.malum.checkAnswerAndCreateButton("chess");
				}
			} else {
				var queen = window.grid[key];
				queen.destroy();
				delete window.grid[key];
			}
		});
	return squareElement;
};


function Queen(x, y) {
	this.x = x;
	this.y = y;
	this.render();
}

Queen.prototype.render = function () {
	var imageElement = document.createElement("img");
	$(imageElement).attr("src", "queen.svg").addClass("queen");
	$(".row" + this.y + ".col" + this.x).append(imageElement);
};

Queen.prototype.destroy = function () {
	$(".row" + this.y + ".col" + this.x).children().remove();
};

Queen.prototype.checkRow = function () {
	var thiskey = "row" + this.y.toString() + "col" + this.x.toString();
	for (var key in window.grid) {
		if (window.grid[key].x === this.x && window.grid[key].y === this.y) {
			continue;
		}

		if (window.grid[key].x === this.x) {
			return false;
		}
	}

	return true;
};

Queen.prototype.checkCol = function () {
	var thiskey = "row" + this.y.toString() + "col" + this.x.toString();
	for (var mykey in window.grid) {
		if (window.grid[mykey].x === this.x && window.grid[mykey].y === this.y) {
			continue;
		}

		if (window.grid[mykey].y === this.y) {
			return false;
		}
	}

	return true;
};

Queen.prototype.checkDiag = function () {
	var thiskey = "row" + this.y.toString() + "col" + this.x.toString();
	for (var mykey in window.grid) {
		if (window.grid[mykey].x === this.x && window.grid[mykey].y === this.y) {
			continue;
		}

		var deltax = window.grid[mykey].x - this.x;
		var deltay = window.grid[mykey].y - this.y;
		if(deltax === deltay){
			return false;
		}

	}

	return true;
};

Queen.prototype.checkAll = function(){
	return this.checkCol() && this.checkRow() && this.checkDiag();
};

function renderGrid(n) {
	var enclosingElement = document.createElement("div");
	for (var row = 0; row < n; row++) {
		var rowElement = document.createElement("div");
		$(rowElement).addClass("rows");
		$(enclosingElement).append(rowElement);
		for (var col = 0; col < n; col++) {
			var square = new Square(col, row);
			var squareElement = square.createElement();
			$(rowElement).append(squareElement);
		}
	}
	return enclosingElement;
}


(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		var grid = renderGrid(GRIDSIZE);
		$contentDiv.append(grid);
		$(".row3.col0").trigger("click");
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();



