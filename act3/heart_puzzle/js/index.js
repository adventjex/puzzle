$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);


});


var count = 1;
var number = 1;


$(document).on('click', '.pseudo-submit', function(){
	checkAnswer();
});

$(".pseudo-input").keyup(function(event){
    if(event.keyCode == 13){
        checkAnswer();
    }
});

$('.pseudo-submit').click(function(){
	checkAnswer();
});

function checkAnswer(){
	var value = $('.pseudo-input').val().toLowerCase();

	if (count == 1){

		if(value == "juliet"){
			nextHeart();
			
		}

	}

	else if (count == 2){

		if(value == "cleopatra"){
			nextHeart();
		}

	}

	else if (count == 3){

		if(value == "guinevere"){
			nextHeart();
		}

	}

	else if (count == 4){

		if(value == "eurydice"){
			nextHeart();
		}

	}

	else if (count == 5){

		if(value == "helena"){
			nextHeart();
		}

	}

	else if (count == 6){

		if(value == "josephine"){
			nextHeart();
			window.malum.checkAnswerAndCreateButton("heartache");
		}

	}


}

function nextHeart(){
	var animation = "animated" + count;
	$('.pulse').removeClass(animation);

	count++;
	number++;
	$('.number').html(number);
	var newAnimation = "animated"+count;
	$('.pulse').addClass(newAnimation);

	$('.pseudo-input').val('');
}