var ballArray = {"blueball": ".blueball", "redball": ".redball", "greenball": ".greenball", "yellowball": ".yellowball"};
var count = 0;
/* level area */
var blueArea = {"key1": 'redkey', "lock1": 'bluelock', "key2": 'yellowkey', "lock2": 'greenlock', "key3": 'yellowkey', "lock3": 'redlock'};
var redArea = {"key1": 'yellowkey', "lock1": 'greenlock', "key2": 'greenkey', "lock2": 'yellowlock', "key3": 'bluekey', "lock3": 'greenlock'};
var greenArea = {"key1": 'bluekey', "lock1": 'redlock', "key2": 'redkey', "lock2": 'bluelock', "key3": 'greenkey', "lock3": 'yellowlock'};
var yellowArea = {"key1": 'greenkey', "lock1": 'yellowlock', "key2": 'bluekey', "lock2": 'redlock', "key3": 'redkey', "lock3": 'bluelock'};
var levelArea = {"blueball": blueArea, "redball": redArea, "greenball": greenArea, "yellowball": yellowArea};
/* level area */

/* locks */
var blueLocks = ["bluelock", true, true, true];
var redLocks = ["redlock", true, true, true];
var greenLocks = ["greenlock", true, true, true];
var yellowLocks = ["yellowlock", true, true, true];

var keyToLock = {"bluekey": blueLocks, "redkey": redLocks, "greenkey": greenLocks, "yellowkey": yellowLocks};
var lockToLock = {"bluelock": blueLocks, "redlock": redLocks, "greenlock": greenLocks, "yellowlock": yellowLocks};
/* locks */

/* position */
var bluePosition = 50;
var redPosition = 50;
var greenPosition = 50;
var yellowPosition = 50;

var positionArray = {"blueball": bluePosition, "redball": redPosition, "greenball": greenPosition, "yellowball": yellowPosition};
/* position */

var win = "";

$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.level-content');
	var col = $('.col');
	var height = $(window).height();
	content.css('height', height);

	var blueSection = $('.blue-section');
	var redSection = $('.red-section');
	var greenSection = $('.green-section');
	var yellowSection = $('.yellow-section');

	var drawLevel = function(){

		for (var i in levelArea){
			if (levelArea.hasOwnProperty(i)){

				var k1 = levelArea[i].key1;
				if ("key1" in levelArea[i]){
					var kCode1 = '<div class="key firstkey '+k1+'"></div>';
				}
				else{
					var kCode1 = "";
				}

				var l1 = levelArea[i].lock1;
				if ("lock1" in levelArea[i]){
					var lCode1 = '<div class="lock firstlock '+l1+'"><span class="leftpin"></span><div class="middlebar"></div><span class="rightpin"></span></div>';
				}
				else{
					var lCode1 = "";
				}

				var k2 = levelArea[i].key2;
				if ("key2" in levelArea[i]){
					var kCode2 = '<div class="key secondkey '+k2+'"></div>';
				}
				else{
					var kCode2 = "";
				}

				var l2 = levelArea[i].lock2;
				if ("lock2" in levelArea[i]){
					var lCode2 = '<div class="lock secondlock '+l2+'"><span class="leftpin"></span><div class="middlebar"></div><span class="rightpin"></span></div>';
				}
				else{
					var lCode2 = "";
				}

				var k3 = levelArea[i].key3;
				if ("key3" in levelArea[i]){
					var kCode3 = '<div class="key thirdkey '+k3+'"></div>';
				}
				else{
					var kCode3 = "";
				}

				var l3 = levelArea[i].lock3;
				if ("lock3" in levelArea[i]){
					var lCode3 = '<div class="lock thirdlock '+l3+'"><span class="leftpin"></span><div class="middlebar"></div><span class="rightpin"></span></div>';
				}
				else{
					var lCode3 = "";
				}

				if(levelArea[i] == blueArea){
					blueSection.append(kCode1+lCode1+kCode2+lCode2+kCode3+lCode3);
				}
				else if(levelArea[i] == redArea){
					redSection.append(kCode1+lCode1+kCode2+lCode2+kCode3+lCode3);
				}
				else if(levelArea[i] == greenArea){
					greenSection.append(kCode1+lCode1+kCode2+lCode2+kCode3+lCode3);
				}
				else if(levelArea[i] == yellowArea){
					yellowSection.append(kCode1+lCode1+kCode2+lCode2+kCode3+lCode3);
				}
			}
		}
	}(); /* this draws the keys and locks based on the level area data object*/

});



$(document).on('click', '.down', function(){
	var ball = $(this).attr('id');
	moveBall(ball);
});

var moveBall = function(ball){
	var theball = $(ballArray[ball]); /* get the ball that is going to move*/
	var position = positionArray[ball]; /* get the ball's position */


	if(position == 50){ /* this is the ball's starting position */

		var index = 1; 
		position = 150;
		positionArray[ball] = 150;

		theball.animate(
			{
				top: position
			},
			{
				duration: 1000,
				complete: function(){
					hideLock(ball,index); /* hide the first level lock with the color of the key the ball passed thru*/
					moveBall(ball);
					
				}
			}
		);
	}

	else if (position == 150){

		//check if the first level lock is false
		var firstlock = levelArea[ball].lock1
		var lockstate = lockToLock[firstlock][1];

		if (lockstate == false){
			var index = 2; 
			position = 260;
			positionArray[ball] = 260;

			theball.animate(
				{
					top: position
				},
				{
					duration: 1000,
					complete: function(){
						hideLock(ball,index); /* hide the first level lock with the color of the key the ball passed thru*/
						moveBall(ball);
					}
				}
			);
		}

		else{
			return;
		}
	}

	else if (position == 260){

		//check if the first level lock is false
		var secondlock = levelArea[ball].lock2
		var lockstate = lockToLock[secondlock][2];

		if (lockstate == false){
			var index = 3; 
			position = 370;
			positionArray[ball] = 370;

			theball.animate(
				{
					top: position
				},
				{
					duration: 1000,
					complete: function(){
						hideLock(ball,index); /* hide the first level lock with the color of the key the ball passed thru*/
						moveBall(ball);
					}
				}
			);
		}

		else{
			return;
		}
	}

	else if (position == 370){

		//check if the first level lock is false
		var thirdlock = levelArea[ball].lock3
		var lockstate = lockToLock[thirdlock][3];

		if (lockstate == false){
			position = 500;
			positionArray[ball] = 500;

			theball.animate(
				{
					top: position
				},
				{
					duration: 1000,
					complete: function(){
						count++;
						win = win+ball;
						if (count == 4){
							checkAnswer(win);
						}
					}
				}
			);
		}

		else{
			return;
		}
	}	
}

var hideLock = function(ball,index){
	var area = levelArea[ball]; /* this is the level area we are interacting with */
	var keynumber = index.toString();
	var keystring = 'key'+keynumber;
	var keyObtained = area[keystring]; /* this is the key the ball passed thru */
	var lockType = keyToLock[keyObtained]; /* this the lock type we will interactive with from the key */

	if(lockType[index] == true){
		lockType[index] = false; /* set the lock from true to false */

		/* get the which lock depending on index*/
		if (index == 1){
			var toUnlock = $('.firstlock'+'.'+lockType[0]);
		}

		else if (index == 2){
			var toUnlock = $('.secondlock'+'.'+lockType[0]); 
		}

		else if (index == 3){
			var toUnlock = $('.thirdlock'+'.'+lockType[0]); 
		}
		
		toUnlock.animate(
			{
				opacity: 0
			},
			{
				duration: 1000,
				complete: function(){
					toUnlock.hide();
				}
			}
		);

	}
}

var checkAnswer = function(answer){
	check = "blueballredballgreenballyellowball";
	if (check == answer){
		window.malum.checkAnswerAndCreateButton("inorder");
	}

	else{
		location.reload();
	}
}
