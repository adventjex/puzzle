var canvas = document.getElementById('canvas');
var c = canvas.getContext('2d');

var chars = '100';
chars = chars.split('');
var fontsize = 16;

// fullscreen canvas
function resizeCanvas() {
  canvas.width = window.innerWidth;
  setTimeout(function() {
    canvas.height = window.innerHeight;
  }, 0);
};
window.onresize = resizeCanvas();
resizeCanvas();


var cols = canvas.width/fontsize;
var drops = [];
for(var x=0;x<cols;x++){
  drops[x]=1;
}

function draw(){
  c.fillStyle = 'rgba(0,0,0,0.05)';
  c.fillRect(0,0,canvas.width,canvas.height);
  c.fillStyle = '#333333';
  c.font = fontsize + 'px courier';
  
  for(var i=0;i<drops.length;i++){
    var txt = chars[Math.floor(Math.random()*chars.length)];
    c.fillText(txt,i*fontsize, drops[i]*fontsize);
    
    if(drops[i]*fontsize>canvas.height&&Math.random()>0.975){
      drops[i] = 0; 
    }
    drops[i]++;
  }
}

setInterval(draw, 32);