var userTotal = 0;
var times = 0;

var generateLevel = function(tiles,sets,total){

	var count = 0;
	var restart = total;
	var levelArea = $('.level-area');
	var array = []

	for(var j = 0; j < sets; j++){
		count = 0;

		for (var i = 0; i < tiles; i++){

				if(i == tiles-1){
					number = total - count;
					count = count+number;
				}

				else{
					var random = total - count;
					var number = Math.round(Math.random()*random/2);
					number = Math.ceil((number+1)/10)*10;			
					count = count + number;
				}
				array.push(number);
		}

	}

	shuffle(array);

	for (var k = 0; k < array.length; k++){
		levelArea.append('<p class="tile">'+array[k]+'</p>');
	}
}

function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

var selectTile = function(tile){
	tile.addClass('select');
	var number = parseInt(tile.text());
	userTotal = userTotal + number;

	if(userTotal > 100){
		$('.tile').removeClass('select');
		userTotal = 0;
		times = 0;
	}

	else if (userTotal == 100){
		$('.select').animate({
			opacity: 0
		}, 500);
		userTotal = 0;
		times++;

		if(times == 4){
			window.malum.checkAnswerAndCreateButton("perfectscore");
		}
	}

}




$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	generateLevel(4,4,100);

	var content = $('.content');
	window.service.verticalCenter(content);

	$('.tile').click(function(){
		var tile = $(this);
		selectTile(tile);
	})

	


});

// $(document).on('click', '.tile', function(){
// 	var tile = $(this);
// 	selectTile(tile);
// });



