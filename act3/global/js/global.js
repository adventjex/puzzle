function showCurrentLevel(){
    var levelWrapper = $('.level');
    levelWrapper.html('<p>' + window.globals.currentLevel +'</p>')
    levelWrapper.animate({
        opacity: "1",
        bottom: "20px"
    },2000);
    levelWrapper.show();
}

function showLevelContent(){
	var levelContent = $('.level-content');
	levelContent.animate({
		opacity: "1"
	},2000);
	levelContent.show();
}

service = {

    verticalCenter: function(element){
        function center(){
            var height = -1*element.height()/2;
            element.css('margin-top', height );
            element.css('top', '50%');
        }
        center();

        $(window).resize(function(){
            center();
        })


    }


};