var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

window.GRIDSIZE = 3;

Object.size = function (obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

function Knight(x, y, winCoord, color) {
	var that = this;
	this.x = x;
	this.y = y;
	this.winCoord = winCoord;
	this.color = color;
	this.element = this.generateElement();
	$(this.element).click(function(){
		that.clickHandler.call(that);
	});
	this.placeElement();
}

Knight.prototype.generateElement = function () {
	var imgsrc = "";
	if (this.color === "white") {
		imgsrc = "whiteknight.svg";
	} else if (this.color === "black") {
		imgsrc = "blackknight.svg";
	}
	var imgElement = document.createElement("img");
	$(imgElement).attr("src", imgsrc).addClass("knight");
	return imgElement;
};

Knight.prototype.placeElement = function () {
	var that = this;
	var selector = ".row" + this.y.toString() + ".col" + this.x.toString();
	$(selector).empty();
	$(selector).append(that.element);
};

Knight.prototype.calculateMoveable = function () {
	var moveable = [];
	//8 moveable squares // 4 pairs.
	var long_move = [-2, 2];
	var short_move = [-1, 1];
	var that = this;

	long_move.forEach(function (long) {
		short_move.forEach(function (short) {
			var newX = that.x + long;
			var newY = that.y + short;
			var movePosition = [newX, newY];
			if (newX >= window.GRIDSIZE || newX < 0 || newY >= window.GRIDSIZE || newY < 0) {
				return;
			} else if(!that.checkIfOccupied(newX, newY)) {
				moveable.push(movePosition);
			}
		});
	});

	long_move.forEach(function (long) {
		short_move.forEach(function (short) {
			var newX = that.x + short;
			var newY = that.y + long;
			var movePosition = [newX, newY];
			if (newX >= window.GRIDSIZE || newX < 0 || newY >= window.GRIDSIZE || newY < 0) {
				return;
			} else if(!that.checkIfOccupied(newX, newY)){
				moveable.push(movePosition);
			}
		});
	});

	return moveable;

};

Knight.prototype.highlightMoveable = function(){
	var moves = this.calculateMoveable();
	$(".square").removeClass("highlight");
	for(var i = 0; i<moves.length; i++){
		var selector = ".row" + moves[i][1].toString() + ".col" + moves[i][0].toString();
		$(selector).addClass("highlight");
	}
};

Knight.prototype.checkWin = function(){
	for(var i = 0; i<this.winCoord.length; i++){
		if(this.x === this.winCoord[i][0] && this.y === this.winCoord[i][1]){
			return true;
		}
	}
	return false;
};

Knight.prototype.checkAllWin = function(){
	for(var i = 0; i<window.knights.length; i++){
		if(!window.knights[i].checkWin()){
			return false;
		}
	}
	return true;
};

Knight.prototype.prepareMove = function(){
	var that = this;
	var moves = this.calculateMoveable();
	//$(".square").click(function(){
	//	$(".square").removeClass("highlight").off();
	//});
	moves.forEach(function(move){
		var selector = ".row"+move[1].toString()+".col"+move[0].toString();
		$(selector).off().click(function(){
			$(this).append(that.element);
			$(".square").off().removeClass("highlight");
			that.x = move[0];
			that.y = move[1];
			if(that.checkAllWin()){
				window.malum.checkAnswerAndCreateButton("reflection");
			}

		})
	});
};

Knight.prototype.checkIfOccupied = function(x, y){
	for(var i = 0; i<window.knights.length; i++){
		if(x === window.knights[i].x && y === window.knights[i].y){
			return true;
		}
	}
	return false;
};

Knight.prototype.clickHandler = function () {
	this.highlightMoveable();
	this.prepareMove();
};


function Square(x, y) {
	this.x = x;
	this.y = y;
}

Square.prototype.createElement = function () {
	var that = this;
	var squareElement = document.createElement("span");
	$(squareElement)
		.addClass("row" + this.y.toString())
		.addClass("col" + this.x.toString())
		.addClass("square");
	return squareElement;
};


function renderGrid(n) {
	var enclosingElement = document.createElement("div");
	for (var row = 0; row < n; row++) {
		var rowElement = document.createElement("div");
		$(rowElement).addClass("rows");
		$(enclosingElement).append(rowElement);
		for (var col = 0; col < n; col++) {
			var square = new Square(col, row);
			var squareElement = square.createElement();
			$(rowElement).append(squareElement);
		}
	}
	return enclosingElement;
}


(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		var grid = renderGrid(GRIDSIZE);
		$contentDiv.append(grid);
		window.knights = [];
		knights.push(new Knight(0, 0, [[2,0],[2,2]], "white"));
		knights.push(new Knight(0, 2, [[2,0],[2,2]], "white"));
		knights.push(new Knight(2, 0, [[0,0],[0,2]], "black"));
		knights.push(new Knight(2, 2, [[0,0],[0,2]], "black"));

		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();



