

$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);
	$('.reset').hide();

	$('.reset').click(function(){
		location.reload();
	});

});


$(document).on('click', ".start", function(){
	$('.start').hide();
	$('.hint').show();
	$('.reset').show();
	circleAndCounter(1,1,1);
});

var circleAndCounter = function(count,power,circles){
	var counter = $('#counter');
	var area = $('.area');

	if (count < 12){


		if(power < 11){

			counter.html(count);
			count = count + 1;

			for (var i = 0; i < circles; i++){

				var left = Math.round(Math.random()*90);
				var top = Math.round(Math.random()*90);
				area.append('<span class="circle" style="top:'+top+'%;left:'+left+'%;"></span>');


			}
			circles = circles * 2;
			power = power+1;
		}

		else{
			area.css('background', '#221e20');
			counter.html(count);
			count = count + 1;

		}

		setTimeout(function(){
			circleAndCounter(count,power,circles);
		},3000);

	}

	else if (count > 11 && count < 101){
		counter.html(count);
		count = count + 1;

		setTimeout(function(){
			circleAndCounter(count,power,circles);
		},100);
	}
}

