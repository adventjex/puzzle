var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

function Grid(data) {
	this.data = data;
	this.cols = data[0].length;
	this.rows = data.length;
	this.element = this.generateGridElement();
}


Grid.prototype.generateGridElement = function () {
	var that = this;
	var tableElement = document.createElement("table");
	for (var i = 0; i < this.rows; i++) {
		var rowElement = document.createElement("tr");
		$(tableElement).append(rowElement);
		for (var j = 0; j < this.cols; j++) {
			var colElement = document.createElement("td");
			var rowClassName = "row" + i.toString();
			var colClassName = "col" + j.toString();
			var data = that.data[i][j];
			$(colElement)
				.addClass(rowClassName)
				.addClass(colClassName)
				.addClass("creatures")
				.addClass(data)
				.html(data);
			that.select(colElement);
			$(rowElement).append(colElement);
		}

	}
	return tableElement;
};

Grid.prototype.select = function (element) {
	var that = this;
	$(element).click(function () {
		var identicalClasses = "." + $(this).html();
		if (!$(this).hasClass("selected")) {
			$(identicalClasses).addClass("selected");
		} else {
			$(identicalClasses).removeClass("selected");
		}
		var isDataCorrect = that.checkDataForCorrectness();
		if(isDataCorrect){
			window.malum.checkAnswerAndCreateButton("gluttony");
		}
	});
};

Grid.prototype.checkDataForCorrectness = function () {
	var that = this;
	for (var i = 0; i < that.data.length; i++) {
		var selected = 0;
		for (var j = 0; j < that.data[0].length; j++) {
			var rowClassName = ".row" + i.toString();
			var colClassName = ".col" + j.toString();
			var element = $(rowClassName + colClassName);
			if ($(element).hasClass("selected")) {
				selected++;
			}
		}
		if (selected !== 1) {
			return false;
		}
	}
	return true;
};


(function () {
	init();


	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		var data = [
			["A", "G", "L", "P", "H"],
			["X", "D", "L", "Z", "I"],
			["B", "O", "M", "R", "H"],
			["C", "D", "T", "P", "J"],
			["B", "O", "L", "Z", "K"],
			["A", "G", "M", "P", "H"],
			["C", "D", "L", "E", "Q"],
			["A", "G", "M", "Z", "H"]
		];

		var grid = new Grid(data);
		$contentDiv.append(grid.element);
		//window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();


