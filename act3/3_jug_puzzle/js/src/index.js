var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

window.winAmount = 6;

function resetElements(){
	window.jugs.forEach(function(jug){
		$(jug.element).remove();
		$(".content").append(jug.element);
	});
}

function checkAmountOfWater(){
	var passed = 0;
	for(var i = 0; i<window.jugs.length; i++){
		if(window.jugs[i].currentWater === window.winAmount){
			passed++;
		}
	}
	return passed === 2;
}

function renableClickForJugs() {
	$(".jugs").off();
	window.jugs.forEach(function (jug) {
		$(jug.element).click(function () {
			jug.handleClick.call(jug);
		});
	})
}

function Jug(maxWater, currentWater, jugHeight, jugWidth, jugBorder, id) {
	var that = this;
	this.id = id;
	this.maxWater = maxWater;
	this.currentWater = currentWater;
	this.jugHeight = jugHeight;
	this.jugWidth = jugWidth;
	this.jugBorder = jugBorder;
	this.element = this.renderElement();
	$.data(this.element, "jugObject", this);
	$(".content").append(this.element);
}

Jug.prototype.renderElement = function () {
	var enclosingSpan = document.createElement("span");
	$(enclosingSpan).addClass("enclosingJug").width(this.jugWidth).height(this.jugHeight);
	var textElement = document.createElement("p");
	$(textElement).html(this.maxWater.toString() + " L");
	//var drainButton = this.createDrainButton();
	var element = document.createElement("span");
	$(element).addClass("jug").width(this.jugWidth).height(this.jugHeight);
	$(element).css({
			"border-bottom": "solid #808080 " + this.jugBorder + "px",
			"border-left": "solid #808080 " + this.jugBorder + "px",
			"border-right": "solid #808080 " + this.jugBorder + "px"
		}
	);
	;
	var water = document.createElement("div");
	$(water).addClass("water");
	var waterHeight = this.getWaterHeight();
	$(water).height(Math.floor(waterHeight)).css({
		position: "absolute"
	}).width(this.jugWidth - (2 * this.jugBorder));
	$(element).append(water);
	$(enclosingSpan).append(element).append(textElement);
	//$(enclosingSpan).append(drainButton);
	return enclosingSpan;
};

Jug.prototype.drain = function () {
	this.currentWater = 0;
	this.updateRenderWater();
};

Jug.prototype.fillAnimation = function (callback) {
	var that = this;
	var position = $(this.element).position();
	var height = $(this.element).height();
	var width = $(this.element).width();

	var faucetPosition = $(".faucet").position();
	var faucetHeight = $(".faucet").height();
	var faucetWidth = $(".faucet").width();

	$(this.element).css({
		position: "absolute",
		left: position.left,
		top: position.top

	});
	var waterStream = document.createElement("div");
	$(waterStream).css({
		"width": "2%",
		"position": "absolute",
		left: faucetPosition.left + faucetWidth * 0.87,
		top: faucetPosition.top + faucetHeight * 0.95,
		height: height,
		"background-color": "#0040D0"

	});


	$(this.element).animate({
		left: faucetPosition.left + faucetWidth * 0.5,
		top: faucetPosition.top + faucetHeight
	}, {
		complete: function () {
			$(".content").prepend(waterStream);
			that.updateRenderWater(function () {
				$(waterStream).remove();
				$(that.element).animate({
					left: position.left,
					top: position.top
				}, {
					"complete":function(){
						$(that.element).css({
							"display": "inline-block",
							"left":"auto",
							"top":"auto",
							"position": "static"
						});

					}
				})
			});
		}
	});
};

Jug.prototype.fill = function (callback) {
	this.currentWater = this.maxWater;
	this.fillAnimation(callback);
};

Jug.prototype.createDrainButton = function () {
	var that = this;
	var button = document.createElement("button");
	$(button).html("Drain");
	$(button).click(function (e) {
		e.stopPropagation();
		that.drain();
	}).addClass("drain");
	return button;
};


Jug.prototype.handleClick = function () {
	var that = this;
	var $jug = $(".enclosingJug");
	$jug.removeClass("glow");
	$jug.off();
	$(this.element).addClass("glow");
	$jug.click(function () {
		var otherJug = $.data(this, "jugObject");
		that.moveWater.call(that, otherJug);
		$jug.off();
		$jug.removeClass("glow");
		renableClickForJugs();
	});
	var $faucet = $(".faucet").off();
	$faucet.click(function () {
		$jug.removeClass("glow");
		that.fill();
		$jug.off();
		renableClickForJugs();
	});


};


Jug.prototype.getWaterHeight = function () {
	var jugHeight = $(this.element).height();
	return (this.currentWater / this.maxWater) * this.jugHeight - 2 * this.jugBorder;
};


Jug.prototype.updateRenderWater = function (callback) {
	var that = this;
	$(this.element).children().children().animate({
		height: that.getWaterHeight()
	}, {
		complete: function () {
			if (callback) {
				callback();
			}
			if(checkAmountOfWater()){
				window.malum.checkAnswerAndCreateButton("threeejugs");
			}
		}
	});
};

Jug.prototype.moveWater = function (otherJug) {
	var moveable = otherJug.maxWater - otherJug.currentWater;
	moveable = moveable < this.currentWater ? moveable : this.currentWater;
	this.currentWater = this.currentWater - moveable;
	otherJug.currentWater = otherJug.currentWater + moveable;
	this.updateRenderWater();

	otherJug.updateRenderWater();


};


(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		window.jugs = [];
		jugs.push(new Jug(12, 12, 200, 80, 4, 0));
		jugs.push(new Jug(8, 0, 200 * (8 / 12), 80, 4));
		jugs.push(new Jug(5, 0, 200 * (5 / 12), 80, 4));
		renableClickForJugs();
	}
})();



