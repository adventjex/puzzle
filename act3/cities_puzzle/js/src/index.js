var _ = require('underscore');
var md5 = require("blueimp-md5").md5;


function checkIfAllCitiesAreCorrect(cities) {
	for (var i = 0; i < cities.length; i++) {
		if (!cities[i].isCorrect()) {
			return false;
		}
	}
	return true;
}

function generateButtonIfAnswerIsCorrect(cities) {
	if (checkIfAllCitiesAreCorrect(cities)) {
		window.malum.checkAnswerAndCreateButton("usborne");
	}
}

function City(x, y, choices, correctChoice) {
	this.choices = choices;
	this.currentChoice = 0;
	this.correctChoice = correctChoice;
	this.y = y;
	this.x = x;

	this.placeElement();
};

City.prototype.isCorrect = function () {
	return this.currentChoice === this.correctChoice;
};

City.prototype.placeElement = function () {
	var selector = ".row" + this.y + ".col" + this.x;
	$(selector).append(this.createSelector());
};

City.prototype.createSelector = function () {
	var that = this;

	var enclosingElement = document.createElement("span");

	var imageElement = document.createElement("img");
	$(imageElement).attr('src', 'town-building-5997-large.png');
	$(imageElement).css({
		"display": "inline-block",
		"width": 50
	});

	var choiceElement = document.createElement("span");
	$(choiceElement).addClass("choice");
	var selectedElement = document.createElement("span");
	$(selectedElement).html(this.choices[this.currentChoice]);

	var leftIconElement = document.createElement("i");
	$(leftIconElement).addClass("fa fa-arrow-left");
	$(leftIconElement).click(function () {
		that.cycleChoicesLeft();
		$(selectedElement).html(that.choices[that.currentChoice]);
	});

	var rightIconElement = document.createElement("i");
	$(rightIconElement).addClass("fa fa-arrow-right");
	$(rightIconElement).click(function () {
		that.cycleChoicesRight();
		$(selectedElement).html(that.choices[that.currentChoice]);
	});

	$(enclosingElement).append(imageElement);
	$(enclosingElement).append(choiceElement);
	$(choiceElement).append(leftIconElement);
	$(choiceElement).append(selectedElement);
	$(choiceElement).append(rightIconElement);

	return enclosingElement;

};

City.prototype.cycleChoicesRight = function () {
	var newChoice = this.currentChoice + 1;
	if (newChoice >= this.choices.length) {
		newChoice = 0;
	}
	this.currentChoice = newChoice;
	generateButtonIfAnswerIsCorrect(window.cities);
};

City.prototype.cycleChoicesLeft = function () {
	var newChoice = this.currentChoice - 1;
	if (newChoice < 0) {
		newChoice = this.choices.length - 1;
	}
	this.currentChoice = newChoice;
	generateButtonIfAnswerIsCorrect(window.cities);
};


(function () {


	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		//window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);

		var info = [[0, 0, 6], [1, 0, 2], [1, 1, 4], [2, 1, 1], [0, 2, 3], [1, 2, 5], [2, 2, 7]];
		//var info = [[0, 0, 0], [1, 0, 0], [1, 1, 0], [2, 1, 0], [0, 2, 0], [1, 2, 0], [2, 2, 0]];

		var choices = [
			"None",
			"The Hacienda",
			"Tropolis",
			"Arcandia",
			"Dena City",
			"Midgar",
			"Apple County",
			"Sunrise City"
		];
		window.cities = [];
		for (var i = 0; i < info.length; i++) {
			var city = new City(info[i][0], info[i][1], choices, info[i][2]);
			window.cities.push(city);
		}


	}
})();





