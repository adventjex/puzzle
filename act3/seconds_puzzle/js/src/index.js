var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();


	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();

window.startDate = new Date();
window.startYear = window.startDate.getFullYear();
window.tenYearsLater = window.startYear + 10;
window.futureDate = new Date(window.tenYearsLater, 1, 1, 0, 0, 0, 0);
window.timeReached = false;
setTimeout(function() {
	function recurse() {
		var date = new Date();
		var month = date.getMonth();
		var day = date.getDate();
		var year = date.getFullYear();
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var seconds = date.getSeconds();
		var timeleft = Math.floor((window.futureDate.getTime() - date.getTime())/1000);
		if(timeleft <= 0 && window.timeReached === false){
			window.malum.checkAnswerAndCreateButton("timeout");
			window.timeReached = true;
		}
		$(".current_time").html("current time: " + month + "/" + day + "/" + year + " " + hours + ":" + minutes + ":" + seconds);
		$(".time_left").html("Seconds left until next level is unlocked: " + timeleft + "s");
		setTimeout(recurse, 1000);
	}
	recurse();

}, 1000);



