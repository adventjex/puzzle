var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function placePuzzle(element, sequences) {
		for (var i = sequences.length-1; i > -1; i--) {
			var spans = generateSpansForStrings(sequences[i]);
			var $div = $(document.createElement("div")).addClass("numbers");
			for (var j = 0; j < spans.length; j++) {
				$div.append(spans[j]);
			}
			$(element).prepend($div);
		}
	}

	function generateSpansForStrings(arrayOfStrings) {
		return _.reduce(arrayOfStrings, function (collector, string) {
			var $span = $(document.createElement("span")).append(string);
			collector.push($span);
			return collector;
		}, [])
	}

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		var sequences = [
			["0", "4", "4", "4", "4", "4"],
			["1", "3", "5", "4", "4", "4"],
			["2", "3", "5", "4", "4", "4"],
			["3", "5", "4", "4", "4", "4"],
			["4", "4", "4", "4", "4", "4"],
			["5", "4", "4", "4", "4", "4"],
			["6", "3", "5", "4", "4", "4"],
			["7", "5", "4", "4", "4", "4"],
			["8", "5", "4", "4", "4", "4"],
			["9", "4", "4", "4", "4", "4"],
			["10", "_", "_", "_", "_", "_"]
		]
		placePuzzle($contentDiv, sequences);

	}


})();



