var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function mergeIterableIntoArray (string1, string2){
		var overallLength = string1.length > string2.length ? string1.length : string2.length;
		var result = [];
		for(var i = 0; i<overallLength; i++){
			if(string1[i]){
				result.push(string1[i]);
			} else {
				result.push(" ");
			}

			if(string2[i]){
				result.push(string2[i]);
			} else {
				result.push(" ");
			}
		}
		return result;
	}

	function divideSequences (length, sequence) {
		var segments = Math.floor(sequence.length/length);
		var result = [];
		for(var i = 0; i<segments; i++){
			result.push(sequence.slice(i*length, i*length+length));
		}
		if(sequence.length % length > 0) {
			var remainingSlice = augmentSegment(length, sequence.slice(segments * length));
			result.push(remainingSlice);
		}
		return result;
	}

	function augmentSegment(length, segment){
		if (segment.length >= length){
			return segment;
		} else {
			for(var i = segment.length; i <length; i++){
				segment.push(" ");
			}
		}
		return segment;
	}

	function createCodeGrid(length, string1, string2){
		var mergedStringsArray = mergeIterableIntoArray(string1, string2);
		var sequences = divideSequences(length, mergedStringsArray)
		return sequences;
	}

	function encodeFourCodeGrid(length, string1, string2, string3, string4){
		var codegrid1 = createCodeGrid(length, string1, string2);
		var codegrid2 = createCodeGrid(length, string3, string4);
		var mergedCodeGrid = mergeIterableIntoArray(codegrid1, codegrid2);
		return mergedCodeGrid;

	}

	function placePuzzle(element, sequences) {
		for (var i = sequences.length-1; i > -1; i--) {
			var spans = generateSpansForStrings(sequences[i]);
			var $div = $(document.createElement("div")).addClass("numbers");
			for (var j = 0; j < spans.length; j++) {
				$div.append(spans[j]);
			}
			$(element).prepend($div);
		}
	}

	function generateSpansForStrings(arrayOfStrings) {
		return _.reduce(arrayOfStrings, function (collector, string) {
			var $span = $(document.createElement("span")).append(string);
			collector.push($span);
			return collector;
		}, [])
	}

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		//window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		var stanza1 = "When I'm young I'm tall".toUpperCase();
		var stanza2 = "When I'm old I'm short".toUpperCase();
		var stanza3 = "When I'm alive I glow".toUpperCase();
		var stanza4 = "Because of your breath I die".toUpperCase();

		var sequences = encodeFourCodeGrid(20, stanza1, stanza2, stanza3, stanza4);
		placePuzzle($contentDiv, sequences);
		var $image = $(".image");
		$image.pep();

	}


})();



