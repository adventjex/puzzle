function showCurrentLevel(){
    var levelWrapper = $('.level');
    levelWrapper.html('<p>' + window.globals.currentLevel +'</p>')
    levelWrapper.animate({
        opacity: "1",
        bottom: "20px"
    },2000);
    levelWrapper.show();
}

function showLevelContent(){
	var levelContent = $('.level-content');
	levelContent.animate({
		opacity: "1"
	},2000);
	levelContent.show();
}

service = {

    verticalCenter: function(element){
        function center(){
            var height = -1*element.height()/2;
            element.css('margin-top', height );
            element.css('top', '50%');
        }
        center();

        $(window).resize(function(){
            center();
        })
    },

    runClueTicker: function(clue){
        setTimeout(function(){
            $('body').append('<div class="ticker"><span class="ticker-content">' + clue + '</span></div>');
            var ticker = $('.ticker');

            ticker.animate(
                {
                    opacity: 0.5
                },
                {
                    duration: 1000
                }
            );

            setTimeout(function(){
               ticker.animate(
                    {
                        opacity: 0
                    },
                    {
                        duration: 3000,
                        complete: function(){
                            ticker.find('.ticker-content').addClass('ticker-fix');
                            ticker.find('.ticker-content').removeClass('ticker-content');
                        }
                    });
            },5500);

        },2000);


        
    }


};
