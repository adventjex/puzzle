$(document).ready(function(){

	showCurrentLevel();
	showLevelContent();


	var setColHeight = function(){
		var windowSize = $(window).height();
		var col = $('.col');
		col.each(function(){
			$(this).css('height', windowSize);
		});
	}

	var dualViewFunctions = {
		answerA: ["a2", "a3", "a1", "a5", "a4"],
		answerB: ["b5", "b3", "b4", "b1", "b2"],
		button: $('.circles').find('span'),
		positionA: 0,
		positionB: 0,
		completed: 0,

		getButtonClicked: function getButtonClicked(){
			var self = this;
			
			self.button.click(function(){
				var column = $(this).closest('.circles');
				var button = $(this);

				if (column.hasClass('left-group')){
					self.checkAnswer(self.answerA,self.positionA,button, "left");
				}

				else if (column.hasClass('right-group')){
					self.checkAnswer(self.answerB,self.positionB,button, "right");
				}
			});
		},

		checkAnswer: function checkAnswer(answer,position,button,column){
			var self = this;
			var id = button.attr('id');
			var correctAnswer = answer[position];

			if (column == "left"){
				if(id == correctAnswer){
					self.positionA++;

					if(self.positionA == 5){
						var side = $('.col');
						self.completeSection(side, column);
					}
				}

				else{
					self.positionA = 0;
				}

			}

			else if (column == "right"){
				if(id == correctAnswer){
					self.positionB++;

					if(self.positionB == 5){
						var side = $('.col');
						self.completeSection(side,column);
					}
				}

				else{
					self.positionB = 0;
				}
			}
		},

		completeSection: function completeSection(side, column){
			var self = this;

			if (column == "left"){
				$('.left').find('span').each(function(){
					$(this).css('background', '#ffffff');
				});
				$('.left').css('opacity', '0.15');
				$('.left-group').removeClass('left-group');
				self.completed++;
				self.checkIfAllDone(self.completed);
			}

			else if (column == "right"){
				$('.right').find('span').each(function(){
					$(this).css('background', '#ffffff');
				});
				$('.right').css('opacity', '0.15');
				$('.right-group').removeClass('left-group');
				self.completed++;
				self.checkIfAllDone(self.completed);
			}
		},

		checkIfAllDone: function checkIfAllDone(completed){
			if (completed == 2){
				window.malum.checkAnswerAndCreateButton("takesides");
			}
		},

		init: function init(){
			this.getButtonClicked();
		}
	}

	function init(){
		setColHeight();
		dualViewFunctions.init();
	}
	init();



});

