var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function placePuzzle(element, sequences) {
		for (var i = sequences.length-1; i > -1; i--) {
			var spans = generateSpansForStrings(sequences[i]);
			var $div = $(document.createElement("div")).addClass("numbers");
			for (var j = 0; j < spans.length; j++) {
				$div.append(spans[j]);
			}
			$(element).prepend($div);
		}
	}

	function generateSpansForStrings(arrayOfStrings) {
		return _.reduce(arrayOfStrings, function (collector, string) {
			var $span = $(document.createElement("span")).append(string);
			collector.push($span);
			return collector;
		}, [])
	}

	//function isalpha(c) {
    	//return (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')));
  	//}
	//
	//function convertPanagramToCipher(panagram){
	//	var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//	var cipherString = _.reduce(panagram, function(collector, letter){
	//		if(isalpha(letter)){
	//			return collector+letter.toUpperCase();
	//		} else {
	//			return collector;
	//		}
	//	},"");
	//	return _.reduce(_.zip(alphabet, cipherString), function(collector, cipherUnit){
	//		var cipherUnitObject = {};
	//		cipherUnitObject[cipherUnit[0]] = cipherUnit[1];
	//		return _.extend(collector, cipherUnitObject);
	//	},{});
	//}
	//
	//function ceasarCipherLetter(letter, translator){
	//	return translator[letter];
	//}
	//
	//function ceasarCipherMessage(message, key){
	//	var translator = convertPanagramToCipher(key);
	//	return _.reduce(message, function(collector, letter){}) TODO
	//}
	//
	//function convertPanagramToDecipher(panagram) {
	//	var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	//	var cipherString = _.reduce(panagram, function(collector, letter){
	//		if(isalpha(letter)){
	//			return collector+letter.toUpperCase();
	//		} else {
	//			return collector;
	//		}
	//	},"");
	//	return _.reduce(_.zip(alphabet, cipherString), function(collector, cipherUnit){
	//		var cipherUnitObject = {};
	//		cipherUnitObject[cipherUnit[1]] = cipherUnit[0];
	//		return _.extend(collector, cipherUnitObject);
	//	},{});
	//}

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		var message = "The third time's a charm.";
		var pangram = "MR. JOCK, TV QUIZ PHD BAGS FEW LYNX.";
		var alphabet= "AB  CDEF  GH IJKL MNO PQRX TUV WXYZ ";


		var sequences = [
			pangram, alphabet
		];
		placePuzzle($contentDiv, sequences);
		$(".message").html(message);

	}
	//GFDHV

})();



