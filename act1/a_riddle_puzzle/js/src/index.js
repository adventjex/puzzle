var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		$(document).ready(function () {
			$(".reversed").
			each(function () {
				$(this).transition({
					scale: [-1, 1]
				}, 3000, "ease");
			});
		});
	}
})();



