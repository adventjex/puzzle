$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var lightSwitch = $('.light-switch');
	var state1 = false;
	var state2 = false;
	var state3 = false;
	var message = $('.message');
	var off = $('.off-btn');

	var main = $('.main');
	var litMessage = $('.lit-message');

	lightSwitch.click(function(){
		var bulb = $(this).closest('.lamp-wrapper').find('.bulb')
		bulb.toggleClass('light');

		var bulbNumber = bulb.attr('data');
		if (bulbNumber == "state1"){
			state1 = !state1;
		}

		if (bulbNumber == "state2"){
			state2 = !state2;
		}

		if (bulbNumber == "state3"){
			state3 = !state3;
		}

		if(!state1 && !state2 && !state3){
			message.html('&nbsp;');
		}

		else if(state1 && !state2 && !state3){
			message.html('1. Jennifer');
		}

		else if(state2 && !state1 && !state3){
			message.html('1. will die');
		}

		else if (state3 && !state1 && !state2){
			message.html("2. That's what");
		}

		else if (state1 && state2 && !state3){
			message.html("2. we decided");
		}

		else if (state1 && state3 && !state2){
			message.html("3. June");
		}

		else if (state2 && state3 && !state1){
			message.html("3. will live on");
		}

		else if(state1 && state2 && state3){
			main.addClass('lit');
			litMessage.show();
			setTimeout(function(){
				main.removeClass('lit');
				litMessage.hide();
				turnOff();
			},2500);

		}

	});

	function turnOff(){
		state1 = false;
		state2 = false;
		state3 = false;
		message.html('&nbsp;');
		$('.bulb').each(function(){
			$(this).removeClass('light');
		})
	}

	off.click(function(){
		turnOff();
	});


});

