var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var startIndex = 0;
		var currentIndex = startIndex;
		var $numberDiv = $('.numbers');
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);

		var puzzleArray = [3, 6, 4, 1, 3, 4, 2, 4, 3, 0];
		var puzzleSpans = generateSpansForStrings(puzzleArray, "puzzle");
		var leftControlElement = createControlElement("fa fa-chevron-left", function () {
			console.log("left");
			var newIndex = moveLeft(currentIndex, puzzleArray);
			if (newIndex) {
				moveFromElement(puzzleSpans[currentIndex], puzzleSpans[newIndex]);
				currentIndex = newIndex;
				if (isGameWon(currentIndex, puzzleArray)){
					window.malum.checkAnswer("next-puzzle");
				}
			}
		});
		var rightControlElement = createControlElement("fa fa-chevron-right", function () {
			console.log("right");
			var newIndex = moveRight(currentIndex, puzzleArray);
			console.log(newIndex);
			if (newIndex) {
				moveFromElement(puzzleSpans[currentIndex], puzzleSpans[newIndex]);
				currentIndex = newIndex;
				if (isGameWon(currentIndex, puzzleArray)){
					window.malum.checkAnswerAndCreateButton("next-puzzle");
				}
			}
		});

		puzzleSpans.forEach(function (element) {
			$numberDiv.append(element);
		});
		$numberDiv.append(rightControlElement);
		$numberDiv.prepend(leftControlElement);

		//initializeGame
		$(puzzleSpans[startIndex]).transition({
			scale:[2,2]
		});

		$(puzzleSpans[puzzleSpans.length-1]).addClass("red");

		$(".reset").click(function(){
			currentIndex = reset(startIndex, currentIndex, puzzleSpans);
			console.log("reset");
		})


	}

	function reset (startIndex, currentIndex, arrayOfElements){
		moveFromElement(arrayOfElements[currentIndex], arrayOfElements[startIndex]);
		return startIndex;
	}

	function moveFromElement(element, newElement) {
		$(element).transition({
			scale: [1, 1]
		});
		$(newElement).transition({
			scale: [2, 2]
		})
	}

	function createControlElement(css_class, callback) {
		var $span = $(document.createElement("span"));
		var $icon = $(document.createElement("i"));
		$icon.addClass(css_class);
		$icon.click(callback);
		$span.append($icon);
		return $span;
	}

	function moveLeft(currentIndex, puzzleArray) {
		var currentMoves = puzzleArray[currentIndex];
		var newIndex = currentIndex - currentMoves;
		if (newIndex < 0) {
			return false;
		} else {
			return newIndex;
		}
	}

	function moveRight(currentIndex, puzzleArray) {
		var currentMoves = puzzleArray[currentIndex];
		var newIndex = currentIndex + currentMoves;
		if (newIndex >= puzzleArray.length) {
			return false;
		} else {
			return newIndex;
		}
	}

	function isGameWon(currentIndex, puzzleArray) {
		return currentIndex === (puzzleArray.length - 1);
	}

	function generateSpansForStrings(arrayOfStrings, classString) {
		return _.reduce(arrayOfStrings, function (collector, string) {
			var $span = $(document.createElement("span")).append(string)
			if (classString) {
				$span.addClass(classString);
			}
			collector.push($span);
			return collector;
		}, [])
	}
})();



