/**
 * Created by brian on 3/30/15.
 */

var _ = require("underscore");

$(document).ready(function () {
	var $content = $(".content");
	showCurrentLevel();
	showLevelContent();

	var width = 320;
	var height = 320;
	var rows = 10;
	var cols = 10;
	var paper = Raphael($(".content")[0], width, height);
	$("svg").attr("class", "ball_game");
	paper.setViewBox(0, 0, width, height);
	var gameGrid = new Grid(width, height, rows, cols);
	var isWinGenerated = false;
	//var test = new Reflector(0,0,true,paper, gameGrid);
	//var test2 = new Reflector(4,0,false,paper, gameGrid);
	//var test3 = new Reflector(4,4,true, paper, gameGrid);
	//var reflectors = generateHashObjectFromArrayOfReflectors([test, test2, test3]);
	//setTimeout(function recurse (){
	//	new Ball(0,4,3,paper, gameGrid, reflectors, function(){});
	//	setTimeout(recurse, 1000);
	//}, 1000);

	var startX = Math.floor(rows / 2);
	var startY = Math.floor(rows / 2);
	//var startDirection = getRandomInt(0,4);
	var startDirection = 1;
	var startDepth = rows * cols;
	var reflectors = {};
	var pathBeenTo = {};

	//generatePuzzleReflectors(startX,startY,paper,gameGrid,startDirection,startDepth,reflectors,pathBeenTo,true, 20);
	generateSpecificPuzzle(paper, gameGrid, reflectors);

	function generateSpecificPuzzle(paper, grid, reflectors) {
		var reflectorsArray = Array();
		reflectorsArray.push(new Reflector(5, 9, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(7, 9, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(7, 8, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(4, 8, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(4, 9, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(1, 9, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(1, 7, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(0, 7, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(0, 8, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(3, 8, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(3, 2, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(2, 2, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(2, 6, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(4, 6, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(4, 5, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(0, 5, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(0, 6, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(1, 6, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(1, 1, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(0, 1, getRandomTrueFalse(), paper, grid, true));
		reflectorsArray.push(new Reflector(0, 4, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(6, 4, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(6, 1, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(5, 1, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(5, 3, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 3, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 0, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(1, 0, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 7, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(3, 7, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(8, 1, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(8, 2, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 2, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 1, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(6, 2, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(0, 0, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(4, 1, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 5, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 4, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(8, 4, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(8, 6, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 6, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(7, 6, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(7, 4, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(2, 7, getRandomTrueFalse(), paper, grid, false));
		reflectorsArray.push(new Reflector(9, 8, getRandomTrueFalse(), paper, grid, false));

		for(var i = 0; i<reflectorsArray.length; i++){
			reflectors[reflectorsArray[i].gridX.toString()+" "+reflectorsArray[i].gridY.toString()] = reflectorsArray[i];
		}
		return reflectors;
	}


	function createACallOnceWrapper() {
		return function (toCall) {
			var called = false;
			return function () {
				if (!called) {
					called = true;
					toCall.apply(this, arguments);
				}
				return;
			}
		}
	}

	var callOnce = createACallOnceWrapper();

	var checkAnswerAndCreateButtonOnce = callOnce(window.malum.checkAnswerAndCreateButton);

	var winHandler = function (x, y, reflectors) {
		var key = x.toString() + " " + y.toString();
		if (reflectors[key] && reflectors[key].isGoal) {
			checkAnswerAndCreateButtonOnce("blindMan");
			return true;
		} else {
			return false;
		}
	}

	var ballGun = new BallGenerator(startX, startY, startDirection, 300, paper, gameGrid, reflectors, winHandler, 60000);


	function BallGenerator(startX, startY, direction, interval, paper, grid, reflectors, handle, ballLifeTime) {
		var that = this;
		that.balls = Array();
		that.isOn = false;
		that.renderObject = paper.rect(
			grid.getXFromIndex(startX) - grid.colWidth() / 2,
			grid.getYFromIndex(startY) - grid.rowHeight() / 2, grid.colWidth(), grid.rowHeight()).attr({
				fill: "red"
			});
		that.renderObject.click(function () {
			if (that.isOn === false) {
				that.isOn = true;
				that.activate();
			} else {
				that.isOn = false;
			}
		});

		that.activate = function () {
			function recur() {
				if (that.isOn) {
					var ball = new Ball(startX, startY, direction, paper, grid, reflectors, handle, ballLifeTime);
					setTimeout(that.activate, interval);
				}
			}

			recur();
		}

	}

	function isReflectorAtCoord(x, y, reflectorObject) {
		return reflectorObject[x.toString() + " " + y.toString()];
	}


	function generateLinearCoordsToNearestReflector(x, y, grid, direction, reflectorObject) {
		var resultArray = Array();

		function incrementDir() {
			if (direction === 0) {
				x++;
			} else if (direction === 1) {
				y++;
			} else if (direction === 2) {
				x--;
			} else if (direction === 3) {
				y--;
			}
		}


		incrementDir();
		while (true) {
			if (x >= grid.columns || y >= grid.rows || x < 0 || y < 0) {
				return resultArray;
			}
			var reflector = isReflectorAtCoord(x, y, reflectorObject);
			if (reflector) {
				return resultArray;
			} else {
				resultArray.push(x.toString() + " " + y.toString());
			}
			incrementDir();
		}
	}

	function eliminateTraveledSpacesFromCoords(coords, traveledSpacesObject) {
		return _.filter(coords, function (coord) {
			if (traveledSpacesObject[coord]) {
				return false;
			} else {
				return true;
			}
		});
	}

	//this function has side effects on traveledSpacesObject which will maintain its state
	//despite the context.
	function augmentTraveledSpaces(x, y, coords, traveledSpacesObject) {
		if (y === coords[1] && x < coords[0]) {
			for (var i = x; i <= coords[0]; i++) {
				traveledSpacesObject[i.toString() + " " + y.toString()] = true;
			}
			return traveledSpacesObject;
		} else if (y === coords[1] && x > coords[0]) {
			for (var i = coords[0]; i <= x; i++) {
				traveledSpacesObject[i.toString() + " " + y.toString()] = true;
			}
		} else if (x === coords[0] && y < coords[1]) {
			for (var i = y; i < coords[1]; i++) {
				traveledSpacesObject[i.toString() + " " + y.toString()] = true;
			}
		} else if (x === coords[0] && y > coords[1]) {
			for (var i = coords[1]; i <= y; i++) {
				traveledSpacesObject[i.toString() + " " + y.toString()] = true;
			}
		}
		return traveledSpacesObject;
	}

	function returnValidSpacesFromHere(x, y, grid, direction, traveledSpacesObject, reflectorObject) {
		var coords = generateLinearCoordsToNearestReflector(x, y, grid, direction, reflectorObject);
		coords = eliminateTraveledSpacesFromCoords(coords, traveledSpacesObject);
		return coords;
	}

	function parseCoord(coordString) {
		var xycoord = coordString.split(" ");
		xycoord[0] = parseInt(xycoord[0]);
		xycoord[1] = parseInt(xycoord[1]);
		return xycoord;
	}

	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}

	function getRandomValidSpaceFromHere(x, y, grid, direction, traveledSpacesObject, reflectorObject) {
		var coords = returnValidSpacesFromHere(x, y, grid, direction, traveledSpacesObject, reflectorObject);
		if (coords.length <= 0) {
			return false;
		}
		var randomIndex = getRandomInt(0, coords.length);
		return parseCoord(coords[randomIndex]);
	}

	function getNextLeftFromDirection(direction) {
		return [3, 0, 1, 2][direction];
	}

	function getNextRightFromDirection(direction) {
		return [1, 2, 3, 0][direction];
	}

	function getRandomTrueFalse() {
		return getRandomInt(0, 2) === 1 ? true : false;
	}

	//this is a stateful function generator that returns a function

	//function createFunctionThatOnlyReturnsTrueAfter(xCalls){
	//	return function(){
	//		xCalls--;
	//		return xCalls === 0 ? true : false;
	//	}
	//}
	//var isTwentiethCall = createFunctionThatOnlyReturnsTrueAfter(20);

	//due to this global the function can only be used once...
	function generatePuzzleReflectors(x, y, paper, grid, direction, pathDepth, reflectorLocationObject, traveledSpacesObject, isStart, solutionDepth) {
		if (pathDepth < 0) {
			return reflectorLocationObject;
		}


		if (!isStart) {
			if (!reflectorLocationObject[x.toString() + " " + y.toString()]) {
				var isGoal = false;
				//console.log(solutionDepth);
				if (solutionDepth === 0 && isWinGenerated === false) {
					isGoal = true;
					isWinGenerated = true;
				}

				var newReflector = new Reflector(x, y, getRandomTrueFalse(), paper, grid, isGoal);
				reflectorLocationObject[x.toString() + " " + y.toString()] = newReflector;
				traveledSpacesObject[x.toString() + " " + y.toString()] = false;
			}
		} else {
			if (!reflectorLocationObject[x.toString() + " " + y.toString()]) {
				traveledSpacesObject[x.toString() + " " + y.toString()] = true;
			}
		}

		var coordNextPosition = getRandomValidSpaceFromHere(x, y, grid, direction, traveledSpacesObject, reflectorLocationObject);
		if (coordNextPosition) {
			traveledSpacesObject = augmentTraveledSpaces(x, y, coordNextPosition, traveledSpacesObject);
			generatePuzzleReflectors(
				coordNextPosition[0],
				coordNextPosition[1],
				paper,
				grid,
				getNextLeftFromDirection(direction),
				pathDepth - 1,
				reflectorLocationObject,
				traveledSpacesObject,
				false,
				solutionDepth - 1);
			generatePuzzleReflectors(
				coordNextPosition[0],
				coordNextPosition[1],
				paper,
				grid,
				getNextRightFromDirection(direction),
				pathDepth - 1,
				reflectorLocationObject,
				traveledSpacesObject,
				false,
				solutionDepth - 1);
		} else {
			return reflectorLocationObject;
		}
	}


	function generateHashObjectFromArrayOfReflectors(reflectorsArray) {
		var resultObject = {}
		reflectorsArray.forEach(function (reflector) {
			resultObject[reflector.gridX.toString() + " " + reflector.gridY.toString()] = reflector;
		});
		return resultObject;
	}

	//0 is right
	//1 is down
	//2 is left
	//3 is up

	//gridX are indexes starting from 0 to row <or> column length - 1
	//if handle returns True the ball will be destroyed
	//handle is checked every grid iteration and is given the parameters of gridX and gridY
	function Ball(gridX, gridY, initialDirection, paper, grid, reflectors, handle, ballLifeTime) {
		var that = this;
		that.gridX = gridX;
		that.gridY = gridY;
		that.direction = initialDirection;
		function getX() {
			return grid.getXFromIndex(that.gridX);
		}

		function getY() {
			return grid.getYFromIndex(that.gridY);
		}

		that.renderObject = paper.circle(getX(), getY(), grid.rowHeight() / 2 * 0.4).attr({
			fill: "black"
		});

		that.renderObject.node.setAttribute("pointer-events", "none");

		setTimeout(function () {
			that.renderObject.remove();
		}, ballLifeTime);

		initMover();


		function getCollidedReflector() {
			result = reflectors[that.gridX + " " + that.gridY];
			return result;
		}

		function move(direction) {
			if (direction === 0) {
				that.gridX++;
			} else if (direction === 1) {
				that.gridY++;
			} else if (direction === 2) {
				that.gridX--;
			} else if (direction === 3) {
				that.gridY--;
			}
		}

		function animateToPosition(callback) {
			var newX = getX();
			var newY = getY();
			that.renderObject.animate({
				cx: newX,
				cy: newY
			}, 100, "linear", callback);
		}

		function updateDirectionFromReflector(reflector) {
			if (that.direction === 0) {
				if (reflector.direction) {
					that.direction = 3;
				} else {
					that.direction = 1;
				}
			} else if (that.direction === 1) {
				if (reflector.direction) {
					that.direction = 2;
				} else {
					that.direction = 0;
				}
			} else if (that.direction === 2) {
				if (reflector.direction) {
					that.direction = 1;
				} else {
					that.direction = 3;
				}
			} else if (that.direction === 3) {
				if (reflector.direction) {
					that.direction = 0;
				} else {
					that.direction = 2;
				}
			}
		}

		function isOutOfBounds() {
			return (that.gridX >= grid.rows || that.gridX < 0 ||
			that.gridY >= grid.columns || that.gridY < 0)
		}

		function updateDirection() {
			var reflector = getCollidedReflector();
			if (reflector) {
				updateDirectionFromReflector(reflector);
			}
		}

		function initMover() {
			if (handle) {
				var destroyBall = handle(that.gridX, that.gridY, reflectors)
				if (destroyBall === true) {
					that.renderObject.remove()
					return;
				}
			}
			if (isOutOfBounds()) {
				that.renderObject.remove();
				return;
			} else {
				updateDirection();
				move(that.direction);
				animateToPosition(initMover);
			}
		}
	}


	function Grid(width, height, rows, columns) {
		var that = this;
		that.width = width;
		that.height = height;
		that.rows = rows;
		that.columns = columns;
		var shrinkConst = 0.9;

		that.colWidth = function () {
			return (that.width / that.rows) * shrinkConst;
		};

		that.rowHeight = function () {
			return (that.height / that.columns) * shrinkConst;
		};

		//index starts at 0
		that.getXFromIndex = function (index) {
			return (that.colWidth() + 1) * index + that.colWidth() / 2;
		};

		that.getYFromIndex = function (index) {
			return (that.rowHeight() + 1) * index + that.rowHeight() / 2;
		};
	}


	//true is NW
	//false is NE
	function Reflector(gridX, gridY, initialDirection, paper, grid, isGoal) {
		var that = this;
		that.gridX = gridX;
		that.gridY = gridY;
		that.isGoal = isGoal;
		function getX() {
			return grid.getXFromIndex(that.gridX);
		}

		function getY() {
			return grid.getYFromIndex(that.gridY) - grid.rowHeight() / 2;
		}

		if (that.isGoal) {
			that.renderObject = paper.rect(
				grid.getXFromIndex(that.gridX) - grid.colWidth() / 2,
				grid.getYFromIndex(that.gridY) - grid.rowHeight() / 2, grid.colWidth(), grid.rowHeight()).attr({
					fill: "green"
				});
		}
		else {
			that.direction = initialDirection;
			that.clickObject = paper.rect(
				grid.getXFromIndex(that.gridX) - grid.colWidth() / 2,
				grid.getYFromIndex(that.gridY) - grid.rowHeight() / 2, grid.colWidth(), grid.rowHeight()).attr({
					fill: "white"
				});
			that.renderObject = paper.rect(getX(), getY(), 2, grid.rowHeight());
			that.renderObject.attr({
				fill: "black"
			});
			that.renderObject.transform("R" + getDirectionAngle());
			that.renderObject.click(function () {
				setRotationAndRender();
			});
			that.clickObject.click(function () {
				setRotationAndRender();
			});

			//left is true right is false
			function setRotation() {
				if (that.direction) {
					that.direction = false
				} else {
					that.direction = true
				}
			}

			function getDirectionAngle() {
				if (that.direction) {
					return 45;
				} else {
					return -45;
				}
			}

			function setRotationAndRender() {
				setRotation();
				var angle = getDirectionAngle();
				console.log(angle);
				that.renderObject.animate({
					transform: "R" + angle.toString()
				}, 100, "<");
			}
		}
	}
});
