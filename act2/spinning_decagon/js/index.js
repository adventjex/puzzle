$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);

	var spinFunction = function(){

		var i = 0;
		var clue = $('#clue');
		var spinBtn = $('.spin-btn');
		var rotationValue = 0;
		var outerRing = $('.outer');

		spinBtn.click(function(){
			rotationValue += 36;
			outerRing.css('transform', 'rotate('+rotationValue+'deg)');
			outerRing.css('-webkit-transform', 'rotate('+rotationValue+'deg)');
			outerRing.css('-ms-transform', 'rotate('+rotationValue+'deg)');
			
			if (i < 9){
				i++;	
			}

			else{
				i = 0;
			}

			if (i == 0){
				clue.html('683');
			}

			else if (i == 1){
				clue.html('013');
			}

			else if (i == 2){
				clue.html('942');
			}

			else if (i == 3){
				clue.html('375');
			}

			else if (i == 4){
				clue.html('268');
			}

			else if (i == 5){
				clue.html('501');
			}

			else if (i == 6){
				clue.html('894');
			}

			else if (i == 7){
				clue.html('137');
			}

			else if (i == 8){
				clue.html('426');
			}

			else if (i == 9){
				clue.html('750');
			}
			

		});

	}
	spinFunction();

});

