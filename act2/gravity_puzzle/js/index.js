$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);

	var letters = ["w","l","e","v","n","o","e","s","e","u","t","l","p"];
	var letterContent = $('.letters');

	for (var i = 0; i < letters.length; i++){
		letterContent.append('<span' + ' class="' + letters[i]+i+'">' + letters[i] + '</span>');
	}

	var down = $('.fa-arrow-down');
	var up = $('.fa-arrow-up');
	var position = "top";

	down.click(function(){
		if(position == "top"){
			goDown();
			
		}
	});

	up.click(function(){
		if(position == "bottom"){
			goUp();
		}
	});

	function goDown(){	
		var order = [".t10",".w0",".e2",".l11",".v3",".e6",".p12",".l1",".u9",".s7",".o5",".n4",".e8"];
		var j = 0;

		function animateLetters(el){
			var element = $(order[j]);
			element.animate(
				{
					top: '150'
				},
				{
					duration: 1000
				}
			)
			setTimeout(function(){
				animateLetters(j++);
			},500);
		}

		animateLetters(j);
		position = "bottom";

	}

	function goUp(){	
		var order = [".e8",".l1",".e2",".v3",".e6",".n4",".p12",".l11",".u9",".s7",".t10",".w0",".o5"];
		var j = 0;

		function animateLetters(el){
			var element = $(order[j]);
			element.animate(
				{
					top: '0'
				},
				{
					duration: 1000
				}
			)
			setTimeout(function(){
				animateLetters(j++);
			},500);
		}

		animateLetters(j);
		position = "top";

	}

});

