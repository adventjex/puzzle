var col1 = $('.col1');
var col3 = $('.col3');
var col2 = $('.col2');
var leftDemons = 3;
var leftHumans = 3;
var rightDemons = 0;
var rightHumans = 0;
var boatDemons = 0;
var boatHumans = 0;
var boatCapacity = 2;
var boatSide = "left";
var boat = $('.boat-wrapper');
var move = $('#move-btn');
var error1 = $('.error1');
var error2 = $('.error2');

$(document).ready(function(){
	showCurrentLevel();
	showLevelContent();

	// var content = $('.content');
	// window.service.verticalCenter(content);
	
	function setUpPuzzle(){

		$('.character').each(function(){
			$(this).remove();
		});

		for (var i = 0; i < leftDemons; i++){
			 col1.append('<span class="character" data-side="left" data-type="demon"><img src="static/demon.png"/></span>');
		}

		for (var i = 0; i < leftHumans; i++){
			col1.append('<span class="character" data-side="left" data-type="human"><img src="static/human.png"/></span>');
		}

	}setUpPuzzle();



});

function failMessage(){

	$('.fail').show();
	$('.fail').animate({
		'opacity': '1'
	},1500);

	$('body').css('overflow', 'hidden');

}



$(document).on('click', '.restart', function(){
	location.reload();
});


$(document).on('click', '.character', function(){

	var side = $(this).attr('data-side');
	var type = $(this).attr('data-type');
	var column = $(this).closest('.col');

	if(side == "boat"){
		boatCapacity++;

		if(type == "demon"){
			boatDemons--;

			if(boatSide == "left"){
				leftDemons++;
				drawLeftCharacters();
				drawBoatCharacters();
				checkMove()
			}

			else if(boatSide == "right"){
				rightDemons++;
				drawRightCharacters();
				drawBoatCharacters();
				checkMove()
			}
		}

		if(type == "human"){
			boatHumans--;

			if(boatSide == "left"){
				leftHumans++;
				drawLeftCharacters();
				drawBoatCharacters();
				checkMove()
			}

			else if(boatSide == "right"){
				rightHumans++;
				drawRightCharacters();
				drawBoatCharacters();
				checkMove()
			}
		}
	}

	else if(boatSide == side){ /* check if boat is on the same side where clicked */

		if (boatCapacity ==0 ){
			error2.show();
		}

		else if (boatCapacity > 0){
			error2.hide();
			boatCapacity--;

			if (type == "demon"){
				boatDemons++;

				if(side == "left"){
					leftDemons--;
					drawLeftCharacters();
					drawBoatCharacters();
					checkMove()
				}

				else if(side == "right"){
					rightDemons--;
					drawRightCharacters();
					drawBoatCharacters();
					checkMove()
				}
			}

			if (type == "human"){
				boatHumans++;

				if(side == "left"){
					leftHumans--;
					drawLeftCharacters();
					drawBoatCharacters();
					checkMove()
				}

				else if(side == "right"){
					rightHumans--;
					drawRightCharacters();
					drawBoatCharacters();
					checkMove()
				}
			}


		}	
	}
});

function drawBoatCharacters(){
	col2.find('.character').each(function(){
		$(this).remove();
	});

	for(var i = 0; i < boatDemons; i++){
		boat.append('<span class="character" data-side="boat" data-type="demon"><img src="static/demon.png"/></span>');
	}

	for(var i = 0; i < boatHumans; i++){
		boat.append('<span class="character" data-side="boat" data-type="human"><img src="static/human.png"/></span>');
	}
}

function moveBoat(){
	move.click(function(){

		if(boatCapacity != 2){

			error1.hide();
			error2.hide();

			if (boatSide == "left"){
				boatSide = "right";

				checkMove();

				boat.animate({
					'left': '100%'
				},1000);

				
			}

			else if (boatSide == "right"){
				boatSide = "left";

				checkMove();

				boat.animate({
					'left': '110px'
				},1000);

				
			}			
		}

		else{
			error1.show();
		}


	});
}moveBoat();

function drawLeftCharacters(){
	col1.find('.character').each(function(){
		$(this).remove();
	});

	for (var i = 0; i < leftDemons; i++){
		col1.append('<span class="character" data-side="left" data-type="demon"><img src="static/demon.png"/></span>');
	}

	for (var i = 0; i < leftHumans; i++){
		col1.append('<span class="character" data-side="left" data-type="human"><img src="static/human.png"/></span>');
	}
}

function drawRightCharacters(){
	col3.find('.character').each(function(){
		$(this).remove();
	});

	for (var i = 0; i < rightDemons; i++){
		col3.append('<span class="character" data-side="right" data-type="demon"><img src="static/demon.png"/></span>');
	}

	for (var i = 0; i < rightHumans; i++){
		col3.append('<span class="character" data-side="right" data-type="human"><img src="static/human.png"/></span>');
	}
}

function checkMove(){

	if (leftHumans == 0 && leftDemons == 0 && boatDemons == 0 && boatHumans == 0 && rightHumans == 3 && rightDemons == 3){
		window.malum.checkAnswerAndCreateButton("allcrossed");
	}

	else if(boatSide == "left"){

		if (leftHumans+boatHumans > 0){
			if( (leftDemons + boatDemons) > (leftHumans + boatHumans) ){
				failMessage();
			}
		}

		if (rightHumans > 0){
			if(rightDemons > rightHumans){
				failMessage();
			}
		}

	}

	else if(boatSide == "right"){

		if (leftHumans > 0){
			if(leftDemons > leftHumans){
				failMessage();
			}
		}

		if (rightHumans+boatHumans > 0){

			if( (rightDemons + boatDemons) > (rightHumans + boatHumans) ){
				failMessage();
			}
		}



	}

}



