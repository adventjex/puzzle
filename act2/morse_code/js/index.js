var sun1 = false;
var sun2 = false;
var sun3 = false;


var sunFunctions = {

	checkSunState: function toggleSun(){

		var col1 = $('#col1');
		var col2 = $('#col2');
		var col3 = $('#col3');

		if(sun1 && sun2 && !sun3){
			col1.addClass('on');
			col1.find('.hint').html('<span>...</span><span>---</span>');
		}

		else{
			col1.removeClass('on');
			col1.find('.hint').html('&nbsp;');
		}

		if(sun2 && sun3 && !sun1){
			col2.addClass('on');
			col2.find('.hint').html('<span>..-</span><span>-.</span>');
		}

		else{
			col2.removeClass('on');
			col2.find('.hint').html('&nbsp;');
		}

		if(sun1 && sun3 && !sun2){
			col3.addClass('on');
			col3.find('.hint').html('<span>-..</span><span>...</span>');
		}

		else{
			col3.removeClass('on');
			col3.find('.hint').html('&nbsp;');
		}

	}
}


$(document).on('click', '.sun', function(){

	var sunID = $(this).attr('id');

	if (sunID == 'sun1'){
		sun1 = !sun1;
	}

	else if (sunID == 'sun2'){
		sun2 = !sun2;
	}

	else if (sunID == 'sun3'){
		sun3 = !sun3;
	}

	$(this).toggleClass('sunColor');

	sunFunctions.checkSunState();

});

$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	// var height = $(window).height();
	// $('.col').each(function(){
	// 	$(this).css('height', height);
	// });

});