var floor0 = $('#floor0');
var elevator = $('.elevator');
var capacity = 2;
var elevatorFloor = 0;
var bottom = 0;
var pig = {imgcode: '<img src="static/pig.svg" class="animal" id="pig"/>', floorPosition: 0, inElevator: false };
var gorilla = {imgcode: '<img src="static/gorilla.svg" class="animal" id="gorilla" />', floorPosition: 0, inElevator: false };
var monkey = {imgcode:'<img src="static/monkey.svg" class="animal" id="monkey" />', floorPosition: 0, inElevator: false };
var tiger = {imgcode: '<img src="static/tiger.svg" class="animal" id="tiger"/>', floorPosition: 0, inElevator: false };
var mouse = {imgcode: '<img src="static/mouse.svg" class="animal" id="mouse" />', floorPosition: 0, inElevator: false };
var lion = {imgcode: '<img src="static/lion.svg" class="animal" id="lion" />',floorPosition: 0, inElevator: false };
var animals = {"pig": pig, "gorilla": gorilla, "monkey": monkey, "tiger": tiger, "mouse": mouse, "lion": lion};
	var message = $('.message');


$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

  	var content = $('.content');
  	window.service.verticalCenter(content);

  	function setUpPuzzle(){
  		floor0.html('<img src="static/pig.svg" class="animal" id="pig"/><img src="static/gorilla.svg" class="animal" id="gorilla" /><img src="static/monkey.svg" class="animal" id="monkey" /><img src="static/tiger.svg" class="animal" id="tiger"/><img src="static/mouse.svg" class="animal" id="mouse" /><img src="static/lion.svg" class="animal" id="lion" />');
  	}
  	setUpPuzzle();
  
});


$(document).on('click', '.animal', function(){

	var id = $(this).attr('id');
	
	if (elevatorFloor == animals[id].floorPosition){

		if(!animals[id].inElevator){
			if (capacity > 0){
				capacity--;
				animals[id].inElevator = true;
				drawElevator();
				drawFloor(elevatorFloor);
				checkAnswer();
			}				
		}
		
		else if(animals[id].inElevator){
			capacity++;
			animals[id].inElevator = false;
			drawElevator();
			drawFloor(elevatorFloor);
			checkAnswer();
		}

	}

});

$(document).on('keydown', function(e){
	if (e.keyCode == 38) { 
     	moveUp();
    }

    else if (e.keyCode == 40){
		moveDown();
	}
});

$(document).on('click', '.fa-arrow-up', function(){
	moveUp();
});

$(document).on('click', '.fa-arrow-down', function(){
	moveDown();
});

function moveUp(){
	if (validateMove() == false){
		return;
	}

	if(elevatorFloor < 5 && capacity !=2 ){
		elevatorFloor++;
		bottom += 60;

		for (i in animals) {
		    if (!animals.hasOwnProperty(i)) {
		        continue;
		    }
		    
		    if(animals[i].inElevator == true){
		   		animals[i].floorPosition++;
		   	}
		}

		message.html('');

		elevator.animate(
			{
				bottom: bottom
			},
			{
				duration: 100
			}
		);
	}	
}

function moveDown(){
		if (validateMove() == false){
		return;
	}

	if(elevatorFloor > 0 && capacity !=2){
		elevatorFloor--;
		bottom -= 60;

		for (i in animals) {
		    if (!animals.hasOwnProperty(i)) {
		        continue;
		    }
		    
		    if(animals[i].inElevator == true){
		   		animals[i].floorPosition--;
		   	}
		}

		message.html('');

		elevator.animate(
			{
				bottom: bottom
			},
			{
				duration: 100
			}
		);
	}

}

var drawElevator = function(){
	elevator.html('<div class="arrows"><i class="fa fa-arrow-up"></i><i class="fa fa-arrow-down"></i></div>');
	for (i in animals) {
	    if (!animals.hasOwnProperty(i)) {
	        continue;
	    }
	    
	    if(animals[i].inElevator == true){
	   		elevator.append(animals[i].imgcode);
	   	}
	}
}

var drawFloor = function(number){
	var floor = $('#floor'+number);
	floor.html('');

	for (i in animals) {
	    if (!animals.hasOwnProperty(i)) {
	        continue;
	    }
	    
	    if(animals[i].inElevator != true){

	    	if(animals[i].floorPosition == elevatorFloor){
	   			floor.append(animals[i].imgcode);
	   		}
	   	}
	}
}

var checkAnswer = function(){
	if(animals["pig"].floorPosition == 1 && animals["gorilla"].floorPosition == 2 && animals["monkey"].floorPosition == 2 && animals["tiger"].floorPosition == 3 && animals["mouse"].floorPosition == 4 && animals["lion"].floorPosition == 5){
		window.malum.checkAnswerAndCreateButton("theanimals");
	}
}

var validateMove = function(){

	if(animals["pig"].inElevator && animals["tiger"].inElevator){
		message.html("The PIG cannot be in the elevator with the TIGER");
		return false;
	}

	if(animals["pig"].inElevator && animals["lion"].inElevator){
		message.html('The PIG cannot be in the elevator with the LION.')
		return false;
	}

	if(animals["mouse"].inElevator && animals["tiger"].inElevator){
		message.html('The MOUSE cannot be in the elevator with TIGER.')
		return false;
	}

	if(animals["mouse"].inElevator && !animals["lion"].inElevator && !animals["pig"].inElevator){
		message.html('The MOUSE cannot ride the elevator alone.')
		return false;
	}

	if(animals["gorilla"].inElevator && !animals["monkey"].inElevator){
		message.html('The GORILLA must ride with the MONKEY.');
		return false;
	}

	if(!animals["gorilla"].inElevator && animals["monkey"].inElevator){
		message.html('The MONKEY must ride with the GORILLA.');
		return false;
	}

}

$(document).on('click', '.restart', function(){
	location.reload();
});
