
var appScreen = $('.screen');
var dots = $('.typing-wrapper');
var robotTurn = true;
var answerType = false;
var count = 0;
var inputValue = "";
var disabled = true;

var next = ["Where to next?", "What's our next destination?", "Where do you want to go next?", "Where are we going next?", "What country is next?", "What's our next country?", "Alright, where to next?"];

var rightResponse = ["sounds good", "is a good choice", "is a good next destination", "is okay", "is somewhere I'm willing to go"];

var wrongResponse = ["Can't go there", "I'm not so sure about that", "That's a bad choice", "That's not part of our itinerary", "That's not somewhere we can go"];

var alternative = ["How about we go to", "Let's go to", "We can go to", "A better choice would have been", "A better choice would be", "A better option is"];

var tCountry = ["Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu"];

var rCountry = ["Reunion", "Romania", "Russian Federation", "Rwanda", "Russia"];

var aCountry = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan"];

var vCountry = ["Vanuatu", "Venezuela", "Vietnam", "Virgin Islands"]

var eCountry = ["East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia"];

var lCountry = ["Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg"];

var hCountry = ["Haiti", "Heard and Mc Donald Islands", "Holy See", "Honduras", "Hong Kong", "Hungary"];

var letterArray = [tCountry, rCountry, aCountry, vCountry, eCountry, lCountry, tCountry, hCountry, eCountry, eCountry, aCountry, rCountry, tCountry, hCountry];

function runDots(){
	disabled = true;

	dots.toggleClass('on');

	$('#input').val('');
	setTimeout(function(){
		addRobotChat(answerType);
		if(count < 13){
			count++;
		}
		else{
			count = 0;
		}
		dots.toggleClass('on');
	},2200);

}

function addRobotChat(answerType){
	if (count == 0){
		firstChat();	
	}

	else if(count == 12){
		lastChat();
	}

	else if (answerType) {
		robotRespondsToRightAnswer();
	}

	else if (!answerType){
		robotRespondsToWrongAnswer();
	}

	disabled = false;
}

function firstChat(){
	var randomCountry = Math.floor((Math.random() * tCountry.length));
	var	randomNext = Math.floor((Math.random() * next.length));
	var message = '<div class="message-wrapper robot"><div class="arrow-left"></div><div class="message-container"><p>Hello. I am Magellan.  We need to travel the earth. Let us start at ' + tCountry[randomCountry] + '. '+ next[randomNext]+'</p></div></div>';

	$('.message-area').append(message);

	$('.message-area').scrollTop(5000);
}

function lastChat(){
	var randomCountry = Math.floor((Math.random() * tCountry.length));
	var	randomNext = Math.floor((Math.random() * next.length));
	var message = '<div class="message-wrapper robot"><div class="arrow-left"></div><div class="message-container"><p> What is our last destination?'+'</p></div></div>';

	$('.message-area').append(message);

	$('.message-area').scrollTop(5000);
}

function robotRespondsToRightAnswer(){
	var randomPhrase = Math.floor((Math.random() * rightResponse.length));
	var	randomNext = Math.floor((Math.random() * next.length));
	var message = '<div class="message-wrapper robot"><div class="arrow-left"></div><div class="message-container"><p>' + inputValue +' ' + rightResponse[randomPhrase] + '. '+ next[randomNext]+'</p></div></div>';
	$('.message-area').append(message);

	$('.message-area').scrollTop(5000);

}

function robotRespondsToWrongAnswer(){
	right = 0;
	var randomPhrase = Math.floor((Math.random() * wrongResponse.length));
	var alt = Math.floor((Math.random() * alternative.length));
	var	randomNext = Math.floor((Math.random() * next.length));
	var country = letterArray[count];
	var real = Math.floor((Math.random() * country.length));
	var message = '<div class="message-wrapper robot"><div class="arrow-left"></div><div class="message-container"><p>' + inputValue +'? ' + wrongResponse[randomPhrase] + '. ' + alternative[alt]+ ' '+ country[real]+ '. ' + next[randomNext]+'</p></div></div>';
	$('.message-area').append(message);

	$('.message-area').scrollTop(5000);

}

$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();


	function setScreenSize(){
		var windowHeight = $(window).height();
		var newHeight = windowHeight - 138;
		var screenWrapper = $('.screen-wrapper');
		var messageArea = $('.message-area');
		var messageHeight = newHeight - 130;
		screenWrapper.css('height', newHeight);		
		messageArea.css('height', messageHeight);
	}
	setScreenSize();

	function centerScreen(){
		var theScreen = $('.screen-wrapper');
		var screenWidth = theScreen.width();
		var offset = screenWidth*-0.5;
		theScreen.css('margin-left', offset);
	}centerScreen();

	$(window).resize(function(){
		setScreenSize();
		centerScreen();
	});

	$('#input').focus();

	runDots();
});

$(document).on('click', '#send', function(){
	if (!disabled){
		addUserChat();
	}
});

$('#send').click(function(){
	if (!disabled){
		addUserChat();
	}
})

$(document).bind('keypress', '#input', function(e) {
	if(e.keyCode==13){
		if (!disabled){
			addUserChat();
		}
	}
});

function addUserChat(){
	console.log(count);
	inputValue = $('#input').val();
	inputValue = inputValue.toLowerCase().replace(/\b[a-z]/g, function(letter) {
    return letter.toUpperCase();
	});
	var country = letterArray[count];
	var message = '<div class="message-wrapper user"><div class="arrow-right"></div><div class="message-container"><p>'+ inputValue+'</p></div></div>'
	$('.message-area').append(message);

	$('.message-area').scrollTop(5000);

	if (count == 13){
		for (var i = 0; i < country.length; i++) {

		    if (inputValue.toLowerCase() == country[i].toLowerCase() ){
				answerType = true; 
				checkAnswer();
			}

			else{
				answerType = false;
				count = 0;
			}
		
		}	
	}

	else{

		for (var i = 0; i < country.length; i++) {

		    if (inputValue.toLowerCase() == country[i].toLowerCase() ){
				answerType = true; 
				return runDots();  
			}

			else{
				answerType = false;
			}
		
		}
	}

	runDots();
}

function checkAnswer(){

	if (answerType){
		$('.screen-wrapper').animate(
			{
				opacity: 0
			},
			{
				duration: 2000,
				complete: function(){
					$('.screen-wrapper').remove();
					$('.secret').html('408.476.5834');
					$('.hidden').show();
					$('.hidden').animate(
						{
							opacity: 1
						},
						{
							duration: 2000
						}
					);
				}
			}
		);

	}
}


