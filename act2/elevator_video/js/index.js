$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();


    var nIntervId; //<----make a global var in you want to stop the timer
                   //-----with clearInterval(nIntervId);
    function updateTime() {
        nIntervId = setInterval(flashTime, 1000); //<---prints the time 
    }                                                //----after every minute

    function flashTime() {
        var now = new Date();
        var h = now.getHours();
        var m = now.getMinutes();
        var s = now.getSeconds();
        if (s < 10){
        	s = '0'+s;
        }
        var time = h + ' : ' + m + ' : ' + s;
        $('.time').html(time); //<----updates the time in the $('#my_box1') [needs jQuery]                
    }                             

    $(function() {
        updateTime();
    });
});

var canvas = document.getElementById('canvas')
  , context = canvas.getContext('2d')
  , img = new Image()
  , w
  , h
  , offset
  , glitchInterval;

img.src = 'static/video2.jpg';
img.onload = function() {
  init();
	window.onresize = init;
};

var init = function() {
	clearInterval(glitchInterval);
	canvas.width = w = 500;
	offset = w * .1;
	canvas.height = h = 341;
	glitchInterval = setInterval(function() {
		clear();
		context.drawImage(img, 0, 0, 500, 340);
		setTimeout(glitchImg, randInt(250, 1000));
	}, 500);
};

var clear = function() {
	context.rect(0, 0, w, h);
	context.fill();
};
    
var glitchImg = function() {
	for (var i = 0; i < randInt(1, 13); i++) {
		var x = Math.random() * w;
		var y = Math.random() * h;
		var spliceWidth = w - x;
		var spliceHeight = randInt(5, h / 3);
		context.drawImage(canvas, 0, y, spliceWidth, spliceHeight, x, y, spliceWidth, spliceHeight);
		context.drawImage(canvas, spliceWidth, y, x, spliceHeight, 0, y, x, spliceHeight);
	}
};

var randInt = function(a, b) {
	return ~~(Math.random() * (b - a) + a);
};