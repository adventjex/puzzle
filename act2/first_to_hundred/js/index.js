$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);


});

var total = 0;
var mymove = $('.mymove');
mymove.hide();
$('.lose').hide();

$(document).on('click', '.add', function(){
	runTurn();
});

$(document).on('keyup', function(e){
	if (e.keyCode == '13') {
        e.preventDefault();
        runTurn();
    }
});

var runTurn = function(){
	var value = $('.value').val();
	value = parseInt(value);
	
	if (value > 0 && value < 11){
		total = value + total;
		$('.total').html(total);

		if(total == 100){
			total = "You Win";
			$('.total').html(total);
			window.malum.checkAnswerAndCreateButton("hundred");

		}

		else{
			$('.button').addClass('disable');
			$('.button').removeClass('add');
			mymove.show(
				setTimeout(function(){
					total = computerTurn(total);
					$('.total').html(total);
				},1000)
			);

		}
	}
}

var computerTurn = function(total){

	if(total == 1){
		var computerValue = 1;
		total = computerValue + total;
	}

	else if (total < 12){
		var computerValue = 12 - total;

		if (computerValue < 11){
			total = 12;
		}

		else{
			total = total + 1;
		}
	}

	else if (total < 23){
		var computerValue = 23 - total;

		if (computerValue < 11){
			total = 23;
		}

		else{
			total = total + 2;
		}
	}

	else if (total < 34){
		var computerValue = 34 - total;

		if (computerValue < 11){
			total = 34;
		}

		else{
			total = total + 3;
		}
	}

	else if (total < 45){
		var computerValue = 45 - total;

		if (computerValue < 11){
			total = 45;
		}

		else{
			total = total + 4;
		}
	}

	else if (total < 56){
		var computerValue = 56 - total;

		if (computerValue < 11){
			total = 56;
		}

		else{
			total = total + 5;
		}
	}

	else if (total < 67){
		var computerValue = 67 - total;

		if (computerValue < 11){
			total = 67;
		}

		else{
			total = total + 6;
		}
	}

	else if (total < 78){
		var computerValue = 78 - total;

		if (computerValue < 11){
			total = 78;
		}

		else{
			total = total + 7;
		}
	}

	else if (total < 89){
		var computerValue = 89 - total;

		if (computerValue < 11){
			total = 89;
		}

		else{
			total = total + 8;
		}
	}

	else if (total < 100){
		var computerValue = 100 - total;

		if (computerValue < 11){
			total = 100;
			$('.lose').show();
			mymove.hide();
			return total;
		}

		else{
			total = total + 9;
		}
	}
	$('.button').removeClass('disable');
	$('.button').addClass('add');
	mymove.hide();
	return total;

}

$(document).on('click', '.restart', function(){
	location.reload();
});

