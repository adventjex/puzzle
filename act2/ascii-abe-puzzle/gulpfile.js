/**
 * Created by brian on 3/7/15.
 */
var gulp = require('gulp');
var closure = require('gulp-closure-compiler-service');
var browserify = require('browserify');
var transform = require('vinyl-transform');

var browserified = transform(function(filename) {
	var b = browserify(filename);
	return b.bundle();
});

gulp.task('build', function() {
  return gulp.src('js/src/*.js')
	.pipe(browserified)
  	//.pipe(closure())
  	.pipe(gulp.dest('js/build/'));
});
