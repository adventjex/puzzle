var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function isEven(number){
		return number%2===0;
	}

	function init() {
		showCurrentLevel();
		showLevelContent();

		var width = $(window).width();
		var height = $(window).height();

		var leftStartPoint = Math.floor(0 - (width/2));
		var rightStartPoint = Math.floor(width + (width/2));
		var interval = 2000;
		var stringArray = [
			"the neuroscientist",
			"that wears",
			"a mask",
			"of sanity."
		];
		var division = height/stringArray.length;
		var elementArray = [];
		var $content = $(".content");
		var $body = $("body");
		for(var i = 0; i< stringArray.length; i++){
			var startPoint;
			if(isEven(i)){
				startPoint = leftStartPoint;
			} else {
				startPoint = rightStartPoint;
			}

			var $newElement = $(document.createElement("div"));
			$newElement.html(stringArray[i]);
			$newElement.css({
				position:"absolute",
				left:startPoint,
				top:Math.floor(division*i)
			}).addClass("moving_elements");
			$body.append($newElement);

			elementArray.push($newElement);
		}

		function cycleElement(elements, speed){
			return function recurse() {
				elements.forEach(function (element, index, array) {
					if (parseInt(element.css("left")) === leftStartPoint) {
						element.transit({
							left: rightStartPoint
						}, speed);
					} else if (parseInt(element.css("left")) === rightStartPoint) {
						element.transit({
							left: leftStartPoint
						}, speed);
					}
				});
				setTimeout(recurse, interval)
			}
		}

		setTimeout(cycleElement(elementArray, 1000), interval);
		var $sumbit = $(".submit").off();
		$sumbit.click(function(e){
			e.preventDefault();
			var answer = $(".answer-input").val();
			window.malum.checkAnswerAndCreateButton(answer, function(){}, function(){
				$(".moving_elements").remove();
			});
		});
	}
	window.utils.dynamicVerticalAndHorizontalCentering($(".content"));

})();



