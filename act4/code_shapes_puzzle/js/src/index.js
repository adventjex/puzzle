var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	function createTable() {
		return $(document.createElement("table")).css({
			"border-spacing": "10px !important",
			"border-collapse": "none !important",
			"margin-left": "auto",
			"margin-right": "auto"
		});
	}

	function createRow(content) {
		var row = $(document.createElement("tr"));
		$(row).append(content);
		return $(row);
	}

	function createColumn(content) {
		var col = $(document.createElement("td")).css({
			"vertical-align": "middle"
		});
		col.append(content);
		return col;
	}

	function createTwoColRow(content1, content2) {
		var row = createRow(createColumn(content1));
		$(row).append(createColumn(content2));
		return $(row);
	}



	function cleanText(text) {
		text = text.toLowerCase();
		text = text.replace(/\t/, "");
		text = text.replace(/\n/, "");
		text = text.replace(/\r/, "");
		text = text.replace(/\b/, "");
		text = text.replace(/\f/, "");
		return text.trim();
	}

	function createCircle() {
		var $icon = $(document.createElement("i"));
		$icon.addClass("fa fa-circle");
		$icon.css({
			"background-color": "black",
			"border-radius": "50%",
			"color": "black"
		}).addClass("black");
		return $icon;
	}

	function createSquare() {
		var $icon = $(document.createElement("i"));
		$icon.addClass("fa fa-square");
		$icon.css({
			"background-color": "black",
			"color": "black"
		}).addClass("black").addClass("square");
		return $icon;
	}

	function white(element) {
		return element.css({
			color: "grey",
			"background-color": "grey"
		}).addClass("white").removeClass("black");
	}

	function black(element) {
		return element.css({
			color: "black",
			"background-color": "black"
		}).addClass("black").removeClass("white");
	}

	function double(element) {
		var $copyOfElement = $(element.clone());
		var $enclosingDiv = $(document.createElement("div")).css({
			"line-height": "0px"
			//"margin-bottom": "-2px"
		});
		var $span1 = $(document.createElement("span")).css({
			width: "auto",
			"line-height": "0px"
		});
		var $span2 = $(document.createElement("span")).css({
			width: "auto",
			"line-height": "0px"
		});
		$span1.append(element);
		$span2.append($copyOfElement);
		$enclosingDiv.append($span1).append($span2);
		return $enclosingDiv;
	}

	function stack(element) {
		var $copyOfElement = $(element.clone());
		var $enclosingDiv = $(document.createElement("div"));
		var $div1 = $(document.createElement("div")).css({
			width: "auto",
			"line-height": "0px"
		});
		var $div2 = $(document.createElement("div")).css({
			width: "auto",
			"line-height": "0px"
		});
		$div1.append(element);
		$div2.append($copyOfElement);
		$enclosingDiv.append($div1).append($div2);
		return $enclosingDiv;
	}

	function invertElement(element) {
		if (element.css("color") === "black" || element.css("color") === "rgb(0, 0, 0)") {
			element.css({
				"color": "grey",
				"background-color": "grey"
			}).removeClass("black").addClass("white");
		} else if (element.css("color") === "grey" || element.css("color") === "rgb(128, 128, 128)") {
			element.css({
				"color": "black",
				"background-color": "black"
			}).removeClass("white").addClass("black");
		}
		return element;
	}

	function invertAll(element) {
		if (element.children().length >= 1) {
			element.children().each(function () {
				invertAll($(this));
			});
		} else {
			invertElement(element);
		}
		return $(element);
	}

	function double_invert(element) {
		var $copyOfElement = $(element.clone());
		var $enclosingDiv = $(document.createElement("div")).css({
			//height: "14px",
			//"margin-bottom": "-2px"
		});
		var $span1 = $(document.createElement("span")).css({
			width: "auto"
			//"height": "14px"
		});
		var $span2 = $(document.createElement("span")).css({
			width: "auto"
			//"height": "14px",
		});
		$span1.append(element);
		$span2.append($copyOfElement);
		$span2 = invertAll($span2);
		$enclosingDiv.append($span1).append($span2);
		return $enclosingDiv;
	}

	function stack_invert(element) {
		var $copyOfElement = $(element.clone());
		var $enclosingDiv = $(document.createElement("div"));
		var $div1 = $(document.createElement("div")).css({
			width: "auto",
			"line-height": "0px"
		});
		var $div2 = $(document.createElement("div")).css({
			width: "auto",
			"line-height": "0px"
		});
		$div1.append(element);
		$div2.append($copyOfElement);
		$div2 = invertAll($div2);
		$enclosingDiv.append($div1).append($div2);
		return $enclosingDiv;
	}

	function createLegend1() {
		var translater = {
			circle: createCircle,
			double: double,
			invert: invertAll
		};
		var table = createTable();
		for (var funcname in translater) {
			var key = funcname;
			if (funcname != "circle" && funcname != "square") {
				key = key + " circle";
			}
			var $keyElement = $(document.createElement("p")).html(key);
			table.append(createTwoColRow($keyElement, translater[funcname](createCircle())));
		}
		var $squareElement = $(document.createElement("p")).html("square");
		table.append(createTwoColRow($squareElement, createSquare()));
		return table;
	}

	function createLegend2() {
		var translater = {
			double_invert: double_invert,
			stack: stack,
			stack_invert: stack_invert
		};
		var table = createTable();
		for (var funcname in translater) {
			var key = funcname;
			if (funcname != "circle" && funcname != "square") {
				key = key + " circle";
			}
			var $keyElement = $(document.createElement("p")).html(key);
			table.append(createTwoColRow($keyElement, translater[funcname](createCircle())));
		}
		var $squareElement = $(document.createElement("p")).html("square");
		table.append(createTwoColRow($squareElement, createSquare()));
		return table;
	}

	function setHeightToPercentOfWindow(element, percent) {
		element.css({
			height: Math.floor((percent / 100) * $(window).height())
		});
		return $(element)
	}

	function getValueOfElement(element) {
		return element.val();
	};

	function parseIntoArrayOfWords(text) {
		text = cleanText(text);
		return text.split(" ");
	}

	function createArrayOfFunctionFromArrayOfWords(arrayOfWords) {
		var translater = {
			circle: createCircle,
			square: createSquare,
			double: double,
			white: white,
			black: black,
			invert: invertAll,
			double_invert: double_invert,
			stack: stack,
			stack_invert: stack_invert
		};

		if (arrayOfWords[arrayOfWords.length - 1] != "circle" && arrayOfWords[arrayOfWords.length - 1] != "square") {
			return false;
		}


		var arrayOfFunctions = [];
		try {
			arrayOfWords.forEach(function (word, index) {
				if (translater[word]) {
					arrayOfFunctions.push(translater[word])
				} else {
					throw Error();
				}
				if (index < arrayOfWords.length-1 && (word === "circle" || word === "square")) {
					throw Error();
				}
			});
		} catch(e) {
			return false;
		}


		return arrayOfFunctions;

	}

	function getArrayOfBaseElements(element) {
		function recurse(element, resultArray) {
			element = $(element);
			if (element.children().length >= 1) {
				element.children().each(function () {
					recurse($(this), resultArray);
				});
			} else {
				resultArray.push(element);
			}
		}
		var result = [];
		recurse(element, result);
		return result;
	}

	function isWin(element) {
		elements = getArrayOfBaseElements(element);
		var squaresInLegend = 2;
		var $squares = $(".square");
		var $blacks = $(".black");
		var $whites = $(".white");
		if($squares.length != 64+squaresInLegend){
			return false;
		}
		if($squares.length!= elements.length+squaresInLegend){
			return false;
		}
		if(!$(elements[0]).hasClass("white")){
			return false;
		}
		if(!$(elements[elements.length-1]).hasClass("white")){
			return false;
		}
		if(!$(elements[elements.length-2]).hasClass("black")){
			return false;
		}
		if(!$(elements[1]).hasClass("black")){
			return false;
		}
		return true;

	};

	function getCompletedElementFrom(arrayOfFunctions) {
		return _.compose.apply(this, arrayOfFunctions)()
	}

	function compileAndHandle(inputElement, handler) {
		var success = false;
		var finalElement = false;
		var text = getValueOfElement(inputElement);
		var arrayOfWords = parseIntoArrayOfWords(text);
		var arrayOfFunctions = createArrayOfFunctionFromArrayOfWords(arrayOfWords);
		if (arrayOfFunctions) {
			success = true;
			finalElement = getCompletedElementFrom(arrayOfFunctions);
		}

		handler(success, finalElement);


	}

	function clearElement(displayElement) {
		displayElement.html("");
	}

	function init() {
		var $content = $(".content");
		showCurrentLevel();
		showLevelContent();
		setHeightToPercentOfWindow($(".answer-input"), 40);
		var legend1 = createLegend1();
		var legend2 = createLegend2();
		var legends = document.createElement("div")
		$(legends).append(legend1).append(legend2).css({
			"overflow":"hidden"
		}).addClass("legends");
		var legend_css = {
			"width":"30%",
			"float":"left"
		};
		$(legend1).css(legend_css);
		$(legend2).css(legend_css);

		$content.prepend(legends);


		//var circle = invertAll(stack(double_invert(white(createCircle()))));
		$display = $(".display");
		//$display.append(circle);
		$answerInput = $(".answer-input");
		$submit = $(".submit");
		$submit.unbind();
		$submit.click(function (e) {
			e.preventDefault();
			clearElement($display);
			compileAndHandle($answerInput, function (success, element) {
				if (success) {
					$display.append(element);
					if(isWin(element)){
						window.malum.checkAnswerAndCreateButton("chessboard");
					}
				} else {
					$display.html("Error.");
				}
			})
		});


	}

	//window.utils.dynamicVerticalAndHorizontalCentering($(".content"));

})();



