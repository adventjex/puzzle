var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();


	function createSpansFromLetters(inputString) {
		var resultArray = [];
		for (var i = 0; i < inputString.length; i++) {
			var $span = $(document.createElement("span"));
			$span.html(inputString[i]);
			resultArray.push($span);
		}
		return resultArray;
	}

	function applyToElements(arrayOfElements, functionToApply) {
		return _.map(arrayOfElements, function (element) {
			return functionToApply(element);
		});
	}

	function createFunctionToMakeElementSpin(spinRate) {
		return function (element) {
			function recurse() {
				element.transit({
					rotate: "+=360deg"
				}, spinRate, 'linear');
				setTimeout(recurse, spinRate);
			}

			recurse();
		}
	}

	function getRandomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}

	function spliceRandomly(maxSlice, arrayToSlice) {
		var result = [];

		function recurse(maxSlice, arrayToSlice, resultArray) {
			var sliceEnd = getRandomInt(1, maxSlice + 1);
			if (sliceEnd >= arrayToSlice.length) {
				resultArray.push(arrayToSlice);
				return;
			}
			var slicedArray = arrayToSlice.slice(0, sliceEnd);
			var recurseArray = arrayToSlice.slice(sliceEnd);
			resultArray.push(slicedArray);
			recurse(maxSlice, recurseArray, resultArray);
		}

		recurse(maxSlice, arrayToSlice, result);
		return result;
	}

	function isEven(number) {
		return number % 2 === 0;
	}

	function ceasarEncryptionShiftRight(inputletter, spaces) {
		return String.fromCharCode(((inputletter.toUpperCase().charCodeAt(0) - "A".charCodeAt(0) + spaces) % 26) + "A".charCodeAt(0))

	}

	function mergeArrays(array1, array2) {
		var resultArray = [];
		var biggestArray;
		var smallestArray;
		if (array1.length > array2.length) {
			biggestArray = array1;
			smallestArray = array2;
		} else {
			biggestArray = array2;
			smallestArray = array1;
		}


		while (biggestArray.length > 0) {
			if (smallestArray.length > 0) {
				resultArray.push(smallestArray[0]);
				smallestArray.shift();
			}
			if (biggestArray.length > 0) {
				resultArray.push(biggestArray[0]);
				biggestArray.shift();
			}

			resultArray.forEach(function(element){
				console.log("..");
				element.forEach(function(element){
					console.log(element.html());
				});
			});
			console.log("___________");
		}
		return _.reduce(resultArray, function (element, memo) {
			var newArray = element.concat(memo);
			newArray.forEach(function(element){
				console.log(element.html());
			});
			console.log("___________");
			return newArray;
		}, []);

	}

	function init() {
		showCurrentLevel();
		showLevelContent();

		window.utils.dynamicVerticalAndHorizontalCentering($(".content"));

		var clue1 = "caesarciphershiftleftbytwo".toUpperCase();
		var clue2 = "hesentemailsfrombeyondthegrave";
		var clue2encoded = _.map(clue2, function (letter) {
			return ceasarEncryptionShiftRight(letter, 2);
		});

		var clue1spans = createSpansFromLetters(clue1);
		var clue2spans = createSpansFromLetters(clue2encoded);

		var splitClue1Spans = spliceRandomly(2, clue1spans);
		var splitClue2Spans = spliceRandomly(2, clue2spans);
		console.log(splitClue1Spans);
		//splitClue1Spans.forEach(function(element){
		//	console.log("..");
		//	element.forEach(function(element){
		//		console.log(element.html());
		//	})
		//});

		var encodedElements = mergeArrays(splitClue1Spans, splitClue2Spans);
		var fastSpin = createFunctionToMakeElementSpin(100);
		var slowSpin = createFunctionToMakeElementSpin(700);

		clue1spans.forEach(function(element){
			fastSpin(element);
		});

		clue2spans.forEach(function(element){
			slowSpin(element);
		});

		var $encodedDiv = $(".encoded");

		encodedElements.forEach(function (element) {
			$encodedDiv.append(element);
		});


	}

})();



