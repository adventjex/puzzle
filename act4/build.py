#!/usr/bin/python2.7

__author__ = 'brian'
import shutil
import os
import json
from jinja2 import Template
import hashlib
import sys

#GLOBALS
DEBUG = "FALSE"


def file_get_contents(filename):
    with open(filename) as f:
        return f.read()


def generate_javascript_local_globals_file_string(currentLevel, encoded_answer, templateFile):
    file_contents = file_get_contents(get_absolute_path(templateFile))
    template = Template(file_contents)
    return template.render(levelNumber=currentLevel, answerHash=encoded_answer)


def create_file(relative_path, file_content):
    opened_file = open(get_absolute_path(relative_path), 'w')
    opened_file.write(file_content)
    opened_file.close()


def create_glue_file(level, encoded_answer, relative_path):
    glue_file_name = "glue.js"
    file_contents = generate_javascript_local_globals_file_string(level, encoded_answer, glue_file_name)
    create_file(os.path.join(relative_path, "js", glue_file_name), file_contents)


def deserialize_json_string(string):
    return json.loads(string)


def get_list_of_ordered_puzzle_answer_dicts():
    return deserialize_json_string(file_get_contents(get_absolute_path("levels.json")))


def get_puzzle_names(list_of_puzzle_answer_dicts):
    return [puzzleAnswerDict['puzzle'] for puzzleAnswerDict in list_of_puzzle_answer_dicts]


def get_puzzle_answers(list_of_puzzle_answer_dicts):
    return [puzzleAnswerDict['answer'] for puzzleAnswerDict in list_of_puzzle_answer_dicts]


def get_encoded_answers(list_of_puzzle_answer_dicts):
    for answer in get_puzzle_answers(list_of_puzzle_answer_dicts):
        md5object = hashlib.md5()
        md5object.update(answer)
        yield md5object.hexdigest()


def get_puzzle_folders(encoded_answers):
    try:
        len(encoded_answers)
    except:
        encoded_answers = [i for i in encoded_answers]

    return ["start"]+encoded_answers


def get_absolute_path(relative_path):
    return os.path.join(os.getcwd(), relative_path)


def delete_folder(relative_path_of_folder):
    absolute_path = get_absolute_path(relative_path_of_folder)
    if os.path.exists(absolute_path):
        shutil.rmtree(absolute_path)


def copy_folder(relative_source, relative_destination):
    absolute_source = get_absolute_path(relative_source)
    absolute_destination = get_absolute_path(relative_destination)
    shutil.copytree(src=absolute_source, dst=absolute_destination, ignore=shutil.ignore_patterns("node_modules"))


def process_cmd_line_arguments(*args):
    if len([arg for arg in args if arg=="-d"]) >= 1:
        global DEBUG
        DEBUG = True

def main():
    build_path = "build"
    global_build_files_directory_js = "../global"
    global_build_files_directory_css = "act_global_css"

    list_of_puzzle_answer_dicts = get_list_of_ordered_puzzle_answer_dicts()
    # print list_of_puzzle_answer_dicts

    delete_folder(build_path)
    copy_folder(global_build_files_directory_js, os.path.join(build_path, "global"))
    copy_folder(global_build_files_directory_css, os.path.join(build_path, "act_global_css"))
    puzzle_names = get_puzzle_names(list_of_puzzle_answer_dicts)
    puzzle_answers = get_puzzle_answers(list_of_puzzle_answer_dicts)
    puzzle_encoded_answers = get_encoded_answers(list_of_puzzle_answer_dicts)
    puzzle_folder_build = get_puzzle_folders(puzzle_encoded_answers)
    totalPuzzles = None
    level_info = zip(puzzle_names, puzzle_folder_build, xrange(len(puzzle_names)), puzzle_answers)
    for folder, folderName, index, puzzle_answer in level_info:
        try:
            answerHash = level_info[index+1][1]
        except IndexError:
            answerHash = "end of act"
        print "level {1}:\t\t\t{0}".format(folder, index)
        print "folder name: {0}".format(folderName)
        print "puzzle answer: {0}".format(puzzle_answer)

        #check to see if hashing is correct.
        md5object = hashlib.md5()
        md5object.update(puzzle_answer)
        assert(md5object.hexdigest() == answerHash or "end of act")

        print "encoded answer: {0}\n".format(answerHash)
        relative_build_new_directory = os.path.join(build_path, folderName)
        absolute_build_new_directory = get_absolute_path(relative_build_new_directory)
        copy_folder(folder, absolute_build_new_directory)
        create_glue_file(index, answerHash, relative_build_new_directory)
        totalPuzzles = index+1

    print "puzzles compiled."
    print "There are {0} puzzles".format(totalPuzzles)

if __name__ == "__main__":
    main()










