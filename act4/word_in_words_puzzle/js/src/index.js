var _ = require('underscore');
var md5 = require("blueimp-md5").md5;


var shuffle = function shuffle(array) {
	var currentIndex = array.length, temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
};

var createLeftTopBallElement = function (radius, color) {
	var span = document.createElement("span");
	$(span).css({
		width: radius,
		height: radius,
		"border-top-right-radius": radius,
		"background-color": color,
		"margin-right": 0
	});
	return span;
};

var reflectElementY = function (element) {
	return $(element).css({
		"-moz-transform": "scale(-1, 1);",
		"-webkit-transform": "scale(-1, 1)",
		"-o-transform": "scale(-1, 1);",
		"-ms-transform": "scale(-1, 1);",
		"transform": "scale(-1, 1);"
	});
};

var reflectElementX = function (element) {
	return $(element).css({
		"-moz-transform": "scale(1, -1);",
		"-webkit-transform": "scale(1, -1)",
		"-o-transform": "scale(1, -1);",
		"-ms-transform": "scale(1, -1);",
		"transform": "scale(1, -1);"
	});
};

var createRightTopBallElement = function (radius, color) {
	return _.compose(reflectElementY, createLeftTopBallElement)(radius, color);
};

var createLeftBottomBallElement = function (radius, color) {
	return _.compose(reflectElementX, createLeftTopBallElement)(radius, color);
};

var createRightBottomBallElement = function (radius, color) {
	return _.compose(reflectElementX, createRightBottomBallElement)(radius, color);
};

var createLeftHalfCircle = function (radius, color) {
	var span = document.createElement("span");
	$(span).css({
		"width": "auto",
		"margin-right": 0
	});
	var top = document.createElement("div");
	var bottom = document.createElement("div");
	$(top).append(createLeftTopBallElement(radius, color)).css("margin", 0).css({
		margin: 0,
		padding: 0,
		width: radius,
		height: radius,
		"margin-bottom": -1
	});
	$(bottom).append(createLeftBottomBallElement(radius, color)).css({
		margin: 0,
		padding: 0,
		width: radius,
		height: radius,
		"margin-bottom": -1
	});
	return $(span).append(top).append(bottom);
};

var createRightHalfCircle = function (radius, color) {
	return _.compose(reflectElementY, createLeftHalfCircle)(radius, color);
};

var createCircle = function (radius, color) {
	var right = createRightHalfCircle(radius, color);
	var left = createLeftHalfCircle(radius, color);
	var span = document.createElement("span");
	$(span).css({
		"width": "auto"
	});
	return $(span).append(right).append(left);
};

var splitIterable = function (iterable) {
	return [iterable.slice(0, iterable.length / 2), iterable.slice(iterable.length / 2)];
};

var insertArrayIntoArray = function (array, insertArray) {
	var splittedArray = splitIterable(array);
	return splittedArray[0].concat(insertArray).concat(splittedArray[1]);
};

var insertStringIntoString = function (string, insertString) {
	return string.slice(0, string.length / 2).concat(insertString).concat(string.slice(string.length / 2));
};

var cleanUpSentence = function (sentence) {
	var punctRE = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#\$%&\(\)\*\+,\-\.\/:;<=>\?@\[\]\^_`\{\|\}~]/g;
	//var spaceRE = /\s+/g;
	return sentence.replace(punctRE, '')
		//.replace(spaceRE, '')
		.trim();
};

var splitSentenceIntoArray = function (sentence) {
	return sentence.split(" ");
};


//composed, high level functions

var createArrayOfWordsFromSentence = function (sentence) {
	return _.compose(splitSentenceIntoArray, cleanUpSentence)(sentence);
};

var createShuffledWordsFromSentence = function (sentence) {
	return _.compose(shuffle, createArrayOfWordsFromSentence)(sentence);
};

function getRandomColor() {
	var letters = '0123456789ABCDEF'.split('');
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

var createWordToColorObject = function (words) {
	return _.reduce(words, function (memo, word) {
		memo[word] = getRandomColor();
		return memo;
	}, {});
};

var fromWordToColorObjectAndWordsCreateArrayOfBalls = function (words, wordToColorObject, radius) {
	return _.map(words, function (word) {
		return createCircle(radius, wordToColorObject[word]);
	});
};

var appendArrayOfElementsToElement = function (elements, targetElement) {
	elements.forEach(function (element) {
		$(targetElement).append(element);
	});
};

var createHalfCirclesFromWord = function (radius, word, wordToColorTranslator) {
	var left = createRightHalfCircle(radius, wordToColorTranslator[word]);
	var right = createLeftHalfCircle(radius, wordToColorTranslator[word]);
	return [left, right];
};

var createDiagramFromWords = function (radius, words, wordToColor) {
	return _.reduce(words, function (memo, word) {
		if (memo.length === 0) {
			return createHalfCirclesFromWord(radius, word, wordToColor);
		}
		return insertArrayIntoArray(createHalfCirclesFromWord(radius, word, wordToColor), memo);
	}, []);
};

var encodeWords = function (words) {
	return _.reduce(words, function (memo, word) {
		var dividers = "()";
		if (memo.length === 0) {
			return insertStringIntoString(dividers, word);
		}
		return _.compose(_.partial(insertStringIntoString, dividers, _), insertStringIntoString)(word, memo);
	}, "");
};


(function () {
	init();

	function init() {
		var $contentDiv = $('.content');
		var div = document.createElement("div");

		//var ball = createCircle(20, "blue");
		var puzzleDiv = document.createElement("div");
		var sentence = "who said once youve accepted your flaws no one can use them against you";
		var words = createArrayOfWordsFromSentence(sentence);
		var wordToColorTranslator = createWordToColorObject(words);
		var balls = fromWordToColorObjectAndWordsCreateArrayOfBalls(words, wordToColorTranslator, 20);
		appendArrayOfElementsToElement(balls, puzzleDiv);
		var shuffledWords = shuffle(words);
		var diagram = createDiagramFromWords(20, shuffledWords, wordToColorTranslator);
		$(puzzleDiv).append(div);
		appendArrayOfElementsToElement(diagram, div);
		var encodedMessage = encodeWords(shuffledWords);
		var div2 = document.createElement("div");
		$(div2).html(encodedMessage);
		$(puzzleDiv).append(div2);
		$contentDiv.prepend(puzzleDiv);

		$(".answer-input").off().click(function(event){
			event.preventDefault();
			var answer = $(".answer-input").val();
			if(answer.toLowerCase().match(/tyrion/)){
				return window.malum.checkAnswerAndCreateButton("tyrion");
			}
		});


		showCurrentLevel();
		showLevelContent();
		//$contentDiv.append(ball);

		//window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();



