var _ = require('underscore');
var md5 = require("blueimp-md5").md5;


var reverseRange = function (length, start) {
	start = start ? 0 : start;
	if (length <= start) {
		return [];
	}
	return [length].concat(reverseRange(length - 1, start));
};

var range = function (start, length) {
	return reverseRange(length, start).reverse();
};

var createMatrix = function (x, y, numbers) {
	if (y === 1) {
		return [numbers.slice(0, x)];
	}
	return [numbers.slice(0, x)].concat(createMatrix(x, y - 1, numbers.slice(x)));
};

var isRange = function (start, end, check) {
	return _.reduce(_.zip(range(start, end), check.sort()), function (memo, subarray) {
		return memo && (subarray[0] === subarray[1]);
	}, true);
};

var sum = function (array) {
	return _.reduce(array, function (memo, number) {
		return memo + number;
	}, 0);
};

var getRowSums = function (matrix) {
	return _.map(matrix, function (row) {
		return sum(row);
	});
};

var getColSums = function (matrix) {
	var result = [];
	for (var col = 0; col < matrix.length; col++) {
		var colSum = 0;
		for (var row = 0; row < matrix[0].length; row++) {
			colSum += matrix[row][col];
		}
		result.push(colSum);
	}
	return result;
};

var getDiagonalSums = function (matrix) {
	var result = [0, 0];
	for (var i = 0; i < matrix.length; i++) {
		result[0] += matrix[i][i];
		result[1] += matrix[i][matrix.length - 1 - i];
	}
	return result;
};

var areAllElementsEqual = function (array) {
	return _.reduce(array, function (memo, num) {
		return memo && (num === array[0]);
	}, true);
};

var checkIfMatrixIsCorrect = function (matrix) {
	var sumarray = getColSums(matrix).concat(getRowSums(matrix)).concat(getDiagonalSums(matrix));
	return areAllElementsEqual(sumarray) && isRange(1, 9, _.flatten(matrix));
};

var generateMatrixElement = function (x, y) {
	var elements = [];
	var matrixElement = document.createElement("div");
	for (var row = 0; row < y; row++) {
		var rowElement = document.createElement("div");
		$(matrixElement).append(rowElement);
		for (var col = 0; col < x; col++) {
			var selectionElement = document.createElement("input");
			$(selectionElement)
				.addClass("x_" + col.toString())
				.addClass("y_" + row.toString())
				.addClass("cell").attr("maxlength", 1).attr("type", "number").attr("type", "text");
			elements.push(selectionElement);
			$(rowElement).append(selectionElement);

		}
	}
	return $(matrixElement).css({
		"margin":"4px"
	});
};

var isAlpha = function (name) {
	return name.search(/[^A-Za-z\s]/) === -1;
};

function IsNumeric(val) {
	return Number(parseFloat(val)) == val;
}

var getAnswers = function () {
	var result = [];
	$(".cell").each(function (index, element) {
		var value = IsNumeric($(this).val()) ? parseInt($(this).val()) : 0;
		result.push(value);
	});
	return result;
};


(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		var matrixElement = generateMatrixElement(3, 3);
		$contentDiv.append(matrixElement);
		$(".cell").keyup(function () {
			if(!IsNumeric($(this).val())){
				this.value = "";
				return;
			}
			var answers = getAnswers();
			var matrix = createMatrix(3, 3, answers);
			if (checkIfMatrixIsCorrect(matrix)) {
				window.malum.checkAnswerAndCreateButton("sum_puzzle_qaz");
			}
		});
	}
})();



