var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

(function () {
	init();

	//function isEven(number){
	//	return number%2===0;
	//}


	function isAnyOfXinY(x,y){
		for(var i = 0; i< x.length; x++){
			for(var j = 0; j< y.length; j++){
				if(x[i] === x[j]){
					continue;
				} else {
					return false;
				}
			}
		}
	}

	function removeXFromY(x,y){
		var regexpstr = "["+x+"]";
		var removeRegExp = RegExp(regexpstr, "g");
		return y.replace(removeRegExp, "");
	}

	function generateRandomCode(message, possible, length){
		possible = possible.toUpperCase();
		message = message.toUpperCase();
		possible = removeXFromY(message, possible);
		var result = "";
		for(var i = 0; i<length; i++){
			var randomVar = _.random(0, possible.length-1);
			result += possible[randomVar];
		}
		return result;
	}

	function init() {
		var currentLevel = showCurrentLevel();
		//$(currentLevel).css({
		//	top:"20px"
		//});
		showLevelContent();
		var code = generateRandomCode("3 kings", "abcdefghijklmnopqrstuvwxyz1234567890", 99999);
		var $code = $(".code").width("100%");
		$code.html(code);
		$answerInput = $(".answer-input");
		$(".submit").click(function(e){
			e.preventDefault();
			var answer = $answerInput.val();
			answer = answer.split(" ").join('');
			window.malum.checkAnswerAndCreateButton(answer);

		});



	}
	//window.utils.dynamicVerticalAndHorizontalCentering($(".content"));

})();



