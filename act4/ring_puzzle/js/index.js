var ring1 = [25,17,15,24,17,34];
var ring2 = [10,40,10,20,10,20];
var ring3 = [6,25,13,15,26,13];
var ring4 = [30,40,50,50,30,60];

var checkAnswer = function(){
	var sum = $(".sum");
	var side1 = ring1[0]+ring2[0]+ring3[0]+ring4[0];
	var side2 = ring1[1]+ring2[1]+ring3[1]+ring4[1];
	var side3 = ring1[2]+ring2[2]+ring3[2]+ring4[2];
	var side4 = ring1[3]+ring2[3]+ring3[3]+ring4[3];
	var side5 = ring1[4]+ring2[4]+ring3[4]+ring4[4];
	var side6 = ring1[5]+ring2[5]+ring3[5]+ring4[5];

	sum.html('<span>'+side1+'</span>'+'<span>'+side2+'</span>'+'<span>'+side3+'</span>'+'<span>'+side4+'</span>'+'<span>'+side5+'</span>'+'<span>'+side6+'</span>')
	
	if (side1 == 100 && side2 == 100 && side3 == 100 && side4 == 100 && side5 == 100 && side6 == 100){
		$('.ring-super').css('opacity', '.25');
		window.malum.checkAnswerAndCreateButton("centurysum");
	}
}

$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);

	checkAnswer();

});

$(document).on('click', '.ringclick', function(){
	var id = $(this).attr('id');

	if(id == "ring1"){
		rotateArray(ring1,id);
	}

	else if(id == "ring2"){
		rotateArray(ring2,id);
	}

	else if(id == "ring3"){
		rotateArray(ring3,id);
	}

	else if(id == "ring4"){
		rotateArray(ring4,id);
	}
});

var rotateArray = function(ring,id){
	var id = id;
	var firstElement = ring.shift();
	ring.push(firstElement);
	drawRing(ring,id);
}

var drawRing = function(ring,id){
	$("#"+id).html('');
	$("#"+id).append('<span class="ring rotate0">' + ring[0] + '</span>');
	$("#"+id).append('<span class="ring rotate60">' + ring[1] + '</span>');
	$("#"+id).append('<span class="ring rotate120">' + ring[2] + '</span>');
	$("#"+id).append('<span class="ring rotate180">' + ring[3] + '</span>');
	$("#"+id).append('<span class="ring rotate240">' + ring[4] + '</span>');
	$("#"+id).append('<span class="ring rotate300">' + ring[5] + '</span>');
	checkAnswer();
}

