var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

//for mobile and other devices that use the cache on "back" this forces a page refresh if it was loaded from the cache.
$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        window.location.reload()
    }
});

(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
		window.state = initState("a", "a", "a", "a");
		constructFinalInitStringFromCookieAndGet();
		checkIfShouldRenderControls();
		renderState(state);
		checkWin();
	}

})();

function checkIfShouldRenderControls(){
	var cookieState = getCookieState();
	if(cookieState["disableControl"] === true){
		$(".controls").hide();
	}

}

//positions are a,b,c,m
function initState(manPosition, ballRed, ballBlue, ballGreen) {
	return {
		manPosition: manPosition,
		ballRed: ballRed,
		ballBlue: ballBlue,
		ballGreen: ballGreen
	}
}

function renderState(state) {
	var $man;
	for (var key in state) {
		if (key === "manPosition") {
			$man = createMan();
			var position = state[key];
			$("." + position).append($man);

		}
		if (key.slice(0, 4) === "ball") {
			var color = key.slice(4).toLowerCase();
			var $ball = createBall(15, color).css({
				//display:"inline"
			});
			var position = state[key];
			if (position != "m") {
				$("." + position).append($ball);
			} else if (position === "m") {
				$ball.css({
					position: "absolute",
					right: 40,
					top: 50
				});
				$man.append($ball);


			}

		}
	}
}

function createMan() {
	var $div = $(document.createElement("div"));
	var $img = $(document.createElement("img"));
	$div.append($img);
	$div.css({
		width: 204.484,
		height: 204.484,
		position: "relative"
	});
	$img.attr("src", "man.svg").css({
		overflow: "hidden"
	});
	return $div;
}

function createBall(radius, color) {
	var $ball = $(document.createElement("span"));
	$ball.css({
		width: 2 * radius,
		height: 2 * radius,
		"border-radius": "50%",
		"background-color": color
	});

	$ball.click(function () {
		clearRender();
		var color = $ball.css("background-color");
		color = color.charAt(0).toUpperCase() + color.slice(1);
		var ballName = "ball" + color;
		window.state = switchBallStateToMan(window.state, ballName);
		renderState(window.state);
	}).addClass("round-balls");

	return $ball;
}

function serializeState(state) {
	return JSON.stringify(state);
}

function deserializeString(string) {
	return JSON.parse(string);
}

function encodeString(string) {
	return btoa(string);
}

function decodeString(string) {
	return atob(string);
}

function encodeStateJSONBase64(state) {
	return encodeString(serializeState(state));
}

function deserializeBase64Encoded(string) {
	return deserializeString(decodeString(string));
}

function clearRender() {
	["a", "b", "c"].forEach(function (letter) {
		$("." + letter).html("");
	});
}

function switchBallStateToMan(state, whichBall) {
	if (state[whichBall] != "m" && state[whichBall] === state["manPosition"]) {
		["Red", "Blue", "Green"].forEach(function (color) {
			var key = "ball" + color;
			if (state[key] === "m") {
				state[key] = state["manPosition"];
			}
		});

		state[whichBall] = "m";
	} else if ((state["manPosition"] === "a" || state["manPosition"] === "c") && state[whichBall] === "m") {
		state[whichBall] = state["manPosition"];
	}
	saveCookieState(createCookieStateFromState(state));
	checkWin();
	return state;
}

function getCookieState() {
	var cookieString = $.cookie("cookieState");
	if (!cookieString) {
		return {};
	} else {
		return deserializeBase64Encoded(cookieString);
	}
}

function saveCookieState(state) {
	$.cookie("cookieState", encodeStateJSONBase64(state));
}

function clearCookieState(){
	$.removeCookie("cookieState");
}

function createCookieStateFromState() {
	var oldCookieState = getCookieState();
	var result = {}
	var colorArray = ["Red", "Blue", "Green"];
	colorArray.forEach(function (color) {
		result["ball" + color] = window.state["ball" + color];
	});
	if (window.state["manPosition"] === "c" || oldCookieState["disableControl"] === true) {
		result["disableControl"] = true;
	}
	return result;
}

function overrideGlobalStateFromCookie() {
	var cookieState = getCookieState();
	for (var key in cookieState) {
		window.state[key] = cookieState[key];
	}
	return window.state;
}

//forward = true; backward = false
var FORWARD = true;
var BACKWARD = false;
function moveMan(state, direction) {
	if (state["manPosition"] === "b" && direction === FORWARD) {
		state["manPosition"] = "c";
	} else if (state["manPosition"] === "b" && direction === BACKWARD) {
		state["manPosition"] = "a";
	} else if (state["manPosition"] === "a" && direction === FORWARD) {
		state["manPosition"] = "b";
	} else if (state["manPosition"] === "c" && direction === BACKWARD) {
		state["manPosition"] = "b";
	}
	return {
		"manPosition":state["manPosition"]
	};
}

function serializeObjectToQuery(obj) {
	var str = [];
	for (var p in obj)
		if (obj.hasOwnProperty(p)) {
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	return str.join("&");
}

function getInitFromQueryString() {
	var url = window.location.href;
	var encodedString = url.split("getState=")[1];
	if (!encodedString) {
		return window.state;
	}
	return deserializeBase64Encoded(decodeURIComponent(encodedString));
}

function constructFinalInitStringFromCookieAndGet() {
	window.state = getInitFromQueryString();
	overrideGlobalStateFromCookie();
	return window.state;

}

$(".forward").click(function (e) {
	e.preventDefault();
	$(this).off();
	var newState = moveMan(window.state, FORWARD);
	var getState = {
		"getState": encodeStateJSONBase64(newState)
	};
	var cookieState = createCookieStateFromState();
	saveCookieState(cookieState);
	var queryString = serializeObjectToQuery(getState);
	var finalUrl = "http://" + window.location.host + window.location.pathname + "?" + queryString;
	window.location.href = finalUrl;

});

$(".back").click(function (e) {
	e.preventDefault();
	$(this).off();
	var newState = moveMan(window.state, BACKWARD);
	var getState = {
		"getState": encodeStateJSONBase64(newState)
	};
	var cookieState = createCookieStateFromState();
	saveCookieState(cookieState);
	var queryString = serializeObjectToQuery(getState);
	var finalUrl = "http://" + window.location.host + window.location.pathname + "?" + queryString;
	window.location.href = finalUrl;
});

$(".restart").click(function(e){
	e.preventDefault();
	$(this).off();
	clearCookieState();
	var finalUrl = "http://" + window.location.host + window.location.pathname;
	window.location.href = finalUrl;
});

function checkWin(){
	var colorArray = ["Red", "Green", "Blue"];
	for(var i = 0; i<colorArray.length; i++){
		var ballName = "ball"+colorArray[i];
		if(window.state[ballName] != "c"){
			return false;
		}
	}

	window.malum.checkAnswerAndCreateButton("threeballs");
}



