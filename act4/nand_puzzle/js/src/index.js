var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

//if input is false return false;

isCorrect = function (input) {
	if (input === undefined)
		return undefined;
	return input.match(/^[\(\)abAB!& ]*$/) ? input : undefined;
};

eliminateSpaces = function (input) {
	return input.replace(/ /g, '');
};

makeLowerCase = function (input) {
	return input.toLowerCase();
};

placeInputs = function (a, b) {
	return function (input) {
		if (input === undefined)
			return undefined;
		return input.replace(/a/g, a).replace(/b/g, b);
	}
};

calculateOutput = function (input) {
	if (input === undefined)
		return undefined;
	try {
		return nandparser.parse(input)
	} catch (e) {
		return undefined;
	}
};

testInputsOnExpression = function (a, b, expression) {
	var expressionWithInputs = placeInputs(a, b);
	return _.compose(calculateOutput, expressionWithInputs, isCorrect, eliminateSpaces, makeLowerCase)(expression);
};

createExpressionTester = function (expression) {
	return function (a, b) {
		return _.compose(calculateOutput, placeInputs(a, b), isCorrect, eliminateSpaces, makeLowerCase)(expression);
	};
};

not = function (a, b) {
	if (b !== a) {
		return false;
	} else {
		return (~a) === 1;
	}
};

and = function (a, b) {
	return (a & b) === 1;
};

nand = function (a, b) {
	return !and(a, b);
}

or = function (a, b) {
	return (a | b) === 1;
};

nor = function (a, b) {
	return !or(a, b);
};

xor = function (a, b) {
	return (a ^ b) === 1;
};

testFunctions = {
	"not": not,
	"and": and,
	"nor": nor,
	"or": or,
	"xor": xor,
	"nand": nand
};

testExpressionAndFunction = function recurse(expression, inputFunction, inputs) {
	if (inputs.length === 0) {
		return true;
	} else {
		var expressionOutput = testInputsOnExpression(inputs[0][0], inputs[0][1], expression);
		var functionOutput = inputFunction(inputs[0][0], inputs[0][1]);
		return expressionOutput === functionOutput ? recurse(expression, inputFunction, inputs.slice(1)) : false;
	}

};

createRow = function (inputs, colSpan) {
	var tr = document.createElement("tr");
	for (var i = 0; i < inputs.length; i++) {
		var td = document.createElement("td");
		$(td).html(inputs[i]);
		$(td).addClass("truth_table_cells");
		if (colSpan) {
			$(td).attr("colspan", 3);
		}
		$(tr).append(td);
	}
	return tr;
};

boolToNum = function (bool) {
	return bool ? 1 : 0;
};


createTruthTable = function (truthFunction, key) {
	var table = document.createElement("table");
	$(table).addClass("truth_table");
	var inputs = [[0, 0], [0, 1], [1, 0], [1, 1]];
	for (var i = 0; i < inputs.length + 2; i++) {
		var row;
		if (i === 1) {
			row = createRow(["A", "B", "OUTPUT"]);
		} else if (i == 0) {
			row = createRow([key], 3);
		} else {
			var a = inputs[i - 2][0];
			var b = inputs[i - 2][1];
			var output = truthFunction(a, b);
			if (output === undefined) {
				return $(document.createElement("div")).html("Error.");
			}
			row = createRow([a, b, boolToNum(truthFunction(a, b))])
		}
		$(table).append(row);
	}
	return table;

};

createPuzzleElement = function (key, successFunction) {
	var div = document.createElement("div");
	var puzzle_holder = document.createElement("div");
	$(puzzle_holder).addClass("puzzle_holder");
	var form = document.createElement("form");
	var input = document.createElement("input");
	$(input).attr("type", "text").addClass("answer-input").attr("id", "truth");

	$(form).append(input);
	var truthTable = createTruthTable(testFunctions[key], key);

	var keyhandler = function (e) {
		$(input).unbind("keyup");
		var inputs = [[0, 0], [0, 1], [1, 0], [1, 1]];
		var expression = $(this).val();
		$(".answer-table").remove();
		var $content = $(".content");
		var answerElement;
		console.log(expression);
		if (expression === "") {
		} else {
			answerElement = createTruthTable(createExpressionTester(expression), expression);
		}
		$(answerElement).addClass("answer-table");
		$(".puzzle_holder").append(answerElement);
		var isValid = testExpressionAndFunction(expression, testFunctions[key], inputs);
		if (isValid) {
			successFunction();
		} else {
			$(input).bind("keyup", keyhandler);
		}
	};
	$(input).bind("keyup", keyhandler);

	//var answerTable = createTruthTable(createExpressionTester(expression));
	$(puzzle_holder).append(truthTable);
	$(div).append(puzzle_holder).append(form).addClass("puzzle");
	return div;
};

createPuzzleSwitchButton = function (elementToAppendTo, switchFunction) {
	var button = document.createElement("button");
	$(button).html("next").css({
		opacity: 0
	});
	$(elementToAppendTo).append(button);
	$(button).animate({
		opacity: 1
	}, 1200, "swing", function () {
		$(button).click(function () {
			switchFunction();
		});
	}).addClass("next-button");
};

puzzle_manager = function (puzzleKeys, elementToAppendInto) {
	if (puzzleKeys.length === 0) {
		window.malum.checkAnswerAndCreateButton("binarypower");
	}

	var puzzle = createPuzzleElement(puzzleKeys[0], function () {
		var successText = document.createElement("div");
		$(successText).html("PASS").css({opacity:0}).addClass("success_text");
		$(puzzle).append(successText);
		$(successText).animate({opacity:1});
		$("input").attr("disabled","disabled");
		createPuzzleSwitchButton($(puzzle), function () {
			$(puzzle).animate({opacity: 0}, 1200);
			$(".answer-table").animate({opacity: 0}, 1200, "swing", function () {
				$(puzzle).remove();
				$(".answer-table").remove();
				puzzle_manager(puzzleKeys.slice(1), elementToAppendInto);
			});
		});
	});
	$(puzzle).css({
		opacity: 0
	});
	$(elementToAppendInto).append(puzzle);
	$(puzzle).animate({opacity: 1});
};

(function () {
	init();


	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		var puzzle_keys = ["nand", "and", "or", "nor", "xor"];
		puzzle_manager(puzzle_keys, $contentDiv);


		//window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();




