var finalAnswer;

var productFunctions = {

	generateRandomNumbers: function(numbers,arrayType){

		var array = [];
		var question = [];

		for (var i = 0; i < numbers; i++){

			var randomInt = Math.floor((Math.random() * 20) + 1);

			array.push(randomInt);

		}

		if(arrayType == 'clue'){
			productFunctions.generateContent(array,"clue");
		}

		else if(arrayType == 'question'){
			productFunctions.generateContent(array,"question");
		}

		

	},

	generateContent: function(array,arrayType){
		var clue = $('.clue');
		var question = $('.question');
		var array = array;

		if(arrayType == "clue"){
			clue.html('');

			for (var i = 0; i < array.length; i++){
				clue.append('<span>'+array[i]+'</span>');
			}

			productFunctions.generateAnswer(array, "clue");

		}
	},

	generateAnswer: function(array,arrayType){
		var clueAnswer = $('.clue-answer');
		var array = array;
		var sortedArray = array.sort(function(a, b){return b-a});
		var posProduct = sortedArray[0]*sortedArray[1]*sortedArray[2];

		if(arrayType == 'clue'){
			clueAnswer.html('');
			clueAnswer.append(posProduct);
		}

	},

	init: function(){
		this.generateRandomNumbers(8,"clue");
	}

};
productFunctions.init();


$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	var content = $('.content');
	window.service.verticalCenter(content);


});

$(document).on('click', '.next', function(){
	productFunctions.init();
});




