

var productFunctions = {

	generateRandomNumbers: function(numbers){
		var array = [];
		var question = [];

		for (var i = 0; i < numbers; i++){
			var randomInt = Math.floor((Math.random() * 10) + 1);
			array.push(randomInt);
		}

		productFunctions.displayNumbers(array);
	},

	createQuestion: function(){
		var array = [9,4,10,5,6];
		var total = 1;
		var question1 = $('.question1');
		var question2 = $('.question2');

		for (var i = 0; i < array.length; i++){
			question1.append('<span>'+array[i]+"</span>");
			total = total*array[i];
		}

		for (var j = 0; j < array.length; j++){
			var num = total/array[j];

			if(j == 0){
				question2.append('<span class="highlight">???</span>')
			}

			else{
				question2.append('<span>'+num+'</span>');
			}
		}

	
	},

	displayNumbers: function(array){
		var array = array;
		var clue1 = $('.clue1');
		for(var i = 0; i < array.length; i++){
			clue1.append('<span>'+array[i]+"</span>");
		}
		productFunctions.createAndDisplayAnswers(array);


	},

	createAndDisplayAnswers: function(array){
		var array = array;
		var total = 1;
		var clue2 = $('.clue2');

		for(var i = 0; i < array.length; i++){
			total = total * array[i];
		}

		for (var j = 0; j < array.length; j++){
			var num = total/array[j];
			clue2.append('<span>'+num+'</span>');
		}
	},

	clearAll: function(){
		$('.clue1').html('');
		$('.clue2').html('');
	},


	init: function(){
		this.clearAll();
		this.generateRandomNumbers(5);
	}

};
productFunctions.init();


$(document).ready(function(){
	
	showCurrentLevel();
	showLevelContent();

	productFunctions.createQuestion();



});

$(document).on('click', '.next', function(){
	productFunctions.init();
});




