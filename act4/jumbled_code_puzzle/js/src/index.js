var _ = require('underscore');
var md5 = require("blueimp-md5").md5;

window.shuffle = function shuffle(array) {
	var currentIndex = array.length, temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
};

window.createLegendFromWordsAndTranslator = function (translatorObject) {

	function size(obj) {
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}


	var table1 = document.createElement("table");
	var table2 = document.createElement("table");
	//var titleRow = document.createElement("tr");
	//var title = document.createElement("th");
	//$(title).html("Legend").attr("colspan", 2);
	//$(table).append(titleRow);
	//$(titleRow).append(title);

	var tbody1 = document.createElement("tbody");
	var tbody2 = document.createElement("tbody");
	$(table1).append(tbody1);
	$(table2).append(tbody2)

	var keys = [];
	for (var key in translatorObject) {
		keys.push(key);
	}
	keys = shuffle(keys);
	var halfTableLength = keys.length/2;

	var acc = 0;
	keys.forEach(function (key) {
		var tbody;
		acc += 1;
		if(acc<=halfTableLength){
			tbody = tbody1;
		} else {
			tbody = tbody2;
		}
		var tr = document.createElement("tr");
		var word = document.createElement("td");
		$(word).html(key);
		var symbol = document.createElement("td");
		$(symbol).html(translatorObject[key]).addClass("symbols");
		$(tr).append(word).append(symbol);
		$(tbody).append(tr);
	});
	$(table1).css({
		display: "inline-block"
	}).addClass("legend").addClass("left");
	$(table2).css({
		display: "inline-block"
	}).addClass("legend").addClass("right");
	var div = document.createElement("div");
	var p = document.createElement("p");
	$(p).html("Legend");
	$(div).append(p).append(table1).append(table2).addClass("legend-box");

	return div;


};


function range(length) {
	var result = [];
	for (var i = 0; i < length; i++) {
		result.push(i);
	}
	return result;
};

window.jumbleByArray = function jumbleByArray(words, jumble) {
	var result = [];
	for (var i = 0; i < jumble.length; i++) {
		result[i] = words[jumble[i]];
	}
	return result;
};

window.wordJumblerMaker = function wordJumblerMaker(length) {
	var jumble = _.compose(shuffle, range)(length);
	return function (words) {
		return jumbleByArray(words, jumble);
	};
};

window.cleanUpSentence = function cleanUpSentence(sentence) {
	var punctRE = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#\$%&\(\)\*\+,\-\.\/:;<=>\?@\[\]\^_`\{\|\}~]/g;
	//var spaceRE = /\s+/g;
	return sentence.replace(punctRE, '')
		//.replace(spaceRE, '')
		.trim();
};

window.splitSentenceIntoArray = function splitSentenceIntoArray(sentence) {
	return sentence.split(" ");
};

function segmentifyArray(array, segments) {
	if (array.length % segments !== 0) {
		throw "Length of the array (" + array.length + ")  must be a multiple of " + segments;
	} else {
		return segmentifyArrayTail(array, segments, []);
	}

	function segmentifyArrayTail(array, segments, tail) {
		if (array.length === 0 || segments === 0) {
			return tail;
		}
		var lengthOfSegment = array.length / segments;
		var segment = array.slice(0, lengthOfSegment);
		tail.push(segment);
		return segmentifyArrayTail(array.slice(lengthOfSegment), segments - 1, tail);

	}
}

window.generateArrayOfShuffleFunctions = function generateArrayOfShuffleFunctions(length, segmentLength) {
	return _.map(range(length), function () {
		return wordJumblerMaker(segmentLength);
	});
};

window.shuffleSegments = function shuffleSegments(segments, shuffleFunctions) {
	return _.map(range(segments.length), function (index) {
		return shuffleFunctions[index](segments[index]);
	});
};

window.encodeSegmentsIntoSentence = function (segments) {
	return _.reduce(segments, function (memo, segment) {
		return memo.concat(segment);
	}, []).join(" ");
};

window.createWordToSymbolTranslatorObject = function (words) {
	var result = {};
	var symbols = "abcdefghijklmnopqrstuvwxyz";
	var currentSymbolIndex = 0;
	for (var i = 0; i < words.length; i++) {
		if (!result[words[i]]) {
			result[words[i]] = symbols[currentSymbolIndex];
			currentSymbolIndex++;
		}
	}
	return result;
};

window.createWordToSymbolTranslator = function (words) {
	return function (word) {
		return createWordToSymbolTranslatorObject(words)[word];
	}
};

window.turnSentenceIntoSegments = function turnSentenceIntoSegments(sentence, segments) {
	var arrayOfWords = _.compose(splitSentenceIntoArray, cleanUpSentence)(sentence);
	var wordTranslator = createWordToSymbolTranslator(arrayOfWords);
	return segmentifyArray(_.map(arrayOfWords, wordTranslator), segments);
};

window.generateEncoder = function (segmentAmount, segmentLength, shuffleFunctions) {
	return _.compose(
		encodeSegmentsIntoSentence,
		_.partial(shuffleSegments, _, shuffleFunctions),
		_.partial(turnSentenceIntoSegments, _, segmentAmount)
	);
};

window.createLegendFromSentence = function (sentence) {
	return _.compose(createLegendFromWordsAndTranslator, createWordToSymbolTranslatorObject, splitSentenceIntoArray, cleanUpSentence)(sentence)
};

window.displayNestedArray = function (nestedArray) {
	var divs = _.map(nestedArray, function (subArray) {
		var div = document.createElement("div");
		var inputString = _.reduce(subArray, function (memo, key) {
			return memo + key + " ";
		}, "");
		$(div).html(inputString).addClass("monospace-font");
		return div;
	});
	var div = document.createElement("span");

	divs.forEach(function (mdiv) {
		$(div).append(mdiv);
	});

	return div;

};

window.createSpannedElements = function createSpannedElements(elements, seperator) {
	var span = $(document.createElement("span")).addClass("monospace-font").css({
		width: "auto",
		margin: "2%"
	});

	for (var i = 0; i < elements.length; i++) {
		$(elements[i]).addClass("diagram").addClass("monospace-font");
		$(span).append(elements[i]);
		if (i < elements.length - 1) {
			var sdiv = document.createElement("div");
			$(sdiv).html(seperator).css({
				margin: "2%"
			});
			$(span).append(sdiv);
		}
	}

	return span;
};


(function () {
	init();

	function init() {
		showCurrentLevel();
		showLevelContent();
		var $contentDiv = $('.content');
		var segmentAmt = 3;
		var segmentLength = 5;
		var fns = generateArrayOfShuffleFunctions(segmentAmt, segmentLength);

		var sentence = "the campaign to bomb the united states with baloons during world war two was called";


		var diagram = "a b c d e f g h i j k l m n o";
		var diagramElement = $(document.createElement("div")).html(diagram);

		var segmentedDiagram = _.compose(_.partial(segmentifyArray, _, 3), splitSentenceIntoArray, cleanUpSentence)(diagram);
		var shuffledSegmentedDiagram = shuffleSegments(segmentedDiagram, fns);

		var segmentedDiagramElement = displayNestedArray(segmentedDiagram);
		var shuffledSegmentedDiagramElement = displayNestedArray(shuffledSegmentedDiagram);

		var encodedDiagram = encodeSegmentsIntoSentence(shuffledSegmentedDiagram);
		var encodedDiagramElement = $(document.createElement("div")).html(encodedDiagram);

		var finalDiagramElement = createSpannedElements([diagramElement, segmentedDiagramElement, shuffledSegmentedDiagramElement, encodedDiagramElement], "->");

		var infodiv = document.createElement("div");

		$(infodiv).append(finalDiagramElement).append(legend);
		var legend = createLegendFromSentence(sentence);
		var encoder = generateEncoder(segmentAmt, segmentLength, fns);
		$contentDiv.append($(document.createElement("div")).html("decode me:"));
		$contentDiv.append($(document.createElement("div")).html(encoder(sentence)).addClass("symbols"));
		$(infodiv).append(legend);
		$contentDiv.prepend(finalDiagramElement);
		$contentDiv.append(infodiv);


		//window.utils.dynamicVerticalAndHorizontalCentering($contentDiv);
	}
})();




